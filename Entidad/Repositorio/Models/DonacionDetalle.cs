﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entidad.Repositorio.Models
{
    public partial class DonacionDetalle
    {
        public int CodigoDonacionDetalle { get; set; }
        public int? CodigoDonacion { get; set; }
        public int? CodigoSolicitud { get; set; }
        public bool? Habilitado { get; set; }
        public int? Cantidad { get; set; }
        public virtual Donacion CodigoDonacionNavigation { get; set; }
        public virtual Solicitud CodigoSolicitudNavigation { get; set; }
    }
}
