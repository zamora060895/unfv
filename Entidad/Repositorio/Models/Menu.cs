﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entidad.Repositorio.Models
{
    public partial class Menu
    {
        public Menu()
        {
            Modulos = new HashSet<Modulo>();
        }

        public int CodigoMenu { get; set; }
        public string Nombre { get; set; }
        public string NombreWeb { get; set; }
        public int? Orden { get; set; }
        public DateTime FechaCreacionRegistro { get; set; }
        public bool Habilitado { get; set; }

        public virtual ICollection<Modulo> Modulos { get; set; }
    }
}
