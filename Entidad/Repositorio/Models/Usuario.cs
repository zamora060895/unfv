﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entidad.Repositorio.Models
{
    public partial class Usuario
    {
        public Usuario()
        {
            DonacionArchivos = new HashSet<DonacionArchivo>();
            DonacionCodigoUsuarioCreacionNavigations = new HashSet<Donacion>();
            DonacionCodigoUsuarioEstudianteNavigations = new HashSet<Donacion>();
            HistorialDonacions = new HashSet<HistorialDonacion>();
            HistorialSolicituds = new HashSet<HistorialSolicitud>();
            Solicituds = new HashSet<Solicitud>();
            UsuarioRols = new HashSet<UsuarioRol>();
        }

        public int CodigoUsuario { get; set; }
        public string Nombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string NumeroDocumento { get; set; }
        public string CorreoElectronico { get; set; }
        public string Usuario1 { get; set; }
        public string Contrasenia { get; set; }
        public string CodigoEstudiante { get; set; }
        public DateTime FechaCreacionRegistro { get; set; }
        public int? CodigoUsuarioModificacion { get; set; }
        public DateTime? FechaModificacionRegistro { get; set; }
        public bool Habilitado { get; set; }
        //public bool Administrador { get; set; }
        //public bool OperadorSolicitudes { get; set; }
        //public bool OperadorDonaciones { get; set; }

        public virtual ICollection<DonacionArchivo> DonacionArchivos { get; set; }
        public virtual ICollection<Donacion> DonacionCodigoUsuarioCreacionNavigations { get; set; }
        public virtual ICollection<Donacion> DonacionCodigoUsuarioEstudianteNavigations { get; set; }
        public virtual ICollection<HistorialDonacion> HistorialDonacions { get; set; }
        public virtual ICollection<HistorialSolicitud> HistorialSolicituds { get; set; }
        public virtual ICollection<Solicitud> Solicituds { get; set; }
        public virtual ICollection<UsuarioRol> UsuarioRols { get; set; }
    }
}
