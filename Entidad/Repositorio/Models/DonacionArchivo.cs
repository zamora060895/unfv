﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entidad.Repositorio.Models
{
    public partial class DonacionArchivo
    {
        public int CodigoDonacionArchivo { get; set; }
        public int? CodigoDonacion { get; set; }
        public string RutaArchivo { get; set; }
        public string NombreArchivo { get; set; }
        public string Extension { get; set; }
        public DateTime? FechaCreacion { get; set; }
        public int? CodigoUsuarioCreacion { get; set; }
        public string TipoArchivo { get; set; }
        public string NumeroVoucher { get; set; }
        public bool? Habilitado { get; set; }
        public string Nota { get; set; }

        public virtual Donacion CodigoDonacionNavigation { get; set; }
        public virtual Usuario CodigoUsuarioCreacionNavigation { get; set; }
    }
}
