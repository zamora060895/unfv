﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entidad.Repositorio.Models
{
    public partial class Donacion
    {
        public Donacion()
        {
            DonacionArchivos = new HashSet<DonacionArchivo>();
            DonacionDetalles = new HashSet<DonacionDetalle>();
            HistorialDonacions = new HashSet<HistorialDonacion>();
        }

        public int CodigoDonacion { get; set; }
        public DateTime? FechaDonacion { get; set; }
        public int? CodigoUsuarioEstudiante { get; set; }
        public int? CodigoEstado { get; set; }
        public int? CodigoUsuarioCreacion { get; set; }
        public DateTime? FechaCreacion { get; set; }
        public decimal? MontoTotal { get; set; }

        public virtual Estado CodigoEstadoNavigation { get; set; }
        public virtual Usuario CodigoUsuarioCreacionNavigation { get; set; }
        public virtual Usuario CodigoUsuarioEstudianteNavigation { get; set; }
        public virtual ICollection<DonacionArchivo> DonacionArchivos { get; set; }
        public virtual ICollection<DonacionDetalle> DonacionDetalles { get; set; }
        public virtual ICollection<HistorialDonacion> HistorialDonacions { get; set; }
    }
}
