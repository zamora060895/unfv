﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entidad.Repositorio.Models
{
    public partial class Modulo
    {
        public Modulo()
        {
            ModuloRols = new HashSet<ModuloRol>();
        }

        public int CodigoModulo { get; set; }
        public string NombreControlador { get; set; }
        public string NombreAccion { get; set; }
        public string NombreWeb { get; set; }
        public int? Orden { get; set; }
        public int? CodigoMenu { get; set; }
        public DateTime FechaCreacionRegistro { get; set; }
        public bool Habilitado { get; set; }

        public virtual Menu CodigoMenuNavigation { get; set; }
        public virtual ICollection<ModuloRol> ModuloRols { get; set; }
    }
}
