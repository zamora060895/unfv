﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Configuration;

#nullable disable

namespace Entidad.Repositorio.Models
{
    public partial class AsuntosAdministrativosDbContext : DbContext
    {
        public AsuntosAdministrativosDbContext()
        {
        }

        public AsuntosAdministrativosDbContext(DbContextOptions<AsuntosAdministrativosDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Area> Areas { get; set; }
        public virtual DbSet<Articulo> Articulos { get; set; }
        public virtual DbSet<Categorium> Categoria { get; set; }
        public virtual DbSet<Donacion> Donacions { get; set; }
        public virtual DbSet<DonacionArchivo> DonacionArchivos { get; set; }
        public virtual DbSet<DonacionDetalle> DonacionDetalles { get; set; }
        public virtual DbSet<Escuela> Escuelas { get; set; }
        public virtual DbSet<Estado> Estados { get; set; }
        public virtual DbSet<Facultad> Facultads { get; set; }
        public virtual DbSet<HistorialDonacion> HistorialDonacions { get; set; }
        public virtual DbSet<HistorialSolicitud> HistorialSolicituds { get; set; }
        public virtual DbSet<Menu> Menus { get; set; }
        public virtual DbSet<Modulo> Modulos { get; set; }
        public virtual DbSet<ModuloRol> ModuloRols { get; set; }
        public virtual DbSet<Rol> Rols { get; set; }
        public virtual DbSet<Solicitud> Solicituds { get; set; }
        public virtual DbSet<Usuario> Usuarios { get; set; }
        public virtual DbSet<UsuarioRol> UsuarioRols { get; set; }

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    if (!optionsBuilder.IsConfigured)
        //    {
        //        //optionsBuilder.UseSqlServer("Data Source=localhost;Initial Catalog=BD_AsuntosAdministrativos;User Id=sa;Password=sql2019;");
        //        //optionsBuilder.UseSqlServer("Data Source=DESKTOP-3GMRH2Q;Initial Catalog=BD_AsuntosAdministrativos;User Id=sa;Password=10581405;");
        //        //optionsBuilder.UseSqlServer("Data Source=DESKTOP-3GMRH2Q;Initial Catalog=BD_AsuntosAdministrativos;User Id=sa;Password=10581405;");
        //        optionsBuilder.UseSqlServer("Data Source=QDE1J;Initial Catalog=BD_AsuntosAdministrativos;User Id=sa;Password=elgatito5neko##;");
        //        //optionsBuilder.UseSqlServer("Data Source=WIN-KF1S3N5NK77;Initial Catalog=BD_AsuntosAdministrativos;User Id=gsanchez;Password=Sistemas1;");
        //        //optionsBuilder.UseSqlServer(@"Data Source=.\SQLLOCALHOST;Initial Catalog=BD_AsuntosAdministrativos;User Id=sa;Password=lodamania1F;");
        //        //optionsBuilder.UseSqlServer(@"Data Source=192.168.151.8,1450;Initial Catalog=BD_AsuntosAdministrativos;User Id=sa;Password=keralty2019;");
        //        //optionsBuilder.UseSqlServer(@"Data Source=WIN-SO8GDK9QBV0;Initial Catalog=BD_AsuntosAdministrativos;User Id=sa;Password=Sistemas1;"); //Walter
        //    }
        //}

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                IConfigurationRoot configuration = new ConfigurationBuilder()
                    .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                    .AddJsonFile("appsettings.json")
                    .Build();

                optionsBuilder.UseSqlServer(configuration.GetConnectionString("MiConexion"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Area>(entity =>
            {
                entity.HasKey(e => e.CodigoArea)
                    .HasName("PK__Area__72615A64FCD88AC9");

                entity.ToTable("Area", "Organization");

                entity.Property(e => e.CodigoArea).HasColumnName("codigo_area");

                entity.Property(e => e.CodigoEscuela).HasColumnName("codigo_escuela");

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("descripcion");

                entity.Property(e => e.Habilitado).HasColumnName("habilitado");

                entity.HasOne(d => d.CodigoEscuelaNavigation)
                    .WithMany(p => p.Areas)
                    .HasForeignKey(d => d.CodigoEscuela)
                    .HasConstraintName("FK__Area__codigo_esc__1F98B2C1");
            });

            modelBuilder.Entity<Articulo>(entity =>
            {
                entity.HasKey(e => e.CodigoArticulo)
                    .HasName("PK__Articulo__01435263F5DE80CF");

                entity.ToTable("Articulo", "Management");

                entity.Property(e => e.CodigoArticulo).HasColumnName("codigo_articulo");

                entity.Property(e => e.CodigoCategoria).HasColumnName("codigo_categoria");

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("descripcion");

                entity.Property(e => e.Habilitado).HasColumnName("habilitado");

                entity.HasOne(d => d.CodigoCategoriaNavigation)
                    .WithMany(p => p.Articulos)
                    .HasForeignKey(d => d.CodigoCategoria)
                    .HasConstraintName("FK__Articulo__codigo__17F790F9");
            });

            modelBuilder.Entity<Categorium>(entity =>
            {
                entity.HasKey(e => e.CodigoCategoria)
                    .HasName("PK__Categori__042DBC1D42BCA9BB");

                entity.ToTable("Categoria", "Management");

                entity.Property(e => e.CodigoCategoria).HasColumnName("codigo_categoria");

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(150)
                    .IsUnicode(false)
                    .HasColumnName("descripcion");

                entity.Property(e => e.Habilitado).HasColumnName("habilitado");
            });

            modelBuilder.Entity<Donacion>(entity =>
            {
                entity.HasKey(e => e.CodigoDonacion)
                    .HasName("PK__Donacion__6466235760D8C1CC");

                entity.ToTable("Donacion", "Management");

                entity.Property(e => e.CodigoDonacion).HasColumnName("codigo_donacion");

                entity.Property(e => e.CodigoEstado).HasColumnName("codigo_estado");

                entity.Property(e => e.CodigoUsuarioCreacion).HasColumnName("codigo_usuario_creacion");

                entity.Property(e => e.CodigoUsuarioEstudiante).HasColumnName("codigo_usuario_estudiante");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("fecha_creacion");

                entity.Property(e => e.FechaDonacion)
                    .HasColumnType("datetime")
                    .HasColumnName("fecha_donacion");

                entity.Property(e => e.MontoTotal)
                    .HasColumnType("decimal(12, 2)")
                    .HasColumnName("monto_total");

                entity.HasOne(d => d.CodigoEstadoNavigation)
                    .WithMany(p => p.Donacions)
                    .HasForeignKey(d => d.CodigoEstado)
                    .HasConstraintName("FK__Donacion__codigo__46B27FE2");

                entity.HasOne(d => d.CodigoUsuarioCreacionNavigation)
                    .WithMany(p => p.DonacionCodigoUsuarioCreacionNavigations)
                    .HasForeignKey(d => d.CodigoUsuarioCreacion)
                    .HasConstraintName("FK__Donacion__codigo__47A6A41B");

                entity.HasOne(d => d.CodigoUsuarioEstudianteNavigation)
                    .WithMany(p => p.DonacionCodigoUsuarioEstudianteNavigations)
                    .HasForeignKey(d => d.CodigoUsuarioEstudiante)
                    .HasConstraintName("FK__Donacion__codigo__45BE5BA9");
            });

            modelBuilder.Entity<DonacionArchivo>(entity =>
            {
                entity.HasKey(e => e.CodigoDonacionArchivo)
                    .HasName("PK__Donacion__56A2E95DDD6306AF");

                entity.ToTable("Donacion_Archivo", "Management");

                entity.Property(e => e.CodigoDonacionArchivo).HasColumnName("codigo_donacion_archivo");

                entity.Property(e => e.CodigoDonacion).HasColumnName("codigo_donacion");

                entity.Property(e => e.CodigoUsuarioCreacion).HasColumnName("codigo_usuario_creacion");

                entity.Property(e => e.Extension)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("extension");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("fecha_creacion");

                entity.Property(e => e.Habilitado).HasColumnName("habilitado");

                entity.Property(e => e.NombreArchivo)
                    .HasMaxLength(800)
                    .IsUnicode(false)
                    .HasColumnName("nombre_archivo");

                entity.Property(e => e.NumeroVoucher)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("numero_voucher");

                entity.Property(e => e.RutaArchivo)
                    .HasMaxLength(800)
                    .IsUnicode(false)
                    .HasColumnName("ruta_archivo");

                entity.Property(e => e.TipoArchivo)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("tipo_archivo");

                entity.Property(e => e.Nota)
                    .HasMaxLength(800)
                    .IsUnicode(false)
                    .HasColumnName("nota");

                entity.HasOne(d => d.CodigoDonacionNavigation)
                    .WithMany(p => p.DonacionArchivos)
                    .HasForeignKey(d => d.CodigoDonacion)
                    .HasConstraintName("FK__Donacion___codig__531856C7");

                entity.HasOne(d => d.CodigoUsuarioCreacionNavigation)
                    .WithMany(p => p.DonacionArchivos)
                    .HasForeignKey(d => d.CodigoUsuarioCreacion)
                    .HasConstraintName("FK__Donacion___codig__540C7B00");
            });

            modelBuilder.Entity<DonacionDetalle>(entity =>
            {
                entity.HasKey(e => e.CodigoDonacionDetalle)
                    .HasName("PK__Donacion__C2B40F9FF072A11D");

                entity.ToTable("Donacion_Detalle", "Management");

                entity.Property(e => e.CodigoDonacionDetalle).HasColumnName("codigo_donacion_detalle");

                entity.Property(e => e.CodigoDonacion).HasColumnName("codigo_donacion");

                entity.Property(e => e.CodigoSolicitud).HasColumnName("codigo_solicitud");

                entity.Property(e => e.Habilitado).HasColumnName("habilitado");

                entity.HasOne(d => d.CodigoDonacionNavigation)
                    .WithMany(p => p.DonacionDetalles)
                    .HasForeignKey(d => d.CodigoDonacion)
                    .HasConstraintName("FK__Donacion___codig__4A8310C6");

                entity.HasOne(d => d.CodigoSolicitudNavigation)
                    .WithMany(p => p.DonacionDetalles)
                    .HasForeignKey(d => d.CodigoSolicitud)
                    .HasConstraintName("FK__Donacion___codig__4B7734FF");
            });

            modelBuilder.Entity<Escuela>(entity =>
            {
                entity.HasKey(e => e.CodigoEscuela)
                    .HasName("PK__Escuela__0AAB9E5C3D22AF4B");

                entity.ToTable("Escuela", "Organization");

                entity.Property(e => e.CodigoEscuela).HasColumnName("codigo_escuela");

                entity.Property(e => e.CodigoFacultad).HasColumnName("codigo_facultad");

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("descripcion");

                entity.Property(e => e.Habilitado).HasColumnName("habilitado");

                entity.HasOne(d => d.CodigoFacultadNavigation)
                    .WithMany(p => p.Escuelas)
                    .HasForeignKey(d => d.CodigoFacultad)
                    .HasConstraintName("FK__Escuela__codigo___1CBC4616");
            });

            modelBuilder.Entity<Estado>(entity =>
            {
                entity.HasKey(e => e.CodigoEstado)
                    .HasName("PK__Estado__042DBC1D87F1C227");

                entity.ToTable("Estado", "Management");

                entity.Property(e => e.CodigoEstado).HasColumnName("codigo_estado");

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("descripcion");

                entity.Property(e => e.Habilitado).HasColumnName("habilitado");

                entity.Property(e => e.TipoEstado)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasColumnName("tipo_estado")
                    .IsFixedLength(true);
            });

            modelBuilder.Entity<Facultad>(entity =>
            {
                entity.HasKey(e => e.CodigoFacultad)
                    .HasName("PK__Facultad__68BA6863B9433DE1");

                entity.ToTable("Facultad", "Organization");

                entity.Property(e => e.CodigoFacultad).HasColumnName("codigo_facultad");

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("descripcion");

                entity.Property(e => e.Habilitado).HasColumnName("habilitado");
            });

            modelBuilder.Entity<HistorialDonacion>(entity =>
            {
                entity.HasKey(e => e.CodigoHistorialDonacion)
                    .HasName("PK__Historia__FF157D54F3311B8B");

                entity.ToTable("Historial_Donacion", "Management");

                entity.Property(e => e.CodigoHistorialDonacion).HasColumnName("codigo_historial_donacion");

                entity.Property(e => e.CodigoDonacion).HasColumnName("codigo_donacion");

                entity.Property(e => e.CodigoEstado).HasColumnName("codigo_estado");

                entity.Property(e => e.CodigoUsuarioCreacion).HasColumnName("codigo_usuario_creacion");

                entity.Property(e => e.Comentario)
                    .HasMaxLength(8000)
                    .IsUnicode(false)
                    .HasColumnName("comentario");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("fecha_creacion");

                entity.HasOne(d => d.CodigoDonacionNavigation)
                    .WithMany(p => p.HistorialDonacions)
                    .HasForeignKey(d => d.CodigoDonacion)
                    .HasConstraintName("FK__Historial__codig__4E53A1AA");

                entity.HasOne(d => d.CodigoEstadoNavigation)
                    .WithMany(p => p.HistorialDonacions)
                    .HasForeignKey(d => d.CodigoEstado)
                    .HasConstraintName("FK__Historial__codig__4F47C5E3");

                entity.HasOne(d => d.CodigoUsuarioCreacionNavigation)
                    .WithMany(p => p.HistorialDonacions)
                    .HasForeignKey(d => d.CodigoUsuarioCreacion)
                    .HasConstraintName("FK__Historial__codig__503BEA1C");
            });

            modelBuilder.Entity<HistorialSolicitud>(entity =>
            {
                entity.HasKey(e => e.CodigoHistorialSolicitud)
                    .HasName("PK__Historia__FB782E0A7C259637");

                entity.ToTable("Historial_Solicitud", "Management");

                entity.Property(e => e.CodigoHistorialSolicitud).HasColumnName("codigo_historial_solicitud");

                entity.Property(e => e.CodigoEstado).HasColumnName("codigo_estado");

                entity.Property(e => e.CodigoSolicitud).HasColumnName("codigo_solicitud");

                entity.Property(e => e.CodigoUsuarioCreacion).HasColumnName("codigo_usuario_creacion");

                entity.Property(e => e.Comentario)
                    .HasMaxLength(8000)
                    .IsUnicode(false)
                    .HasColumnName("comentario");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("fecha_creacion");

                entity.HasOne(d => d.CodigoEstadoNavigation)
                    .WithMany(p => p.HistorialSolicituds)
                    .HasForeignKey(d => d.CodigoEstado)
                    .HasConstraintName("FK__Historial__codig__2FCF1A8A");

                entity.HasOne(d => d.CodigoSolicitudNavigation)
                    .WithMany(p => p.HistorialSolicituds)
                    .HasForeignKey(d => d.CodigoSolicitud)
                    .HasConstraintName("FK__Historial__codig__2EDAF651");

                entity.HasOne(d => d.CodigoUsuarioCreacionNavigation)
                    .WithMany(p => p.HistorialSolicituds)
                    .HasForeignKey(d => d.CodigoUsuarioCreacion)
                    .HasConstraintName("FK__Historial__codig__30C33EC3");
            });

            modelBuilder.Entity<Menu>(entity =>
            {
                entity.HasKey(e => e.CodigoMenu)
                    .HasName("PK__Menu__EB9D53E8D8CAB1C6");

                entity.ToTable("Menu", "Setting");

                entity.Property(e => e.CodigoMenu).HasColumnName("codigo_menu");

                entity.Property(e => e.FechaCreacionRegistro)
                    .HasColumnType("datetime")
                    .HasColumnName("fecha_creacion_registro");

                entity.Property(e => e.Habilitado).HasColumnName("habilitado");

                entity.Property(e => e.Nombre)
                    .HasMaxLength(150)
                    .IsUnicode(false)
                    .HasColumnName("nombre");

                entity.Property(e => e.NombreWeb)
                    .HasMaxLength(150)
                    .IsUnicode(false)
                    .HasColumnName("nombre_web");

                entity.Property(e => e.Orden).HasColumnName("orden");
            });

            modelBuilder.Entity<Modulo>(entity =>
            {
                entity.HasKey(e => e.CodigoModulo)
                    .HasName("PK__Modulo__6DCE59A3ED9725DC");

                entity.ToTable("Modulo", "Setting");

                entity.Property(e => e.CodigoModulo).HasColumnName("codigo_modulo");

                entity.Property(e => e.CodigoMenu).HasColumnName("codigo_menu");

                entity.Property(e => e.FechaCreacionRegistro)
                    .HasColumnType("datetime")
                    .HasColumnName("fecha_creacion_registro");

                entity.Property(e => e.Habilitado).HasColumnName("habilitado");

                entity.Property(e => e.NombreAccion)
                    .HasMaxLength(150)
                    .IsUnicode(false)
                    .HasColumnName("nombre_accion");

                entity.Property(e => e.NombreControlador)
                    .HasMaxLength(150)
                    .IsUnicode(false)
                    .HasColumnName("nombre_controlador");

                entity.Property(e => e.NombreWeb)
                    .HasMaxLength(150)
                    .IsUnicode(false)
                    .HasColumnName("nombre_web");

                entity.Property(e => e.Orden).HasColumnName("orden");

                entity.HasOne(d => d.CodigoMenuNavigation)
                    .WithMany(p => p.Modulos)
                    .HasForeignKey(d => d.CodigoMenu)
                    .HasConstraintName("FK_Modulo_Menu");
            });

            modelBuilder.Entity<ModuloRol>(entity =>
            {
                entity.HasKey(e => e.CodigoModuloRol)
                    .HasName("PK__Modulo_R__2C786E4F93A8CFB0");

                entity.ToTable("Modulo_Rol", "Setting");

                entity.Property(e => e.CodigoModuloRol).HasColumnName("codigo_modulo_rol");

                entity.Property(e => e.CodigoModulo).HasColumnName("codigo_modulo");

                entity.Property(e => e.CodigoRol).HasColumnName("codigo_rol");

                entity.Property(e => e.FechaCreacionRegistro)
                    .HasColumnType("datetime")
                    .HasColumnName("fecha_creacion_registro");

                entity.Property(e => e.Habilitado).HasColumnName("habilitado");

                entity.HasOne(d => d.CodigoModuloNavigation)
                    .WithMany(p => p.ModuloRols)
                    .HasForeignKey(d => d.CodigoModulo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Modulo_Rol_Modulo");

                entity.HasOne(d => d.CodigoRolNavigation)
                    .WithMany(p => p.ModuloRols)
                    .HasForeignKey(d => d.CodigoRol)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Modulo_Rol_Rol");
            });

            modelBuilder.Entity<Rol>(entity =>
            {
                entity.HasKey(e => e.CodigoRol)
                    .HasName("PK__Rol__1F78389E7D5A67B0");

                entity.ToTable("Rol", "Setting");

                entity.Property(e => e.CodigoRol).HasColumnName("codigo_rol");

                entity.Property(e => e.FechaCreacionRegistro)
                    .HasColumnType("datetime")
                    .HasColumnName("fecha_creacion_registro");

                entity.Property(e => e.Habilitado).HasColumnName("habilitado");

                entity.Property(e => e.Nombre)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("nombre");
            });

            modelBuilder.Entity<Solicitud>(entity =>
            {
                entity.HasKey(e => e.CodigoSolicitud)
                    .HasName("PK__Solicitu__2ABAFA8AFF807A5E");

                entity.ToTable("Solicitud", "Management");

                entity.Property(e => e.CodigoSolicitud).HasColumnName("codigo_solicitud");

                entity.Property(e => e.CodigoArea).HasColumnName("codigo_area");

                entity.Property(e => e.CodigoArticulo).HasColumnName("codigo_articulo");

                entity.Property(e => e.CodigoEstado).HasColumnName("codigo_estado");

                entity.Property(e => e.CodigoUsuarioCreacion).HasColumnName("codigo_usuario_creacion");
                entity.Property(e => e.Cantidad).HasColumnName("cantidad");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("fecha_creacion");

                entity.Property(e => e.ValorAproximado)
                    .HasColumnType("decimal(12, 2)")
                    .HasColumnName("valor_aproximado");

                entity.HasOne(d => d.CodigoAreaNavigation)
                    .WithMany(p => p.Solicituds)
                    .HasForeignKey(d => d.CodigoArea)
                    .HasConstraintName("FK__Solicitud__codig__2A164134");

                entity.HasOne(d => d.CodigoArticuloNavigation)
                    .WithMany(p => p.Solicituds)
                    .HasForeignKey(d => d.CodigoArticulo)
                    .HasConstraintName("FK__Solicitud__codig__29221CFB");

                entity.HasOne(d => d.CodigoEstadoNavigation)
                    .WithMany(p => p.Solicituds)
                    .HasForeignKey(d => d.CodigoEstado)
                    .HasConstraintName("FK__Solicitud__codig__2B0A656D");

                entity.HasOne(d => d.CodigoUsuarioCreacionNavigation)
                    .WithMany(p => p.Solicituds)
                    .HasForeignKey(d => d.CodigoUsuarioCreacion)
                    .HasConstraintName("FK__Solicitud__codig__2BFE89A6");
            });

            modelBuilder.Entity<Usuario>(entity =>
            {
                entity.HasKey(e => e.CodigoUsuario)
                    .HasName("PK__Usuario__37F064A0ED0DE38F");

                entity.ToTable("Usuario", "Organization");

                entity.Property(e => e.CodigoUsuario).HasColumnName("codigo_usuario");

                entity.Property(e => e.ApellidoMaterno)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("apellido_materno");

                entity.Property(e => e.ApellidoPaterno)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("apellido_paterno");

                entity.Property(e => e.CodigoEstudiante)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("codigo_estudiante");

                entity.Property(e => e.CodigoUsuarioModificacion).HasColumnName("codigo_usuario_modificacion");

                entity.Property(e => e.Contrasenia)
                    .HasMaxLength(80)
                    .IsUnicode(false)
                    .HasColumnName("contrasenia");

                entity.Property(e => e.CorreoElectronico)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("correo_electronico");

                entity.Property(e => e.FechaCreacionRegistro)
                    .HasColumnType("datetime")
                    .HasColumnName("fecha_creacion_registro");

                entity.Property(e => e.FechaModificacionRegistro)
                    .HasColumnType("datetime")
                    .HasColumnName("fecha_modificacion_registro");

                entity.Property(e => e.Habilitado).HasColumnName("habilitado");

                entity.Property(e => e.Nombre)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("nombre");

                entity.Property(e => e.NumeroDocumento)
                    .HasMaxLength(11)
                    .IsUnicode(false)
                    .HasColumnName("numero_documento");

                entity.Property(e => e.Usuario1)
                    .HasMaxLength(25)
                    .IsUnicode(false)
                    .HasColumnName("usuario");
            });

            modelBuilder.Entity<UsuarioRol>(entity =>
            {
                entity.HasKey(e => e.CodigoUsarioRol)
                    .HasName("PK__Usuario___8A9CBECEC26D4725");

                entity.ToTable("Usuario_Rol", "Setting");

                entity.Property(e => e.CodigoUsarioRol).HasColumnName("codigo_usario_rol");

                entity.Property(e => e.CodigoRol).HasColumnName("codigo_rol");

                entity.Property(e => e.CodigoUsuario).HasColumnName("codigo_usuario");

                entity.Property(e => e.FechaCreacionRegistro)
                    .HasColumnType("datetime")
                    .HasColumnName("fecha_creacion_registro");

                entity.Property(e => e.Habilitado).HasColumnName("habilitado");

                entity.HasOne(d => d.CodigoRolNavigation)
                    .WithMany(p => p.UsuarioRols)
                    .HasForeignKey(d => d.CodigoRol)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Usuario_Rol_Rol");

                entity.HasOne(d => d.CodigoUsuarioNavigation)
                    .WithMany(p => p.UsuarioRols)
                    .HasForeignKey(d => d.CodigoUsuario)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Usuario_Rol_Usuario");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
