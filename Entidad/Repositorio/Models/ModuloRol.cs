﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entidad.Repositorio.Models
{
    public partial class ModuloRol
    {
        public int CodigoModuloRol { get; set; }
        public int CodigoModulo { get; set; }
        public int CodigoRol { get; set; }
        public DateTime FechaCreacionRegistro { get; set; }
        public bool Habilitado { get; set; }

        public virtual Modulo CodigoModuloNavigation { get; set; }
        public virtual Rol CodigoRolNavigation { get; set; }
    }
}
