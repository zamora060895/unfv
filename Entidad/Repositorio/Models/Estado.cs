﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entidad.Repositorio.Models
{
    public partial class Estado
    {
        public Estado()
        {
            Donacions = new HashSet<Donacion>();
            HistorialDonacions = new HashSet<HistorialDonacion>();
            HistorialSolicituds = new HashSet<HistorialSolicitud>();
            Solicituds = new HashSet<Solicitud>();
        }

        public int CodigoEstado { get; set; }
        public string Descripcion { get; set; }
        public string TipoEstado { get; set; }
        public bool? Habilitado { get; set; }

        public virtual ICollection<Donacion> Donacions { get; set; }
        public virtual ICollection<HistorialDonacion> HistorialDonacions { get; set; }
        public virtual ICollection<HistorialSolicitud> HistorialSolicituds { get; set; }
        public virtual ICollection<Solicitud> Solicituds { get; set; }
    }
}
