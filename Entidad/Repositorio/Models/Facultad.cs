﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entidad.Repositorio.Models
{
    public partial class Facultad
    {
        public Facultad()
        {
            Escuelas = new HashSet<Escuela>();
        }

        public int CodigoFacultad { get; set; }
        public string Descripcion { get; set; }
        public bool? Habilitado { get; set; }

        public virtual ICollection<Escuela> Escuelas { get; set; }
    }
}
