﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entidad.Repositorio.Models
{
    public partial class HistorialSolicitud
    {
        public int CodigoHistorialSolicitud { get; set; }
        public int? CodigoSolicitud { get; set; }
        public int? CodigoEstado { get; set; }
        public string Comentario { get; set; }
        public int? CodigoUsuarioCreacion { get; set; }
        public DateTime? FechaCreacion { get; set; }

        public virtual Estado CodigoEstadoNavigation { get; set; }
        public virtual Solicitud CodigoSolicitudNavigation { get; set; }
        public virtual Usuario CodigoUsuarioCreacionNavigation { get; set; }
    }
}
