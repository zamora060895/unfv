﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entidad.Repositorio.Models
{
    public partial class Rol
    {
        public Rol()
        {
            ModuloRols = new HashSet<ModuloRol>();
            UsuarioRols = new HashSet<UsuarioRol>();
        }

        public int CodigoRol { get; set; }
        public string Nombre { get; set; }
        public DateTime FechaCreacionRegistro { get; set; }
        public bool Habilitado { get; set; }

        public virtual ICollection<ModuloRol> ModuloRols { get; set; }
        public virtual ICollection<UsuarioRol> UsuarioRols { get; set; }
    }
}
