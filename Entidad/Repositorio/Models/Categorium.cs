﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entidad.Repositorio.Models
{
    public partial class Categorium
    {
        public Categorium()
        {
            Articulos = new HashSet<Articulo>();
        }

        public int CodigoCategoria { get; set; }
        public string Descripcion { get; set; }
        public bool? Habilitado { get; set; }

        public virtual ICollection<Articulo> Articulos { get; set; }
    }
}
