﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entidad.Repositorio.Models
{
    public partial class Area
    {
        public Area()
        {
            Solicituds = new HashSet<Solicitud>();
        }

        public int CodigoArea { get; set; }
        public string Descripcion { get; set; }
        public int? CodigoEscuela { get; set; }
        public bool? Habilitado { get; set; }

        public virtual Escuela CodigoEscuelaNavigation { get; set; }
        public virtual ICollection<Solicitud> Solicituds { get; set; }
    }
}
