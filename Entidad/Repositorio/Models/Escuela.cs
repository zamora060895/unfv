﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entidad.Repositorio.Models
{
    public partial class Escuela
    {
        public Escuela()
        {
            Areas = new HashSet<Area>();
        }

        public int CodigoEscuela { get; set; }
        public string Descripcion { get; set; }
        public int? CodigoFacultad { get; set; }
        public bool? Habilitado { get; set; }

        public virtual Facultad CodigoFacultadNavigation { get; set; }
        public virtual ICollection<Area> Areas { get; set; }
    }
}
