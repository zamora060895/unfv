﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entidad.Repositorio.Models
{
    public partial class Articulo
    {
        public Articulo()
        {
            Solicituds = new HashSet<Solicitud>();
        }

        public int CodigoArticulo { get; set; }
        public string Descripcion { get; set; }
        public int? CodigoCategoria { get; set; }
        public bool? Habilitado { get; set; }

        public virtual Categorium CodigoCategoriaNavigation { get; set; }
        public virtual ICollection<Solicitud> Solicituds { get; set; }
    }
}
