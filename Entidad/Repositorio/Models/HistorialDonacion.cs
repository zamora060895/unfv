﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entidad.Repositorio.Models
{
    public partial class HistorialDonacion
    {
        public int CodigoHistorialDonacion { get; set; }
        public int? CodigoDonacion { get; set; }
        public int? CodigoEstado { get; set; }
        public string Comentario { get; set; }
        public int? CodigoUsuarioCreacion { get; set; }
        public DateTime? FechaCreacion { get; set; }

        public virtual Donacion CodigoDonacionNavigation { get; set; }
        public virtual Estado CodigoEstadoNavigation { get; set; }
        public virtual Usuario CodigoUsuarioCreacionNavigation { get; set; }
    }
}
