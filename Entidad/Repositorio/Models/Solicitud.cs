﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entidad.Repositorio.Models
{
    public partial class Solicitud
    {
        public Solicitud()
        {
            DonacionDetalles = new HashSet<DonacionDetalle>();
            HistorialSolicituds = new HashSet<HistorialSolicitud>();
        }

        public int CodigoSolicitud { get; set; }
        public int? CodigoArticulo { get; set; }
        public int? CodigoArea { get; set; }
        public decimal? ValorAproximado { get; set; }
        public int? CodigoEstado { get; set; }
        public int? CodigoUsuarioCreacion { get; set; }
        public DateTime? FechaCreacion { get; set; }
        public int? Cantidad { get; set; }

        public virtual Area CodigoAreaNavigation { get; set; }
        public virtual Articulo CodigoArticuloNavigation { get; set; }
        public virtual Estado CodigoEstadoNavigation { get; set; }
        public virtual Usuario CodigoUsuarioCreacionNavigation { get; set; }
        public virtual ICollection<DonacionDetalle> DonacionDetalles { get; set; }
        public virtual ICollection<HistorialSolicitud> HistorialSolicituds { get; set; }
    }
}
