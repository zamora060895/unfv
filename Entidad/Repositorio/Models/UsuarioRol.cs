﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entidad.Repositorio.Models
{
    public partial class UsuarioRol
    {
        public int CodigoUsarioRol { get; set; }
        public int CodigoUsuario { get; set; }
        public int CodigoRol { get; set; }
        public DateTime FechaCreacionRegistro { get; set; }
        public bool Habilitado { get; set; }

        public virtual Rol CodigoRolNavigation { get; set; }
        public virtual Usuario CodigoUsuarioNavigation { get; set; }
    }
}
