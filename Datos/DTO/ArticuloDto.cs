﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.DTO
{
    public class ArticuloDto
    {
        public int CodigoArticulo { get; set; }
        public string Descripcion { get; set; }
        public int CodigoCategoria { get; set; }
        public string Estado { get; set; }
        public string DescripcionCategoria { get; set; }
    }
}
