﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.DTO
{
    public class DonacionDto
    {
        public int CodigoDonacion { get; set; }
        public string FechaDonacion { get; set; }
        public string Estudiante { get; set; }
        public string Estado { get; set; }
        public decimal MontoTotal { get; set; }
        public string CodigoEstudiante { get; set; }
        public string Usuario { get; set; }
        public int CodigoUsuarioEstudiante { get; set; }
        public List<SolicitudList> SolicitudList { get; set; }
        public List<FileDto> DocumentList { get; set; }
        public int CodigoUsuarioCreacion { get; set; }
        public string NumeroDocumento { get; set; }
        public int flagHasBoleta { get; set; }
        
        public int Cantidad { get; set; }
    }

    public class SolicitudList
    {
        public decimal? CodigoSolicitud { get; set; }
        public decimal? Cantidad { get; set; }
        
    }
}
