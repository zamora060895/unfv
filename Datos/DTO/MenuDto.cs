﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.DTO
{
    public class MenuDto
    {
        public decimal CodigoMenu { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public int Order { get; set; }
        public string SystemName { get; set; }

        public List<ModuleDto> Module { get; set; }
    }
}
