﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.DTO
{
    public class PDFBoletaDto
    {
        public string numeroVoucher { get; set; }
        public string comentario { get; set; }
    }
}
