﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.DTO
{
    public class HistorialSolicitudDto
    {
        public string estado { get; set; }
        public string comentario { get; set; }
        public string usuario { get; set; }
        public string fechacreacion { get; set; }
    }
}
