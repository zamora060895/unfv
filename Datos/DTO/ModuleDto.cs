﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.DTO
{
    public class ModuleDto
    {        
            public decimal CodigoModulo { get; set; }
            public string ControllerName { get; set; }
            public string ActionName { get; set; }
            public string DisplayName { get; set; }
            public int Order { get; set; }
            public decimal MenuId { get; set; }
            public decimal? SubMenuId { get; set; }
    }
}
