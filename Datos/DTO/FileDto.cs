﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.DTO
{
    public class FileDto
    {
        public decimal CodigoFile { get; set; }
        public string RutaArchivo { get; set; }
        public string NombreArchivo { get; set; }
        public string Extension { get; set; }
        public string TipoArchivo { get; set; }
        public string NumeroVoucher { get; set; }
        public string Nota { get; set; }
    }
}
