﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.DTO
{
    public class ResultDto
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
        public bool Success { get; set; }
    }
}
