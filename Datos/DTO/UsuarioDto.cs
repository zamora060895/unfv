﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.DTO
{
    public class UsuarioDto
    {
        public int CodigoUsuario { get; set; }
        public string Nombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string NumeroDocumento { get; set; }
        public string Login { get; set; }
        public string Contrasenia { get; set; }
        public string Estado { get; set; }
        public string CorreoElectronico { get; set; }
        public string CodigoEstudiante { get; set; }
        public bool Habilitado { get; set; }
        public bool Administrador { get; set; }
        public bool OperadorSolicitudes { get; set; }
        public bool OperadorDonaciones { get; set; }

    }
}
