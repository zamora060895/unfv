﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.DTO
{
    public class AreaDto
    {
        public int CodigoArea { get; set; }
        public string Descripcion { get; set; }
        public int CodigoEscuela { get; set; }
        public string Estado { get; set; }
        public string DescripcionEscuela { get; set; }
    }
}
