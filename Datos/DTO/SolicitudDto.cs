﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.DTO
{
    public class SolicitudDto
    {
        public int CodigoSolicitud { get; set; }
        public int CodigoArticulo { get; set; }
        public int CodigoArea { get; set; }
        public decimal ValorAproximado { get; set; }        
        public string DescripcionArticulo { get; set; }
        public string DescripcionArea { get; set; }
        public string DescripcionEscuela { get; set; }
        public string DescripcionFacultad { get; set; }
        public string Estado { get; set; }
        public int Cantidad { get; set; }
        public string FechaSolicitud { get; set; }
        public int CodigoEscuela { get; set; }
        public int CodigoFacultad { get; set; }
        public string CodigoDonacion{ get; set; }

        public int TotalCantidadDonado{ get; set; }
    }
}
