﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.DTO
{
    public class FileDownLoadModel
    {
        public string FileBase64 { get; set; }
        public string FileName { get; set; }
    }
}
