﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.DTO
{
    public class PDFArticuloDto
    {
        public int cantidad { get; set; }
        public string Descripcion { get; set; }
    }
}
