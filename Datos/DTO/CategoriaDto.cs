﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.DTO
{
    public class CategoriaDto
    {
        public int CodigoCategoria { get; set; }
        public string Descripcion { get; set; }
        public string Estado { get; set; }
    }
}
