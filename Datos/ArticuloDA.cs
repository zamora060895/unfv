﻿using System;
using Entidad.Repositorio.Models;
using System.Data.SqlClient;
using System.Linq;
using System.Transactions;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Datos.DTO;
using Microsoft.EntityFrameworkCore;

namespace Datos
{
    public class ArticuloDA
    {
        public async Task<DataCollection<ArticuloDto>> GetAllArticuloPaginate(string descripcion, int codigocategoria, string page, string size)
        {
            using (var bdAsuntosAdministrativos = new AsuntosAdministrativosDbContext())
            {
                try
                {
                    var ArticuloList = from articulo in bdAsuntosAdministrativos.Articulos
                                   join categoria in bdAsuntosAdministrativos.Categoria on articulo.CodigoCategoria equals categoria.CodigoCategoria
                                   select new
                                   {
                                       codigoArticulo = articulo.CodigoArticulo,
                                       descripcion = articulo.Descripcion,
                                       habilitado = articulo.Habilitado,
                                       codigocategoria = articulo.CodigoCategoria,
                                       descripcioncategoria = categoria.Descripcion
                                   };

                    ArticuloList = ArticuloList.OrderByDescending(x => x.codigoArticulo);

                    if (descripcion != "" && descripcion != null)
                    {
                        ArticuloList = ArticuloList.Where(x => x.descripcion.Contains(descripcion));
                    }

                    if (codigocategoria != 0)
                    {
                        ArticuloList = ArticuloList.Where(x => x.codigocategoria == codigocategoria);
                    }

                    var resultList = await ArticuloList.GetPagedAsync(Convert.ToInt32(page), Convert.ToInt32(size));

                    DataCollection<ArticuloDto> articuloDto = new DataCollection<ArticuloDto>()
                    {
                        Page = resultList.Page,
                        Pages = resultList.Pages,
                        Total = resultList.Total,
                        Items = resultList.Items.Select(x => new ArticuloDto()
                        {
                            CodigoArticulo = x.codigoArticulo,
                            Descripcion = x.descripcion,
                            Estado = x.habilitado == true ? "Activo" : "Inactivo",
                            DescripcionCategoria = x.descripcioncategoria
                        })
                    };

                    return articuloDto;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public async Task<ResultDto> InsertArticulo(Articulo objArticulo)
        {
            decimal ArticuloId = objArticulo.CodigoArticulo;

            using (var bdAsuntosAdministrativos = new AsuntosAdministrativosDbContext())
            {
                var Articulo_ = await bdAsuntosAdministrativos.Articulos.Where(x => x.CodigoArticulo == ArticuloId).FirstOrDefaultAsync();
                using TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

                if (Articulo_ != null)
                {
                    try
                    {
                        Articulo_.Descripcion = objArticulo.Descripcion;
                        Articulo_.Habilitado = objArticulo.Habilitado;
                        Articulo_.CodigoCategoria = objArticulo.CodigoCategoria;
                        bdAsuntosAdministrativos.Articulos.Attach(Articulo_);
                        await bdAsuntosAdministrativos.SaveChangesAsync();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                else
                {
                    Articulo articulo = new Articulo()
                    {
                        Descripcion = objArticulo.Descripcion,
                        Habilitado = objArticulo.Habilitado,
                        CodigoCategoria = objArticulo.CodigoCategoria
                    };

                    try
                    {
                        bdAsuntosAdministrativos.Articulos.Add(articulo);
                        await bdAsuntosAdministrativos.SaveChangesAsync();
                        ArticuloId = articulo.CodigoArticulo;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

                scope.Complete();
            }

            ResultDto eobj = new ResultDto();
            eobj.Id = Convert.ToInt32(ArticuloId);
            eobj.Descripcion = "Well done";
            eobj.Success = true;
            return eobj;
        }

        public async Task<ArticuloDto> GetArticuloById(decimal codigoArticulo)
        {
            decimal ArticuloId = codigoArticulo;
            ArticuloDto articuloDto = new ArticuloDto();
            using (var bdAsuntosAdministrativos = new AsuntosAdministrativosDbContext())
            {
                var Articulo_ = await bdAsuntosAdministrativos.Articulos.Where(x => x.CodigoArticulo == ArticuloId).FirstOrDefaultAsync();
                if (Articulo_ != null)
                {
                    articuloDto.CodigoCategoria = Convert.ToInt32(Articulo_.CodigoCategoria);
                    articuloDto.Descripcion = Articulo_.Descripcion;
                }
            }
            return articuloDto;
        }

        public async Task<ResultDto> ChangeStatusArticulo(Articulo objArticulo)
        {
            decimal ArticuloId = objArticulo.CodigoArticulo;

            using (var bdAsuntosAdministrativos = new AsuntosAdministrativosDbContext())
            {
                var Articulo_ = await bdAsuntosAdministrativos.Articulos.Where(x => x.CodigoArticulo == ArticuloId).FirstOrDefaultAsync();
                using TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

                if (Articulo_ != null)
                {
                    try
                    {
                        Articulo_.Habilitado = objArticulo.Habilitado;
                        bdAsuntosAdministrativos.Articulos.Attach(Articulo_);
                        await bdAsuntosAdministrativos.SaveChangesAsync();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                scope.Complete();
            }

            ResultDto eobj = new ResultDto();
            eobj.Id = Convert.ToInt32(ArticuloId);
            eobj.Descripcion = "Well done";
            eobj.Success = true;
            return eobj;
        }

        public async Task<List<ArticuloDto>> GetAllArticulo()
        {
            using (var bdAsuntosAdministrativos = new AsuntosAdministrativosDbContext())
            {
                try
                {
                    var ArticuloList = await bdAsuntosAdministrativos.Articulos.Where(x => x.Habilitado == true).ToListAsync();

                    return ArticuloList.Select(x => new ArticuloDto()
                    {
                        CodigoArticulo = x.CodigoArticulo,
                        Descripcion = x.Descripcion,
                        Estado = x.Habilitado == true ? "Activo" : "Inactivo"
                    }).ToList();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

    }
}
