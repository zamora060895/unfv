﻿using System;
using Entidad.Repositorio.Models;
using System.Data.SqlClient;
using System.Linq;
using System.Transactions;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Datos.DTO;
using Microsoft.EntityFrameworkCore;
namespace Datos
{
    public class SolicitudDA
    {
        public async Task<DataCollection<SolicitudDto>> GetAllSolicitudPaginate(string creationDateFrom, string creationDateTo, int nrosolicitud, int codigofacultad, int codigoescuela, int codigoarea, int codigoestado, string page, string size)
        {
            using (var bdAsuntosAdministrativos = new AsuntosAdministrativosDbContext())
            {
                try
                {
                    var dateFrom = Convert.ToDateTime(creationDateFrom);
                    var dateTo = Convert.ToDateTime(creationDateTo).AddHours(24).AddMinutes(-1);

                    var SolicitudList = from solicitud in bdAsuntosAdministrativos.Solicituds
                                        join estadoSolicitud in bdAsuntosAdministrativos.Estados on solicitud.CodigoEstado equals estadoSolicitud.CodigoEstado
                                        join articulo in bdAsuntosAdministrativos.Articulos on solicitud.CodigoArticulo equals articulo.CodigoArticulo
                                        join area in bdAsuntosAdministrativos.Areas on solicitud.CodigoArea equals area.CodigoArea
                                        join escuela in bdAsuntosAdministrativos.Escuelas on area.CodigoEscuela equals escuela.CodigoEscuela
                                        join facultad in bdAsuntosAdministrativos.Facultads on escuela.CodigoFacultad equals facultad.CodigoFacultad
                                        join donaciondetalle in bdAsuntosAdministrativos.DonacionDetalles.Where(d => d.Habilitado == true) on solicitud.CodigoSolicitud equals donaciondetalle.CodigoSolicitud
                                        into gj 
                                        from subDonaci in gj.DefaultIfEmpty()
                                        join donacion in bdAsuntosAdministrativos.Donacions on subDonaci.CodigoDonacion equals donacion.CodigoDonacion
                                        into dj
                                        from Donaci in dj.DefaultIfEmpty()
                                        where solicitud.FechaCreacion >= dateFrom && solicitud.FechaCreacion <= dateTo
                                        select new
                                        {
                                            codigoSolicitud = solicitud.CodigoSolicitud,
                                            descripcionArea = facultad.Descripcion + " | " + escuela.Descripcion + " | " + area.Descripcion,
                                            descripcionArticulo = articulo.Descripcion,
                                            estado = estadoSolicitud.Descripcion,
                                            codigoescuela = escuela.CodigoEscuela,
                                            codigoarea = area.CodigoArea,
                                            codigofacultad = facultad.CodigoFacultad,
                                            codigoestado = estadoSolicitud.CodigoEstado,
                                            valoraproximado = solicitud.ValorAproximado,
                                            cantidad = solicitud.Cantidad,
                                            fechasolicitud = solicitud.FechaCreacion,
                                            codigoDonacion = Donaci == null ? "": Donaci.CodigoEstado == 8 ? "" : subDonaci.CodigoDonacion.ToString()
                                        };
                    
                    SolicitudList = SolicitudList.OrderByDescending(x => x.codigoSolicitud);

                    if (codigofacultad != 0)
                    {
                        SolicitudList = SolicitudList.Where(x => x.codigofacultad == codigofacultad);
                    }

                    if (codigoescuela != 0)
                    {
                        SolicitudList = SolicitudList.Where(x => x.codigoescuela == codigoescuela);
                    }

                    if (codigoarea != 0)
                    {
                        SolicitudList = SolicitudList.Where(x => x.codigoarea == codigoarea);
                    }

                    if (codigoestado != 0)
                    {
                        SolicitudList = SolicitudList.Where(x => x.codigoestado == codigoestado);
                    }

                    if (nrosolicitud != 0)
                    {
                        SolicitudList = SolicitudList.Where(x => x.codigoSolicitud == nrosolicitud);
                    }

                    var resultList = await SolicitudList.GetPagedAsync(Convert.ToInt32(page), Convert.ToInt32(size));

                    DataCollection<SolicitudDto> solicitudDto = new DataCollection<SolicitudDto>()
                    {
                        Page = resultList.Page,
                        Pages = resultList.Pages,
                        Total = resultList.Total,
                        Items = resultList.Items.Select(x => new SolicitudDto()
                        {
                            CodigoSolicitud = x.codigoSolicitud,
                            DescripcionArea = x.descripcionArea,
                            Estado = x.estado,
                            DescripcionArticulo = x.descripcionArticulo,
                            ValorAproximado = Convert.ToDecimal(x.valoraproximado),
                            Cantidad = Convert.ToInt32(x.cantidad),
                            FechaSolicitud = Convert.ToDateTime(x.fechasolicitud).ToString("dd/MM/yyyy HH:mm:ss"),
                            CodigoDonacion = x.codigoDonacion == null ? "" : x.codigoDonacion.ToString()
                        })
                    };
                    
                    return solicitudDto;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public async Task<ResultDto> InsertSolicitud(Solicitud objSolicitud)
        {
            decimal SolicitudId = objSolicitud.CodigoSolicitud;

            using (var bdAsuntosAdministrativos = new AsuntosAdministrativosDbContext())
            {
                var Solicitud_ = await bdAsuntosAdministrativos.Solicituds.Where(x => x.CodigoSolicitud == SolicitudId).FirstOrDefaultAsync();
                using TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

                if (Solicitud_ != null)
                {
                    try
                    {
                        Solicitud_.CodigoArea = objSolicitud.CodigoArea;
                        Solicitud_.CodigoArticulo = objSolicitud.CodigoArticulo;
                        Solicitud_.ValorAproximado = objSolicitud.ValorAproximado;
                        bdAsuntosAdministrativos.Solicituds.Attach(Solicitud_);
                        await bdAsuntosAdministrativos.SaveChangesAsync();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                else
                {
                    Solicitud solicitud = new Solicitud()
                    {
                        CodigoArea = objSolicitud.CodigoArea,
                        CodigoArticulo = objSolicitud.CodigoArticulo,
                        CodigoEstado = 1,
                        ValorAproximado = objSolicitud.ValorAproximado,
                        Cantidad = objSolicitud.Cantidad,
                        CodigoUsuarioCreacion = objSolicitud.CodigoUsuarioCreacion,
                        FechaCreacion = DateTime.Now
                    };

                    try
                    {
                        bdAsuntosAdministrativos.Solicituds.Add(solicitud);
                        await bdAsuntosAdministrativos.SaveChangesAsync();
                        SolicitudId = solicitud.CodigoSolicitud;

                        HistorialSolicitud historiaSolicitud = new HistorialSolicitud()
                        {
                            CodigoSolicitud = Convert.ToInt32(SolicitudId),
                            Comentario = "Solicitud Nueva",
                            CodigoEstado = 1,
                            FechaCreacion = DateTime.Now,
                            CodigoUsuarioCreacion = objSolicitud.CodigoUsuarioCreacion,
                        };
                        decimal historiaSolicitudId = 0;
                        bdAsuntosAdministrativos.HistorialSolicituds.Add(historiaSolicitud);
                        await bdAsuntosAdministrativos.SaveChangesAsync();
                        historiaSolicitudId = historiaSolicitud.CodigoHistorialSolicitud;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

                scope.Complete();
            }

            ResultDto eobj = new ResultDto();
            eobj.Id = Convert.ToInt32(SolicitudId);
            eobj.Descripcion = "Well done";
            eobj.Success = true;
            return eobj;
        }

        public async Task<SolicitudDto> GetSolicitudById(decimal codigoSolicitud)
        {
            decimal SolicitudId = codigoSolicitud;
            SolicitudDto solicitudDto = new SolicitudDto();
            using (var bdAsuntosAdministrativos = new AsuntosAdministrativosDbContext())
            {
                //var Solicitud_ = await bdAsuntosAdministrativos.Solicituds.Where(x => x.CodigoSolicitud == SolicitudId).FirstOrDefaultAsync();

                var Solicitud_ = from solicitud in bdAsuntosAdministrativos.Solicituds.Where(x => x.CodigoSolicitud == SolicitudId)
                                 join area in bdAsuntosAdministrativos.Areas
                                 on solicitud.CodigoArea equals area.CodigoArea
                                 join escuela in bdAsuntosAdministrativos.Escuelas
                                 on area.CodigoEscuela equals escuela.CodigoEscuela
                                 join facultad in bdAsuntosAdministrativos.Facultads
                                 on escuela.CodigoFacultad equals facultad.CodigoFacultad
                                 join articulo in bdAsuntosAdministrativos.Articulos
                                 on solicitud.CodigoArticulo equals articulo.CodigoArticulo
                                 select new
                                 {
                                     Cantidad = solicitud.Cantidad,
                                     CodigoArea = solicitud.CodigoArea,
                                     CodigoArticulo = solicitud.CodigoArticulo,
                                     ValorAproximado = solicitud.ValorAproximado,
                                     CodigoEscuela = area.CodigoEscuela,
                                     CodigoFacultad = escuela.CodigoFacultad,
                                     DescripcionArticulo = articulo.Descripcion,
                                     DescripcionArea = area.Descripcion,
                                     DescripcionEscuela = escuela.Descripcion,
                                     DescripcionFacultad = facultad.Descripcion
                                 };

                var result = await Solicitud_.FirstOrDefaultAsync();

                if (result != null)
                {
                    solicitudDto.Cantidad = Convert.ToInt32(result.Cantidad);
                    solicitudDto.CodigoArea = Convert.ToInt32(result.CodigoArea);
                    solicitudDto.CodigoArticulo = Convert.ToInt32(result.CodigoArticulo);
                    solicitudDto.ValorAproximado = Convert.ToDecimal(result.ValorAproximado);
                    solicitudDto.CodigoEscuela = Convert.ToInt32(result.CodigoEscuela);
                    solicitudDto.CodigoFacultad = Convert.ToInt32(result.CodigoFacultad);

                    solicitudDto.DescripcionArticulo = result.DescripcionArticulo;
                    solicitudDto.DescripcionArea = result.DescripcionArea;
                    solicitudDto.DescripcionEscuela = result.DescripcionEscuela;
                    solicitudDto.DescripcionFacultad = result.DescripcionFacultad;
                }
            }
            return solicitudDto;
        }

        public async Task<ResultDto> ChangeStatusSolicitud(Solicitud objSolicitud)
        {
            decimal SolicitudId = objSolicitud.CodigoSolicitud;

            using (var bdAsuntosAdministrativos = new AsuntosAdministrativosDbContext())
            {
                var Solicitud_ = await bdAsuntosAdministrativos.Solicituds.Where(x => x.CodigoSolicitud == SolicitudId).FirstOrDefaultAsync();
                using TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

                if (Solicitud_ != null)
                {
                    try
                    {
                        Solicitud_.CodigoEstado = objSolicitud.CodigoEstado;
                        bdAsuntosAdministrativos.Solicituds.Attach(Solicitud_);
                        await bdAsuntosAdministrativos.SaveChangesAsync();
                        string comentario = "";
                        if (objSolicitud.CodigoEstado == 4)
                        {
                            comentario = "Solicitud Recibida";
                        }
                        else
                        {
                            comentario = "Solicitud Rechazada";
                        }
                        HistorialSolicitud historiaSolicitud = new HistorialSolicitud()
                        {
                            CodigoSolicitud = Convert.ToInt32(SolicitudId),
                            Comentario = comentario,
                            CodigoEstado = objSolicitud.CodigoEstado,
                            FechaCreacion = DateTime.Now,
                            CodigoUsuarioCreacion = objSolicitud.CodigoUsuarioCreacion
                        };
                        decimal historiaSolicitudId = 0;
                        bdAsuntosAdministrativos.HistorialSolicituds.Add(historiaSolicitud);
                        await bdAsuntosAdministrativos.SaveChangesAsync();
                        historiaSolicitudId = historiaSolicitud.CodigoHistorialSolicitud;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                scope.Complete();
            }

            ResultDto eobj = new ResultDto();
            eobj.Id = Convert.ToInt32(SolicitudId);
            eobj.Descripcion = "Well done";
            eobj.Success = true;
            return eobj;
        }

        public async Task<List<EstadoDto>> GetAllEstadoSolicitud()
        {
            using (var bdAsuntosAdministrativos = new AsuntosAdministrativosDbContext())
            {
                try
                {
                    var EstadoList = await bdAsuntosAdministrativos.Estados.Where(x => x.Habilitado == true && x.TipoEstado == "S").ToListAsync();

                    return EstadoList.Select(x => new EstadoDto()
                    {
                        CodigoEstado = x.CodigoEstado,
                        Descripcion = x.Descripcion,
                    }).ToList();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public async Task<DataCollection<HistorialSolicitudDto>> GetAllSolicitudTrackingPaginate(string codigosolicitud, string page, string size)
        {
            using (var bdAsuntosAdministrativos = new AsuntosAdministrativosDbContext())
            {
                try
                {
                    int codigoSolicitud_ = Convert.ToInt32(codigosolicitud);

                    var trackingSolicitudList = from historialsolicitud in bdAsuntosAdministrativos.HistorialSolicituds
                                                where historialsolicitud.CodigoSolicitud == codigoSolicitud_
                                                join estadoSolicitud in bdAsuntosAdministrativos.Estados on historialsolicitud.CodigoEstado equals estadoSolicitud.CodigoEstado
                                                join usuario in bdAsuntosAdministrativos.Usuarios on historialsolicitud.CodigoUsuarioCreacion equals usuario.CodigoUsuario
                                                select new
                                                {
                                                    codigoHistorial = historialsolicitud.CodigoHistorialSolicitud,
                                                    estado = estadoSolicitud.Descripcion,
                                                    usuario = usuario.Usuario1,
                                                    comentario = historialsolicitud.Comentario,
                                                    fechaSolicitud = historialsolicitud.FechaCreacion
                                                };

                    trackingSolicitudList = trackingSolicitudList.OrderByDescending(x => x.codigoHistorial);
                    var resultList = await trackingSolicitudList.GetPagedAsync(Convert.ToInt32(page), Convert.ToInt32(size));
                    DataCollection<HistorialSolicitudDto> historiasolicitudDto = new DataCollection<HistorialSolicitudDto>()
                    {
                        Items = resultList.Items.Select(x => new HistorialSolicitudDto()
                        {
                            usuario = x.usuario,
                            estado = x.estado,
                            comentario = x.comentario,
                            fechacreacion = Convert.ToDateTime(x.fechaSolicitud).ToString("dd/MM/yyyy HH:mm:ss")
                        })
                    };

                    return historiasolicitudDto;

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public async Task<DataCollection<SolicitudDto>> GetAllSolicitudPendientePaginate(int nrosolicitud, string page, string size)
        {
            using (var bdAsuntosAdministrativos = new AsuntosAdministrativosDbContext())
            {
                try
                {

                    var SolicitudList = from solicitud in bdAsuntosAdministrativos.Solicituds
                                                  join estadoSolicitud in bdAsuntosAdministrativos.Estados
                                                  on solicitud.CodigoEstado equals estadoSolicitud.CodigoEstado
                                                  join articulo in bdAsuntosAdministrativos.Articulos
                                                  on solicitud.CodigoArticulo equals articulo.CodigoArticulo
                                                  join area in bdAsuntosAdministrativos.Areas
                                                  on solicitud.CodigoArea equals area.CodigoArea
                                                  join escuela in bdAsuntosAdministrativos.Escuelas
                                                  on area.CodigoEscuela equals escuela.CodigoEscuela
                                                  join facultad in bdAsuntosAdministrativos.Facultads
                                                  on escuela.CodigoFacultad equals facultad.CodigoFacultad
                                                  where solicitud.CodigoEstado == 1 || solicitud.CodigoEstado == 9  
                                        select new
                                                  {
                                                      codigoSolicitud = solicitud.CodigoSolicitud,
                                                      descripcionArea = facultad.Descripcion + " | " + escuela.Descripcion + " | " + area.Descripcion,
                                                      descripcionArticulo = articulo.Descripcion,
                                                      estado = estadoSolicitud.Descripcion,
                                                      codigoescuela = escuela.CodigoEscuela,
                                                      codigoarea = area.CodigoArea,
                                                      codigofacultad = facultad.CodigoFacultad,
                                                      codigoestado = estadoSolicitud.CodigoEstado,
                                                      valoraproximado = solicitud.ValorAproximado,
                                                      cantidad = solicitud.Cantidad,
                                                      fechasolicitud = solicitud.FechaCreacion,
                                                  } into solicitudConTotal
                                                  join donacionDetalle in bdAsuntosAdministrativos.DonacionDetalles
                                                  on solicitudConTotal.codigoSolicitud equals donacionDetalle.CodigoSolicitud
                                                  into solicitudDonacionGroup
                                                  from donacion in solicitudDonacionGroup.DefaultIfEmpty()
                                                  group donacion by solicitudConTotal into g
                                                  select new
                                                  {
                                                      Solicitud = g.Key,
                                                      TotalCantidadDonado = g.Sum(x => x != null ? x.Cantidad : 0)
                                                  };

                    SolicitudList = SolicitudList.OrderByDescending(x => x.Solicitud.codigoSolicitud);

                    if (nrosolicitud != 0)
                    {
                        SolicitudList = SolicitudList.Where(x => x.Solicitud.codigoSolicitud == nrosolicitud);
                    }

                    var resultList = await SolicitudList.GetPagedAsync(Convert.ToInt32(page), Convert.ToInt32(size));


                    DataCollection<SolicitudDto> solicitudDto = new DataCollection<SolicitudDto>()
                    {
                        Page = resultList.Page,
                        Pages = resultList.Pages,
                        Total = resultList.Total,
                        Items = resultList.Items.Select(x => new SolicitudDto()
                        {
                            CodigoSolicitud = x.Solicitud.codigoSolicitud,
                            DescripcionArea = x.Solicitud.descripcionArea,
                            Estado = x.Solicitud.estado,
                            DescripcionArticulo = x.Solicitud.descripcionArticulo,
                            ValorAproximado = Convert.ToDecimal(x.Solicitud.valoraproximado),
                            Cantidad = Convert.ToInt32(x.Solicitud.cantidad),
                            FechaSolicitud = Convert.ToDateTime(x.Solicitud.fechasolicitud).ToString("dd/MM/yyyy HH:mm:ss"),
                            TotalCantidadDonado = x.TotalCantidadDonado.Value
                        })
                    };
     
                    return solicitudDto;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public async Task<DataCollection<SolicitudDto>> GetAllSolicitudxDonacionPaginate(int nrodonacion, string page, string size)
        {
            using (var bdAsuntosAdministrativos = new AsuntosAdministrativosDbContext())
            {
                try
                {

                    var SolicitudList = from solicitud in bdAsuntosAdministrativos.Solicituds
                                        join estadoSolicitud in bdAsuntosAdministrativos.Estados on solicitud.CodigoEstado equals estadoSolicitud.CodigoEstado
                                        join articulo in bdAsuntosAdministrativos.Articulos on solicitud.CodigoArticulo equals articulo.CodigoArticulo
                                        join area in bdAsuntosAdministrativos.Areas on solicitud.CodigoArea equals area.CodigoArea
                                        join escuela in bdAsuntosAdministrativos.Escuelas on area.CodigoEscuela equals escuela.CodigoEscuela
                                        join facultad in bdAsuntosAdministrativos.Facultads on escuela.CodigoFacultad equals facultad.CodigoFacultad
                                        join solicituddonacion in bdAsuntosAdministrativos.DonacionDetalles on solicitud.CodigoSolicitud equals solicituddonacion.CodigoSolicitud
                                        where solicituddonacion.CodigoDonacion == nrodonacion
                                        select new
                                        {
                                            codigoSolicitud = solicitud.CodigoSolicitud,
                                            descripcionArea = facultad.Descripcion + " | " + escuela.Descripcion + " | " + area.Descripcion,
                                            descripcionArticulo = articulo.Descripcion,
                                            estado = estadoSolicitud.Descripcion,
                                            codigoescuela = escuela.CodigoEscuela,
                                            codigoarea = area.CodigoArea,
                                            codigofacultad = facultad.CodigoFacultad,
                                            codigoestado = estadoSolicitud.CodigoEstado,
                                            valoraproximado = solicitud.ValorAproximado,
                                            cantidad = solicitud.Cantidad,
                                            fechasolicitud = solicitud.FechaCreacion
                                        };

                    SolicitudList = SolicitudList.OrderByDescending(x => x.codigoSolicitud);

                    var resultList = await SolicitudList.GetPagedAsync(Convert.ToInt32(page), Convert.ToInt32(size));

                    DataCollection<SolicitudDto> solicitudDto = new DataCollection<SolicitudDto>()
                    {
                        Page = resultList.Page,
                        Pages = resultList.Pages,
                        Total = resultList.Total,
                        Items = resultList.Items.Select(x => new SolicitudDto()
                        {
                            CodigoSolicitud = x.codigoSolicitud,
                            DescripcionArea = x.descripcionArea,
                            Estado = x.estado,
                            DescripcionArticulo = x.descripcionArticulo,
                            ValorAproximado = Convert.ToDecimal(x.valoraproximado),
                            Cantidad = Convert.ToInt32(x.cantidad),
                            FechaSolicitud = Convert.ToDateTime(x.fechasolicitud).ToString("dd/MM/yyyy HH:mm:ss")
                        })
                    };

                    return solicitudDto;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
    }
}
