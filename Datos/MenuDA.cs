﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidad.Repositorio.Models;
using Microsoft.EntityFrameworkCore;
using Datos.DTO;
namespace Datos
{
    public class MenuDA
    {
        public async Task<List<MenuDto>> Obtener_Menu_Por_Usuario(long codigo_usuario)
        {
            using (var bdAsuntosAdministrativos = new AsuntosAdministrativosDbContext())
            {
                try
                {
                    var menuData = await (from userProfile in bdAsuntosAdministrativos.Usuarios
                                          join userProfileRole in bdAsuntosAdministrativos.UsuarioRols
                                          on userProfile.CodigoUsuario equals userProfileRole.CodigoUsuario
                                          join moduleRole in bdAsuntosAdministrativos.ModuloRols
                                          on userProfileRole.CodigoRol equals moduleRole.CodigoRol
                                          join module in bdAsuntosAdministrativos.Modulos
                                          on moduleRole.CodigoModulo equals module.CodigoModulo
                                          join menu in bdAsuntosAdministrativos.Menus
                                          on module.CodigoMenu equals menu.CodigoMenu
                                          where userProfile.CodigoUsuario == codigo_usuario && menu.Habilitado== true
                                          select new
                                          {
                                              module,
                                              menu
                                          }).Distinct().ToListAsync();

                    var ListMenu = menuData.Select(x => x.menu).OrderBy(x => x.Orden).Distinct().ToList();
                    List<MenuDto> menus = new List<MenuDto>();
                    foreach (var item in ListMenu)
                    {
                        MenuDto data = new MenuDto
                        {
                            CodigoMenu = item.CodigoMenu,
                            Name = item.Nombre,
                            DisplayName = item.NombreWeb,
                            Order = Convert.ToInt32(item.Orden),
                            SystemName = item.NombreWeb,
                            Module = menuData.Where(x => x.module.CodigoMenu == item.CodigoMenu)
                                     .Select(x => new ModuleDto()
                                     {
                                         ActionName = x.module.NombreAccion,
                                         ControllerName = x.module.NombreControlador,
                                         DisplayName = x.module.NombreWeb,
                                         CodigoModulo = x.module.CodigoModulo,
                                         MenuId =Convert.ToDecimal(x.module.CodigoMenu),
                                         Order = Convert.ToInt32(x.module.Orden),
                                         //SubMenuId = x.module.s,
                                     }).OrderBy(x => x.Order).ToList()
                        };

                        menus.Add(data);
                    }

                    return menus;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
    }
}
