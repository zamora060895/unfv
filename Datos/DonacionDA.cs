﻿using System;
using Entidad.Repositorio.Models;
using System.Data.SqlClient;
using System.Linq;
using System.Transactions;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Datos.DTO;
using Microsoft.EntityFrameworkCore;
using System.Text;
using DinkToPdf;
using DinkToPdf.Contracts;

namespace Datos
{
    public class DonacionDA
    {
        public readonly IConverter _converter;

        public DonacionDA()
        {
            var pdfConfig = new PdfTools();
            _converter = null;
            _converter = new SynchronizedConverter(pdfConfig);            
        }

        public async Task<DataCollection<DonacionDto>> GetAllDonacionPaginate(string creationDateFrom, string creationDateTo, int codigodonacion, string codigoestudiante, int codigoestado, string page, string size)
        {
            using (var bdAsuntosAdministrativos = new AsuntosAdministrativosDbContext())
            {
                try
                {
                    var dateFrom = Convert.ToDateTime(creationDateFrom);
                    var dateTo = Convert.ToDateTime(creationDateTo).AddHours(24).AddMinutes(-1);

                    var DonacionList = from donacion in bdAsuntosAdministrativos.Donacions
                                       join estadoSolicitud in bdAsuntosAdministrativos.Estados on donacion.CodigoEstado equals estadoSolicitud.CodigoEstado
                                       join Estudiante in bdAsuntosAdministrativos.Usuarios on donacion.CodigoUsuarioEstudiante equals Estudiante.CodigoUsuario
                                       join Usuario in bdAsuntosAdministrativos.Usuarios on donacion.CodigoUsuarioCreacion equals Usuario.CodigoUsuario
                                       join donacionArchivo in bdAsuntosAdministrativos.DonacionArchivos.Where(da => da.TipoArchivo == "1")
                                       on donacion.CodigoDonacion equals donacionArchivo.CodigoDonacion into donacionArchivos
                                       from archivo in donacionArchivos.DefaultIfEmpty()
                                       where donacion.FechaCreacion >= dateFrom && donacion.FechaCreacion <= dateTo
                                       select new
                                       {
                                           codigoDonacion = donacion.CodigoDonacion,
                                           fechacreacion = donacion.FechaCreacion,
                                           codigoestudianteid = donacion.CodigoUsuarioEstudiante,
                                           codigoestado = donacion.CodigoEstado,
                                           estado = estadoSolicitud.Descripcion,
                                           estudiante = Estudiante.Nombre + " " + Estudiante.ApellidoPaterno + " " + Estudiante.ApellidoMaterno + " - " + Estudiante.NumeroDocumento,
                                           codigoestudiante_ = Estudiante.CodigoEstudiante,
                                           montototal = donacion.MontoTotal,
                                           usuario = Usuario.Usuario1,
                                           flagHasBoleta = archivo != null ? 1 : 0
                                       };

                    DonacionList = DonacionList.OrderByDescending(x => x.codigoDonacion);

                    if (codigodonacion != 0)
                    {
                        DonacionList = DonacionList.Where(x => x.codigoDonacion == codigodonacion);
                    }

                    if (codigoestudiante != "")
                    {
                        DonacionList = DonacionList.Where(x => x.codigoestudiante_ == codigoestudiante);
                    }

                    if (codigoestado != 0)
                    {
                        DonacionList = DonacionList.Where(x => x.codigoestado == codigoestado);
                    }

                    var resultList = await DonacionList.GetPagedAsync(Convert.ToInt32(page), Convert.ToInt32(size));

                    DataCollection<DonacionDto> donacionDto = new DataCollection<DonacionDto>()
                    {
                        Page = resultList.Page,
                        Pages = resultList.Pages,
                        Total = resultList.Total,
                        Items = resultList.Items.Select(x => new DonacionDto()
                        {
                            CodigoDonacion = x.codigoDonacion,
                            Estado = x.estado,
                            Estudiante = x.estudiante,
                            CodigoEstudiante = x.codigoestudiante_,
                            MontoTotal = Convert.ToDecimal(x.montototal),
                            FechaDonacion = Convert.ToDateTime(x.fechacreacion).ToString("dd/MM/yyyy HH:mm:ss"),
                            Usuario = x.usuario,
                            flagHasBoleta = x.flagHasBoleta
                        })
                    };

                    return donacionDto;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public async Task<List<EstadoDto>> GetAllEstadoDonacion()
        {
            using (var bdAsuntosAdministrativos = new AsuntosAdministrativosDbContext())
            {
                try
                {
                    var EstadoList = await bdAsuntosAdministrativos.Estados.Where(x => x.Habilitado == true && x.TipoEstado == "D").ToListAsync();

                    return EstadoList.Select(x => new EstadoDto()
                    {
                        CodigoEstado = x.CodigoEstado,
                        Descripcion = x.Descripcion,
                    }).ToList();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public async Task<DataCollection<HistorialDonacionDto>> GetAllDonacionTrackingPaginate(string codigodonacion, string page, string size)
        {
            using (var bdAsuntosAdministrativos = new AsuntosAdministrativosDbContext())
            {
                try
                {
                    int codigoDonacion_ = Convert.ToInt32(codigodonacion);

                    var trackingDonacionList = from historialdonacion in bdAsuntosAdministrativos.HistorialDonacions
                                               where historialdonacion.CodigoDonacion == codigoDonacion_
                                               join estadoSolicitud in bdAsuntosAdministrativos.Estados on historialdonacion.CodigoEstado equals estadoSolicitud.CodigoEstado
                                               join usuario in bdAsuntosAdministrativos.Usuarios on historialdonacion.CodigoUsuarioCreacion equals usuario.CodigoUsuario
                                               select new
                                               {
                                                   codigoHistorial = historialdonacion.CodigoHistorialDonacion,
                                                   estado = estadoSolicitud.Descripcion,
                                                   usuario = usuario.Usuario1,
                                                   comentario = historialdonacion.Comentario,
                                                   fechacreacion = historialdonacion.FechaCreacion
                                               };

                    trackingDonacionList = trackingDonacionList.OrderByDescending(x => x.codigoHistorial);
                    var resultList = await trackingDonacionList.GetPagedAsync(Convert.ToInt32(page), Convert.ToInt32(size));
                    DataCollection<HistorialDonacionDto> historiadonacionDto = new DataCollection<HistorialDonacionDto>()
                    {
                        Items = resultList.Items.Select(x => new HistorialDonacionDto()
                        {
                            usuario = x.usuario,
                            estado = x.estado,
                            comentario = x.comentario,
                            fechacreacion = Convert.ToDateTime(x.fechacreacion).ToString("dd/MM/yyyy HH:mm:ss")
                        })
                    };

                    return historiadonacionDto;

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public async Task<FileDownLoadModel> GetPdfDonacionById(string codigodonacion, string decano, string jefeOficAdm, int flagPreActa)
        {
            try
            {
                decimal DonacionId = Convert.ToDecimal(codigodonacion);
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Clear();
                FileDownLoadModel eobjFileDownLoadModel = new FileDownLoadModel();

                using (var bdAsuntosAdministrativos = new AsuntosAdministrativosDbContext())
                {
                    //var Solicitud_ = await bdAsuntosAdministrativos.Solicituds.Where(x => x.CodigoSolicitud == SolicitudId).FirstOrDefaultAsync();

                    var Donacion_ = from donacion in bdAsuntosAdministrativos.Donacions.Where(x => x.CodigoDonacion == DonacionId)
                                    join estadoSolicitud in bdAsuntosAdministrativos.Estados on donacion.CodigoEstado equals estadoSolicitud.CodigoEstado
                                    join solicitante in bdAsuntosAdministrativos.Usuarios on donacion.CodigoUsuarioEstudiante equals solicitante.CodigoUsuario
                                    select new
                                    {
                                        codigoDonacion = donacion.CodigoDonacion,
                                        codigoEstado = donacion.CodigoEstado,
                                        codigoUsuarioCreacion = donacion.CodigoUsuarioCreacion,
                                        codigoUsuarioEstudiante = donacion.CodigoUsuarioEstudiante,
                                        estudiante = solicitante.Nombre + " " + solicitante.ApellidoPaterno + " " + solicitante.ApellidoMaterno,
                                        codigoestudiante_ = solicitante.CodigoEstudiante,
                                        montototal = donacion.MontoTotal,
                                        numeroDocumento = solicitante.NumeroDocumento
                                    };


                    var ArticulosDonacionList = from donacionDetalle in bdAsuntosAdministrativos.DonacionDetalles.Where(x => x.CodigoDonacion == DonacionId)
                                                join solicitud in bdAsuntosAdministrativos.Solicituds on donacionDetalle.CodigoSolicitud equals solicitud.CodigoSolicitud
                                                join articulo in bdAsuntosAdministrativos.Articulos on solicitud.CodigoArticulo equals articulo.CodigoArticulo
                                                select new
                                                {
                                                    cantidad = solicitud.Cantidad,
                                                    nombreArticulo = articulo.Descripcion
                                                };

                    var BoletasDonacionList = from donacionArchivos in bdAsuntosAdministrativos.DonacionArchivos.Where(x => x.CodigoDonacion == DonacionId)
                                              where donacionArchivos.TipoArchivo == "1" //Solo los tipo que son boletas 1. Boleta 2. Otros
                                              select new
                                              {
                                                  numeroVoucher = donacionArchivos.NumeroVoucher,
                                                  comentario = donacionArchivos.Nota
                                              };

                    string head = @"<!DOCTYPE html> 
                                    <html lang=""en"">
                                    <head>
                                        <meta charset=""UTF-8"">
                                        <meta name=""viewport"" content=""width=device-width, initial-scale=1.0"">

                                        <style>
                                            .tableBorder{
                                              border-bottom: 1px solid #000000;
                                              border-left: 1px solid #000000;
                                              border-right: 1px solid #000000;
                                              border-collapse: collapse;
                                            }
                                            .trBorder {
                                              border: 1px solid #000000;
                                              border-collapse: collapse;
                                            }
                                            .tdBorder {
                                              border: 1px solid #000000;
                                              border-collapse: collapse;
                                              padding: 3px;
                                            }
                                            .trBorderTitle {
                                              border-collapse: collapse;
                                            }
                                            .tdBorderTitle {
                                              border-collapse: collapse;
                                              padding: 3px;
                                            }
                                            .tableBorderTitle{
                                              border-collapse: collapse;
                                            }
                                            .fields{
                                              background-color: #ffffff;
                                              font-weight: normal;
                                              font-size: 12px;
                                            }

                                            .linea{
                                                border: none;
                                                height: 1px;
                                                background-color: orange; 
                                              }
                                        </style>
                                    </head> 
                                    <body style=""padding: 0 20px;"">";
                    stringBuilder.Append(head);


                    string FirstTable = @"<table style='width: 100%; font-family: Arial, Helvetica, sans-serif;  font-size: 14px'>
                                        <tbody>
                                            <tr>
                                              <td style='padding: 20px;' align='left'>
                                                  <img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAABzCAMAAAAVDM/RAAADAFBMVEUnIyMn" +
                                                  @"JCQrKCgxLi47OTkxLi4uKys1MzMzMDA4NTU+OzsoJCQmIyNHcEwkISHLy8vo6Ojg39/j4+Pc3NxtbGxNS0vOzs7s7O" +
                                                  @"yop6elpKSBf3+trKy5uLjZ2dlnZWVgXl7DwsLX1taVk5MJBgbm5ub///+bmpqTkZETEBBJRkYeGxtaWFignp68u7u/" +
                                                  @"vr50cnKJh4eqqamko6N8enqCgIBnZWXU09P+/v719PSGhIRhX18hHh4oJSXw7++enJyHhoanpqaxsLBeXFz4+Ph0cnJUUlJ" +
                                                  @"samolISFYVlaEgoL7+/tPTU1ycHB3dXVWVFRGRESGhIRmZGRycHBkYWGioaEtKipwbm5BPj58enpKSEhqaGhkYmL" +
                                                  @"R0NBeXFxEQkI6NzdraWl5eHh+fHwvLCyura0rJydTUFBDQEAnJCSVlJRMSUlbWVkXFBQjICBcWloQDAw0MjIbGBhY" +
                                                  @"VlaCgICNi4t2dHSLiYmdnJyGhYWnpqaYlpa1tLSNjIxRTk6wr68LBwe2tbWwr6/Ix8crKChUUlKQjo4rKCh5d3dLSEh" +
                                                  @"/fX2qqamSkJAaFhZ8enqamJiamJiioaElIiJvbm4uKytua2skISHW1tYmIyPe3d0mIiInIiIkIyMjIyMlHR0iIiIAAAAm" +
                                                  @"IyMnIyMmIyMnJCQnIyMmIyMmIyMmIyMmIyMmIiInJCQmIyMmIyMmIyMmIyMmIyMmIyMmIyMnIyMtKSknIyMmIyMmIyMmIy" +
                                                  @"MmIyMnJCQqJycoJSUmJCQnIyMnJCQmIyMmJCQmJCQmJCQsKSkmJCQnIyMpJiYnJCQmIyMnIyMnJCQmIyMqIyMnIyMmJSUnJCQm" +
                                                  @"JCQmIyMoJSUlIiImIyMnIyMmIyMmIyMmJCQnIyMoJSUmIyMmIyMmJCQmIyMmIiIbGxsnIyMmIyMnIyMnISEnJCQmJCQnIyMmJC" +
                                                  @"QlIiInIyMmIyMmIyMlJCQkIyMnJSXwTTb8UjnuTjXvTjbvTTb/Wz7/VTv/Vjz3UDj/VDrzTzfvTTWysbHwUDH+UTbuSzay/Bud" +
                                                  @"AAABAHRSTlPr9Pf8+vPy+/v7+vicAPP7/v38/Pn6+/36+/j5+vz6+vv7/KD9//f6//r/+/j5+vj3+Pf09ff8/f34+P/8/Pf4//r4" +
                                                  @"/fr4+v329/36+fr6+vz39/r/+/b7+Pr59/v6+vv3+fr8//L6+v33+vj+/vug8v/4+/b2+Pz19vf3+vn8/vr2+/z7+P/3+Pf/9qD" +
                                                  @"7/Pb4ovr+9vH18fwQFhMMCQUBssiKeraljZaTQ+A+mkjkf61fwP/VuaqQnu///29M9oS+aL3/0hv//0C7ZiECHxkdy/nYOmNrT3" +
                                                  @"Poz70yW8R1LAdxh9wlWHxs2TU4VVMoMN1Yd3dwx/////////X/Ii4uPK/izgAAIu9JREFUeAHk0MUBxEAMA8C95TAzg4+Z+28s1E" +
                                                  @"bmZUs/IbTBM0IZZXjBBcaSSWV5GOaqKjAhGGtYN9YNmZZtO67t+TIII3sWJ66VJmEWTW+UF0VZ1VHRTEXbrX6sHmC7g3Z/OJ7OF5h" +
                                                  @"cb/fH8/X+fH8A/5E9O+xK2wrjAH52kjZwX0xBWUUooAeKASrSWiOIWtFAIcpF0hgSJEHZEgRxBRWLmqoCBsvaOsa2fdxh+zHI67z7" +
                                                  @"nfvc55//DbpDS2H98srq4MNbDcvgcq1F1jcwMhp7Fze6iIQXUJvBrSTUpQiXZTsdJulwYN4ywHqvYQ2wEtAxwpjGILrjXBg3Qz876" +
                                                  @"c9w4xjvnrKm0WjKE7VmFxOEhjUYQ2LXOZcjdlMOdwysCYxIxk35vf2k30QW4CoeXgv4CPucGRHGfh16rN+mvDNLe9yCxHjcmExzwna" +
                                                  @"R4uKTB/GSZ5NJ2jm2zFAvuf1DoXL09vdhx/owXa3hRYlnOPk4qiMDkqXEJk+CpwcQtdQtFMvjNSZJFqgSyp4NPdYTX9WzUQI1ty1FW2" +
                                                  @"Os42NZYOnG+UVaJ5C+SwX5xJonknhlF16daWP4kyEFGtdFcE1cyOs6dMO5Bkuv4ys3UUS4NZYEythMLIgMv2hdIN5rF7yBSMQvWnov" +
                                                  @"cYsRxwyXcETeUO35Fdu7vNMBmlai6LQJd47XN1p0GGAZCeJWvdSxI06GWETkVZbhjw7U+Rebh7koFNdSSTMLbLPz61p0eBzDSyRE1b3" +
                                                  @"sxnLnfNamy/FRuXxwf9oWy36atesY/wbefJm12HSfPg891gf3IQhD+kwHp7YOKzYsvtk2pRvddhaLp7vN24mMvSXbJyTYZAtbfww91p" +
                                                  @"emBETTV0qOU5xwjZezHfvFt90HFdpUUVo/S0Yl3YO9hCktjP5z2LGeOvQz+5doZk7P72E5x3Esl1W7jWy8GyLH0c2WvNIj8V6nsha9s" +
                                                  @"ms562enc+FbgC2vCOTENAVKQDnppre8W9ngSZSCkIvNcTSFSDcPI+uPrYP2Ix346NcH92FIyomcklT1qExJFBuM4IUlv05iazBsyd/+i" +
                                                  @"A4a1nL5wAQDJRSceqroJF0Yn+LGbpBcvOi5U3WozRS7vPQbHInHbahhuT6LX5s9C0r6lyp/ZUIQA0syS9l9pkLxtN8NKshz3Vbhiv5+Z2" +
                                                  @"lYBH4pkfevVus7QjmDcyElJtRE2mbfbp+8svQPrdOCXs5VvEO/Df9+xHI5gDm8c0wqFElKPDoe7UFerJnydkRfzoS9zwAF0NbdkYY1wCJcRMtNM30yNCNLcUXk+X6unKydsuwhH2mx61uQmd0/jGpjOMAarY+4dvXGIxjJLsk7TGEyrS+xkTgl0NCTrsG1MQxPXrcNxGN00E7W3M4ikauf9/NZD15W/fa8ItSy2xJfAkubr2dXaQo5z18PooO2DUcJo2HH6ppiLKMvVvWgHvD123m534PuXZ+MqMrycxbSvvgzI6GdrKcOb1W6FxcJenoZRfFOOcSoD2Ql1HgDQnslAWeOemTTWsqgnrn3WgevVPEwzh4TY6y5eI/fvUuKp9ek1Pkn3SQjGKciggnsuQGGK2GtdXjSlPBIs4YsuHpf3KCkr3D9hi+kYI0LtaDDlaSZljArAGVGLPS0iua4crVy5qG8hIE3Z2UAcNhon0C1/sKPCqZOJHjadG+gcAxeeaeHfgz/Hf1lomukAHrtMlc7+o4M5iiOMqGwfABrONLR86JZZs4eAlrr8CM6ZO5vqyS++ByoSAVP00iPl1NUse5HPIW83DzLgeRt3/g9OmjP98R/ijEVJgMJwlCpVrqh0Op+zGKSM9GZavN/9s04HspsfeAj7sXo/exvW257U1X3U7lKm8S1zU71WTVlrpjFJzAL1WdlqO6aRbK5W5UXP782yf24YKAQGjAS72vIjl4KEIkFQpFk///vd54575iB5ta9e//b54/mPec85zznfN/nPOeZY+KZ+MQlhHx9LGn/lmWpQ3pG+sJzZkbWsrGzof2DJX3RSFlQeofIMjKXVmToPufosQIjp3/gvLI532xwNfxuk61jyLnICLsIX5FnkkvwVVOLEKOA+LjTLrE34vbEBB5ec1x4Kvzw0tQh9xvtl8W8b/KXDX971R3iw6VgVaFOqej3d9+pWfxNyWKSpWlp9zTP8m/KMvXaKV9VQby/wGorOd96hjgd9Q89ZPnn3Z8e3BifGuzmJvITi4PCuOKAgLC9wgs2x0IOHfM19hbxl946VCm05qoVRcuno7hPfLjUKB7olG7eqn2nppysy1pMj6KpevZZSTboh9WoyCU+RB4qmjjfmgYG7j4Ve9HR/2higvcOgbvYgiv0D7UVG1sE8Fx3GAXvO2O51TjAhWvMd1sC6ydVzcJzs2q5B2Q9yvx3tuEj3e2xWrH63bAozhJYTAtDt2oa0/RZ//DZPVY84XwbdYb/cYpxwNb9xgHO+06ZmgYZinlutwJ8XAKc3HYa7hZwzzmmnOZf4p09od2GemFltGVoddrbdUIKrnnUpjvLR48worYOXUa4qlMHVnuGroasq0NGtC6HxekmOV1LYGV1tXXKdHq2LTGU06mdMEinjkIOWNXC+sfWv9gkbb7ac91xvTHX51ZsApdrmxC4w9RWKOI6XDMUG3L3f2Ia6LbHbi0O8HphPXjaq2xmKDqvD+qePa0l0rur+7HGc1QiiI67A6i9W90uq2ruKGDowU5C+TOfYlqqKgn1GLfhozS3h2KeKu9iWEO1qBtjVjWMT5PCMoZKK1cyy2F1NKu6Zbqw+kb4qGdJESCEceoYKuRnJXqsf1qkRlNYgtqrR7OhkFvd0YSKDfeLCZCxe2C1B1nFsCB1+Fj8/VfHY7cH2HrGhwRv2nVdKg0LDfKw3ndeGmiYmGjvmhIQIrHfgFMH/bByyW5msDA/jyTrISrcGiGI2lt5+GjkKJDNUo6iubG+qEGRi1Y0XtcySFWXdMuGSbr2+YsCRpEPY9wqgjVSqoe3y7upAQpg9fNVVeXPJ7pJsgmATyrK8kdrmLqWgaWwGsanDFS1Wlg5NYqe1aOjtWaqSXCQl3xF3qsXNRQ5TRD5tyCi9g4oSvKfF6UpBoHmDNNNVU08KFExvTDcKtVIo9rqEy2sbZZOa08F7LTa4Hpjp21y3EmnVGdfey9pTLJtqL/4s7UXuaZuEXaO+9/Ds2ooqkkdacgyGQT4xwQxxWdeq9+waqadyOCoJqDQOagoRLBKmJ5iQpaVM6lSe98wrA8CPPTCkbqcAljpPbfK8UmpGkT//qz4GSDNGjDLYXUSbyiyfgFW362GKbWPGJBzBNHWQ45CqVJFvsUBPnNAVQuOmDGiGEEBY5Chwaeyu1UV6N+6W4Ws1W4WVtqa9d9dCfj0fEjClm1/MQ5NlPr47BY4BSS6BTnbu3txjx9xvC4KsRJ9du3okR//9i9hkSPqUi/VkMHCIioUBWoFBXo9rxR5WH0IzqrxMhI6Ee0lZGGmekldMhbWhEaxWwWwKp/j0DdHTrYT84zZW3VplFwJFtFIMsUaWENN/Zr5NaEWzajlr9owrGlVCR65rYFCnQZV93DIUCHF7Mrn2azVGU2ATzmz4+xu43+aio44/umoU6iHQ2pEhCDUYZ2zq3+CNN7Y5iOu4eGUFBPT0B0uy05DwIBlEMNiE4ghxqxTA2uWaXiE1kEOZCODZN6L5yDlLZQcYOEY9IRSGfzcqMzQpg4jikZ2WZoA3zE7N1pQxiBYz1Qwd2DLcFaCReSqOB2EUhPg08eK+/KrzMDlH8KoOqkDGNNkJnmqcljFEwJvg+5FVjWwfncV/THnuOiE5dGEj7aahjmEn3GJiOI5m/NunA73TrDeuT7F4tSnbi4WFgFX/ZfAuq/SybPIQvVCseO+1sKChaNp3FG8AqTobMfSYzYHsJRsUpSbRipUPfflGljVqnoCc1TDynhQRpEkM9AMsMpVI+yx1VK3IqysGbTWVgxLfieEIim6mUM2AYv6RbCAwARbzAWzg6o+bFUNK32ihLW64Fnnwn84wBUZf+RiYe/pauRizXc+kLhbEB2zLtHX1/d0gGFS4FfXpVfXuJ5xXZo63ARnxZJVR47ihS6HpSTLiCnGIFP9+h50qaUzO0sGsPoX8oTSFzV1JCPHYyDF27i+HmBlQEQf7u3C2/CFqpl97fSKsCBCqR7MqmFVUuSdeuVYOjFINsGor5bAqmC3AvblxbDSmxUc1qoG1jdbvnO1jPvCZdPmq2HBwd6m3h4u/iLBtU2efvukApNwB5G595dSkxhL++XXytOkQacmP2SY0nfAAjcpblSoo1ORogrrjw/LsxZgZc49wcBGFBXsGEW4K5RWw0Al6XhTAiwUELuwP1IrxSyQOYZ+TNdlErIyEru6TO1Z9xR32FS3uxJgwcgs+PQ0cm4JrGnVQAZrdSZHk2c5OkpifAO5fzx51d0h3Mj6tDgwJSKF58X1iAw5S28MNVkvNHKzdFjhtw6dBmQVnvbbGVWz7J2wKsnBMn4bjmVUEz5gbvVkLsCaolTwCXu1iB1jjGFeQk0rH07De6pqbIbDlMiInDxFDZSyB98FC4I8DbCy0lCSAJJPwek6S/GLoTTPV5ViWOh8fEGA1KrKspbAalSVqAtTHNj8GNaabY5rUi+EBPMsjQLFX4R727o53LhwwDZJmhJ/9YsdHpuchF78K7YSxxV+JvmGr2ioKR+duEMrSsYI9cl3F8Oi+GpYD7EboYBTw2amJFnQ3/vyvoovh3oW0oSCnpD3zlWoOFOaMepJ6m5x6wRjRtUiZAxV0DrWmk+n0QbpiNmAoupNad8kQy8N8FSPxtVzKRo2/k1F2vTQ/HAVk0Y2go+omPzi0lEDiFQT4GawT2v6e4erFGml7JEN0qQYRG+WJiuKsdW0LPzdsG7N+dCTh12tvrD13WPvGm4YeyNMuF0YFWPvJl0XayLy2fMHoXXUR9w/XnO5svyn3a9rDEiFQkENPOhcdGMwZJbWqXPrMGpmMERgUeahmEkyj3sB4uQqJatQQkJtxVvtGHPNqIYqvL0KDvPhX9AzVVfeVWYGPdpqGNTYPZe2xLNazeo0sLIGzQYQrOzVZiqKYvJm61dVtcNA1WCopEl761BcBROiC8A00b0Ke1bfKjhD+p8uWJUjt1/Vx/l24xku/9w1lGKt++f3wsj43eZO9u7xSdEeUqfAgNhUJ9dPXYwOhfIMjY1Ox61wrZw5q5xTzmdpSuwXKVlXF5paekcGrs7peqTt0Vbc39rB7uOubLZyfF7eX9q5aAxivl/eRYx3ZKrbh/qV8zIU6tkhu+TKeRThuxZfWY2DWVayu3Djo9b+4i71V0LcNCbvH0IDwewyFyZUmsFOqCMLo8ZmZENK1mom/JPF+R3fwtffRGC1+c9BJ91MDcN45mKhIClSYhPrFMnzDpfyzY03nOeeFpnwRCd/+4OF35kfUr4ylq715B4zij3jdtpdZBdzwDbCxt1CJBXdSvU1/XG/iZvxFf4PJ/73tz9YrPnxyNY/7BBY/ZVrauF0xtV/lWhPkm30dnuxrze6gOdxnQ6luIScd/z677/9RRpOw23bkq8Z8z6+FBZ55IyFePsRhwNJeyN8eBsvxp8KkFjaiK1F13ZtwP8587e/7nyy/8Dm8DCHyI+2HwwRmSeai5K/j0iNsvZxdYqVBn7pGCAIv3Lpa8vPl8HqeKsjem7/O6F9nC3o05G9923qWx31dngeyyKm0EcHispQ7ERBGs/qVxJY6xTA2rIuKtn/lPTK1tO+waHhTqkp0RduxHuILXk7HEyNXc6LDD0vSSRfLvesh0zLglCVev4EQbW00POEXrlJoUHeEu8pYzSYHMaFVgYNz+8gyugWZhC1oSZIzl5S6PPZrwWrGxlJA1ifRwiEn4W7XDx/1MPaWiCNj95rfzneXmxubS3yOGpuIeWvsUsV/2U5rKoPgTVE6JWCD4KVM8Mgk/dwoZxsAUoyXVg3WVjTvxasQWTQgJP28bYNkYl7rxw+GpZsbB0WYvjF3qS99ttv8ATicJHFKauT6D8MOEqifdascFP6K3pWAUnT9HvDIgopBKgZP98HcI1E9gBDU83/Vc8CWNsuR0oOfRwesHut5ykL0x1xe2wlwTG2iTECbqDrH2KNzzluW+8l/OQdsGhKLYq+/wxWUR2SKeJ9pRhM93TAYxaHRuB6ifFqNELVf3kb/o/jfrtj0VscvwsP+/K6G9c0NTUyWOyW4CI+cVx8zMR399k1Wz62S43b8vWy38EDLLq5CMvsfwZLlo0kh3hfyQZC1Bt47IVZwC02jDD+34X1u7+LPCV++z777jNSHBbuLbGwSEGXWt4Crig62GWneaxJnOsPx2ycfZJN3f70t+WwqIlFVRnK6fqXY9qyvL6yg7ipA6uzX0ehs6sDfbd5Pa3MJtIzkLSzg8ibXvSVZmtHeTtc39S6+GtgDaC4C0+jFI5RRCYaIXNFWJ3yJy+mh3uzFoxmEkPT/dnoWw0SdH3chF267U399JzWu7uUTajXbLYOrG8Djwo8/MwTDd0OnpG6W/i6SHaGRfF2eMRv9BR5b4w0Nv/nDpf4eOe9Ec5nPW+uAOuBrnsU1lFI6JFSAlvPQyWDynsLsMZZhapeKLVXm/WY3ZYzJJlLrDbr6elRx6ycCQ6DdJhfNF4xlcuHckm9ru1KSh3VkeTCLF4ixypBo+WtACvrXhoaEEZ4AvqDSK2xFRm9T/ShR05HFUnxWxHEAjPQ6qnAuDILDHCvX4Z1YG102Ovjd9Ir7PTHh42lUu8Aj2C/iCDXxDhBbKDvpgSpOM5TaOXB220etvHiv/Cs8YckjaM9XwnlrBkKglrLQAsLK7tKo2AmB1iTKCTfnGGQi2gDfE4Fq8OQeOgxDqUu0+RqQitdZqjG7BHc89DooZN4Z4Bv/wkPCOEV1v2UoqmCZmR0NfGEpJkB9Crpni6ia5Ji5zYA08ju1vRiKKUWVpztbne/ve5WJzbzooIERt6JAvdkr4u7JAlBLk5BPJ9du7Zv3Ocg9nCyurFSgKcHC9XSBkGaRGWGRrVMWgZcU5HsJDWw7mkVOBkAC7UYQONrbZ7ViHRACabdCsR/gRZ1mWwitJIHo/QTxDyE9xFY3jvyrEpS3V9tdRKFtmoGG0VnwhOkYtaD1HIJYoTSzI0aRFr1bC8aytqYFee3yckvMSXF+uL6LfsSvU66ixwue0VeiBbxjvjFS77fti1KmnjEYfcRJ5vzf3vnaagqRpGFRiXOy9kCVE3mw6uGCrSCFhbWGJQH3vTWgEIjhgUzJEu0SWlHDyx+4OcymDUwuA2A6Two0xydhPwVsCgkiGfw+UoPrDwFQrC6t/c+jDCEYYHRsnaABUgY1TAxp85F5K0j7KaeUaFeD3pLoRwytXAa7rcSCt1tzYOFXzn+KdTPPdjHRXzZy8vuxGkxNz7YyNtxS/xBSZRDoiTIcaXTkBW4gZ8Ae3BAzdAtcP07CygaXo7lMiysQgo7A4EgwU0thmVWXzmshTUKOt0ZRHoeSVEtbYRsEvXmz6EysziZew1Q89ij9rUeWKMFeSXdcCGJB8CwekYrKwkMi3k4V55B3EePDVNwVU63UHeI9vKavLKf4ISC+Si1sPbECVOtHKIkgjUnrTe5h/ISHNb7Bdn5cJ0iLSw8eFb77Cz3RARFR0j+uiIsWi2wY9By6AGob8QL72MPLEiEABYYpMvwjTi8LoCFvYPQwgKyQJ5orSmfm89WeyOTy2ZWlM4J016CYlVDJoCnS2R6YGHJ6a0BY9MYFlVEgAAsuKrHc2SqoO4OTDJb3QUZab0DvYY1sH6fvF0oSIwWJwpsI1PjTxoHxhgFXfYSJwmNBRbW5/Y5JaRGedpFRAoPRkV+9X8rwKqbBPnlNY60A6+QgAFGTtyGlzKHzyuABduSLruNFO6DQjGGxcwugjUIxHWyhjew5BdwOqXREJu0co8CrBkhGI1eWBnKu9UQjTSwwLgGFnNHnZzAa2+GyQ+iSfLbcJZzb4ZR93qphRVxOlYSZ+RpaSNdJxaaG4q4NpvtbL6/IDAx9QjzEUVeuZLosjfmQtjhXVYrnYYP2tWCVlOnDspI1J+VagdjIEazqUNG2iKFl2pYdEOnLqwcVKX5GsOuB79ZeBeLW/rB+qgS2uf0wirNRUkAwzAhC7Dong7t4PnoAYcMGs8Nhzb54waK7aWF9Y/Pd/Eku2542XySdPGYIHSnhbWLf3BssEgkMvUWmfJPf31ou52HzaVdScIVYxbsIizpGBYrimminGJfYS2GlZm2SKEJw8Iurx9W5Uqw0sHVcovQwW+QoQ/WHIMAoNTuLl8Li5OuHfw5joAYFhayFMUQaKMfFy2CVfd1XFT8Pp4wcf2uk2GSyI2e14ITnJ1OOHj6Xwv2OxZnu9P3x8viYwLPg7y1CJaepFRWAkt/Nj09/Qz+GSPqKWyHeMwALLxPy5o0Cm8xrMn25dtQBnDkb9MX/Ae2kgGOKlq5o54/qn1I6IE1rj5HVw+h6KeFVZathfUEnqZC4FxZmHxGehqcKw/GiF5mEaytcQJnv3Ve62wurDt4QCBJ4fGSE5PsIpLizDfGR0bFuXhd2OgdLZFs8v1cPyyiShPgWVFCcwFaZh0b4EcWK2BYM+0rB/g3ZAunuV8dTJjHmtHu6pqfhi40sNQH6zUMeBOwU1pYJeNaWH3qzgOLI6Jcs7Q+ahGstde5Pqnx4qjN0daptqeuiiyE16SeUk9/gY/UoiV+u7NT8oWEmAg/gRju4PXAwgEd8sbVNfnP5saJRz0A6bZ8hE0dcBCDudWCgmxFz3oOOs1TOfPgp9RrIqcZHONJext8UnJd81N8Nucd0wMLQ76NjxmtZy2FRVRAftqLNnfFzcYncqJS01K1yLPS1lq6W8ZzBbabPxK6O7mYmAr9jL+M+iEm+fgxyVWTEw6hMbu+DPOwdRen/lUvLJxP0j2NlY8hIjXLsCGU8i0kpW18COjllT+BwuDKsGC3tTBmM0gVJ6V9JPSeNAAHqQZdrQA/VDuTow9WKcD+pfT1TfQJ5XfAKqYgYa5/0gxzu0n0Q7F5drZi8WlYt8bykm2Sw/c2l5IEFsL4SH+fTbyrIV8Ymmza6Gftnhi90/KKZF+ET5TNus/1w2K/qVAUzI4/jx0fhK4LwbCIfMjGWYUxYkVY+HsGjVP7Wch2Rii2goajVVfyoQVyJj2w8LkDmaB62S/eBYvIxXODV1WWSXT26Paq1MYsO99EL4nUak2Ss1+MkXEQT3qCG3LWUHTYOmydyCsm1jl5s/PuRFsfr2XbcISkaVWRTgzKJbEXUcwwOwHwj5kmBlkdWqTAH4ZiGUVTZRhWjYqm8X4qohgaD9KEk6RuUlteJK1Ik2ZIJQuLQ9FkNXxZRLNCoXIYjQi/x3pBqkFTE9WoHUGcJGmKg2E1gQo7auYg6IGdull48bgXU15C0ap85MVorSEI1j4nvwih+OSGtddF9t6hFuiXt9bkWePgg+d8BAFiB694u+SwyMsH42MPLQ3wtU+R3NatGX0awjB0XW4pO//aBobOa5sHPXz9cnumRa2Ar2juo/r7GFY+6LQRIMMjDWiWDQ+LCXaUxhKaoQ3ulC67AcyDTt3puDRehQoVKJRB5QSK0fA5DLF0AJlsRnkfKufmELnoo0qGM15QeaNJRR6U0Uhx4OYU3iYclGMNviHuIhWEvgIsoQC/ycheyEtw+2jXFauDFkfNpWGxIv7RW1wXT/PYEGH8+f2XU47be4iD+BiWfml/W9o6n6ktd5TOL1aQYQX98uh162yXTnl8rHW2k/i3JX229G0OOz99etljpaVj6ZpSJuqlmTT7CbD+n507bU5TXeAA7ky8UfMmmyS3Llk6ahTTuIcTyHLLOahkkYq3gpIQqndUJHYGBrwxDV1MPYRx5tixnOV7nFe2H8CPc/d9NXnZrxD+rq9/wLM/jy2PqVliaf+gG1fPk1e0Q0TGCHjDRTmiHdjX9tOFfrTfK31tHq8ymltxXhb3XcVubOboUlAjPIx962g4Grdkr2ygWYH2BdAThmfww1+bWAubWBKPhjoslYwGhJSt6lTU0semVATszaIef3ey4jR0xVWEJ7WheWftDIcpRZe6O1mlHydVR1pWupG1ayWXWQ3wvKvyyyOW1lxDu/kYWkZ76XqShyX74iYawghxBpsqyGSq7SKIfDUjKdzB3rHdGZGHdsjE+vErZ6hfNCjbQBYogwLhmtbr9JObfI8OqUEhCl/2308lAh2+PG8+htNPhqlIP2EQ0Nu6Uazac/uzlcqrrfjxux9yVEPAVgNzJ1ybT+cw65eDf7//wx8fVP5k+QWrhqxlq2MIPTPwQAgst/Pxk0qAG9D0kb1+lYDby8Bz8fltTT/4cu/On//y14eUv/3dMhryKEHY7MBXvX13H2DLtAvWyjuMkI24KIBPutyPWConUUDY9+Vq5T//458PKX//l2XkTZ8fNTWy5llD68yqFo9pyqUmoxwWd2vM0HrzDi+lUCYEBk///QXWf/77v4cVy2jZZZ8x1FUp+s7A0qhxyQi1ut1mbUpFTjn+RjrmFHYTv5VysNnOsowWV+0OTIkym2dloAXamRJBhI5UFVtxkjmwwZKtcC6Sh0uI+NNvTKyfTlgYz+bRDfE8B45brZub5x/v83MDGSN2KhjWuBr6isG8JtZouy3lKlSopCAO9AIERWTyEUEQRDJIZvdbwCGqR6qQyw1r2ybWaP3EwFZDLOFEMhKWkxggjJbTKFMWiJSgSkQGDKetVqkQAU//bWLNh5rOF88bn23jcctBlZKqxPWjfb14xMIUWK22xtUGAt7gUfb0tYnlC6s7xxcTqfFnZArZxTRVpVMHEaFOZ6tiIRL8nBlPMtS5hbcPHsuyvZNnmGCr1UKCzQ0RAWD9LEWTCpovog1xdd4evBDFi3MOlTaemcfYZbOCpNvs9o/VM9/b4JRGqEwWV2gYyEWq4nvId52GX57wnK1Eph881mjWdd6kExsD7x7kd1+02CtG0FyBnKDyFCLmIL8fWsc/8DswtWwW8JbugCZ7+5vGot/vf3rRgGuu/GWkTebxI/RGDE+w/G/xgHxM9mLm0ZvTbqeuJRIy8GTC8urNriA0IxFZ6bkKQg0J3mFBnstkfFB0rZndHYu3QsO1k1TqpwnWaZ11ysxOT25f9Uiyic08vruzlrLRlVVNWDIbpaOfaO68iRn61xOWNXnwkhLOf/70qVFdxdR37v7CHRbOqIpytW4OK1vW+yGJB5ztCdYCw+kCWsF3AYBCr5RvUrJydniHJeEKcDbp7piPYbEkKZJK73Wfqiewrkci0sHKygHDKZNUznLd5VPiffhlrmRiWSzb+3qsc5DclMt6u1dDYRRWDe2YQwsqBlA19+OiEucfuzu6vmg+htOzFLffjzLXvm5MrSMgiIBIsM4GJz8IAtqwZ92tNeVD50NUcv/bxNqmUjip4S/WIvUDAhnfBTk+ydz3CDONboib3crLcnSfeWJiWbaPmLzRO3uh4KeH1XusVtV3yIL3/zLLy20yRsrJqMx4TCzL4hmZMK5Ies7vh95fjO8CrkPn92xi+NAPdT3tjoa/5cx2lsUyH9XiA89gH7prZyGNRqYFIvMQHGxlJmpb/knWs67BoMNsm1iW9Wi0d0rzPHQ4aWld36pAiapA/rWSenRb5yC/b+3QKw/a/fZgzqwNp2cZHi8aPL98fLzhDVgJqlTy+P3zpXqdtVZ8B4WXj5YUvnPZC+z9ysSaFRT5Uin2FtxXrFBz2Gci7Abk355RVZvD2sSuZ71ritzrp/qnJtb0qcB13snEARS+Tekt8SN2RlCQ/9EUR8yIwbKess4uFSOxd4rxg1lmjTyMsuFJZXvzv20Dq1Z7YfWcKPh839lqtvzMTE0oPO1uwbXOVpF58t2Dx/p/eXBMAyAAADEwT9JuyOyOMIJG8MHdk5tl2MAvW4RcqJVKHff5by96kLsPq8GO2gAAAABJRU5ErkJggg=='" +
                                                  @" alt='logo unfv'  style='width: 180px; height: 90px;'>
                                              </td>
                                              <td style='padding: 5px;color:#000000' align='right'>
                                                  <table align='right' class='tableBorderTitle'>
                                                    <tr class='trBorderTitle'>
                                                      <td class='tdBorderTitle'>Donación N°</td>
                                                      <td width='150px'> <b>" + codigodonacion + @" </b> </td>
                                                    </tr>
                                                  </table>
                                              </td>
                                          </tr>
                                        </tbody>
                                     </table>";

                    stringBuilder.Append(FirstTable);

                    string Title = "";

                    if (flagPreActa == 1)
                    {
                        Title = "PRE - ACTA DE DONACIÓN";
                    }
                    else
                    {
                        Title = "ACTA DE DONACIÓN";
                    }



                    string SecondTable = @"<table style='width: 100%; font-family: Arial, Helvetica, sans-serif;  font-size: 16px; color:#000000'>
                                                <tbody>
                                                  <tr align='center'>
                                                    <td style='text-align: center; padding-left: 5px; padding-right: 5px;'>
                                                    <h3 style='margin-bottom: 0;'>" + "FACULTAD DE INGENIERIA INDUSTRIAL Y DE SISTEMAS" + @"</h3>
                                                    <h3 style='margin-bottom: 0;'>" + "OFICINA DE ADMINISTRACION" + @"</h3>
                                                    <h4 style='margin-bottom: 0;'>" + "“ Año del Bicentenario, de la consolidación de nuestra Independencia, y de la conmemoración de las heroicas batallas de Junín y Ayacucho ”" + @"</h4>
                                                    <hr class='linea'>
                                                    <h3 style='margin-bottom: 0;'>" + Title + @"</h3>
                                                    </td>
                                                  </tr>
                                                </tbody>
                                           </table> <br>";

                    stringBuilder.Append(SecondTable);

                    var NewDonacion_ = await Donacion_.Where(x => x.codigoDonacion == DonacionId).FirstOrDefaultAsync();

                    string articulosList = @"";
                    foreach (var item in ArticulosDonacionList)
                    {
                        articulosList = articulosList + item.cantidad + @" " + item.nombreArticulo + @"<br> ";
                    }

                    string boletasList = @"";
                    if (flagPreActa != 1)
                    {
                        foreach (var item in BoletasDonacionList)
                        {
                            boletasList = boletasList + item.numeroVoucher + @" |   Nota: " + item.comentario + @"<br> ";
                        }
                    }

                    string hora = DateTime.Now.ToString("HH");
                    string minuto = DateTime.Now.ToString("mm");
                    string mes = DateTime.Now.ToString("MMMM");
                    string dia = DateTime.Now.ToString("dd");
                    string anio = DateTime.Now.ToString("yyyy");

                    string SixthTable = @"<table
                                            style='width: 100%; font-family: Times New Roman, Helvetica, sans-serif;
                                                font-size: 16px; line-height: color:#000000'
                                            class='tableBorder'>
                                            <tbody>
                                              <tr >
                                                <td style='font-weight: bold; font-family: Times New Roman, Helvetica, sans-serif; background-color:#000000; color:#ffffff' >DATOS DE DONACIÓN</td>
                                              </tr>
                                              <tr class='trBorder'>
                                                <td class='tdBorder fields' style=' width: 100px; max-width: 100px; font-family: Times New Roman, sans-serif; font-style: italic;
                                                    text-align: justify; font-size: 16px; line-height: 20px; padding:15px; ' >
                                                  <br>
                                                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Siendo la(s) " + hora + @" y " + minuto + @" horas del dia " + dia + @" de " + mes + @" del " + anio + @", en presencia del" +
                                                  @" &nbsp" + decano + @"," +
                                                  @" de la " + jefeOficAdm + @", y del (la) donante: &nbsp;" +
                                                  @" <b> " + NewDonacion_.estudiante + @" con DNI: " + NewDonacion_.numeroDocumento + @" &nbsp </b>," +
                                                  @" dio inicio al protocolo de entrega de donación para la Faculta de Ingeniería Industrial y de Sistemas, consiste en: 
                                                    <br>
                                                    <div> 
                                                        <center>" + articulosList + @"</center>
                                                    </div>
                                                    <br>
                                                    Adjuntado para ello, Boleta Electrónica:
                                                    <br>
                                                    <div>
                                                        <center>" + boletasList + @"</center>
                                                        <br>
                                                        <center>
                                                        Por el monto de " + NewDonacion_.montototal + @"
                                                        </center>
                                                    </div>
                                                    <br>
                                                    Culminó la reunión con las palabras de agradecimiento del <b> &nbsp" + decano + @" </b>; hacia el donante.
                                                    <br>
                                                </td>
                                              </tr>
                                            </tbody>
                                          </table>";

                    stringBuilder.Append(SixthTable);


                    string ThirteenthTable = @"<table id='antecedentes' style='width: 100%; font-family: Arial, Helvetica, sans-serif; font-size: 12px; color:#000000'  class='tableBorder'>
                                                <tbody>                                             
                                                  <tr>
                                                    <td colspan='2' style='padding: 15px 15px ;'>
                                                      <table style='width: 100%; font-family: Arial, Helvetica, sans-serif; font-size: 12px; color:#000000;'  class='tableBorder'>
                                                        <tbody  >
                                                          <tr style='text-align: center;'>
                                                            <td class='tdBorder' style='width: 33.3%; padding-top: 120px;' >Decano</td>
                                                            <td class='tdBorder' style='width: 33.3%; padding-top: 120px;'>Jefe de la Oficina de Administración</td>
                                                            <td class='tdBorder' style='width: 33.3%; padding-top: 120px;'>Donante</td>
                                                          </tr>
                                                        </tbody>
                                                      </table>
                                                    </td>
                                                  </tr>
                                                  <!--<tr>
                                                    <td colspan='2' style='padding: 8px 3px ;line-height: 15px;' >
                                                      <span style='font-weight: bold;display: block;'>OBSERVACIONES:</span>

                                                    </td>
                                                  </tr>-->

                                                </tbody>
                                              </table>";

                    stringBuilder.Append(ThirteenthTable);

                    string Bottom = @"</body> </html>";
                    stringBuilder.Append(Bottom);

                    var result = GeneratePdf(stringBuilder.ToString());

                    await Task.Delay(1);
                    stringBuilder.Clear();
                    stringBuilder = null;
                    eobjFileDownLoadModel.FileBase64 = Convert.ToBase64String(result);

                    if (flagPreActa == 1)
                    {
                        eobjFileDownLoadModel.FileName = "Pre_Acta_Donacion_" + codigodonacion + ".pdf";
                    }
                    else
                    {
                        eobjFileDownLoadModel.FileName = "Acta_Donacion_" + codigodonacion + ".pdf";
                    }


                }

                return eobjFileDownLoadModel;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private byte[] GeneratePdf(string htmlContent)
        {
            try
            {
                var globalSettings = new GlobalSettings
                {
                    ColorMode = ColorMode.Color,
                    Orientation = Orientation.Portrait,
                    PaperSize = PaperKind.A4,
                    Margins = new MarginSettings { Top = 5, Bottom = 5, Left = 5, Right = 5 },
                };

                var objectSettings = new ObjectSettings
                {
                    PagesCount = true,
                    HtmlContent = htmlContent,
                    WebSettings = { DefaultEncoding = "utf-8" },
                    HeaderSettings = { FontSize = 10, Right = "", Line = false },
                    FooterSettings = { FontSize = 8, Center = "", Line = false },
                };

                var htmlToPdfDocument = new HtmlToPdfDocument()
                {
                    GlobalSettings = globalSettings,
                    Objects = { objectSettings },
                };

                return _converter.Convert(htmlToPdfDocument);
               
            }
            catch (Exception ex)
            {

                throw ex;
            }            

        }

        public async Task<ResultDto> ChangeStatusDonacion(Donacion objDonación)
        {
            decimal DonacionId = objDonación.CodigoDonacion;

            using (var bdAsuntosAdministrativos = new AsuntosAdministrativosDbContext())
            {
                var Donacion_ = await bdAsuntosAdministrativos.Donacions.Where(x => x.CodigoDonacion == DonacionId).FirstOrDefaultAsync();
                using TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

                if (Donacion_ != null)
                {
                    try
                    {
                        Donacion_.CodigoEstado = objDonación.CodigoEstado;
                        bdAsuntosAdministrativos.Donacions.Attach(Donacion_);
                        await bdAsuntosAdministrativos.SaveChangesAsync();

                        //Buscar solicitudes
                        var SolicitudList = await bdAsuntosAdministrativos.DonacionDetalles.Where(x => x.CodigoDonacion == DonacionId).ToListAsync();

                        foreach (var item in SolicitudList)
                        {
                            if (objDonación.CodigoEstado == 8) //Rechazado, se liberan la solicitudes
                            {
                                var Solicitud_Det = await bdAsuntosAdministrativos.DonacionDetalles.Where(x => x.CodigoDonacionDetalle == item.CodigoDonacionDetalle).FirstOrDefaultAsync();
                                if (Solicitud_Det != null)
                                {
                                    Solicitud_Det.Habilitado = false;
                                    bdAsuntosAdministrativos.DonacionDetalles.Attach(Solicitud_Det);
                                    await bdAsuntosAdministrativos.SaveChangesAsync();
                                }
                            }                          


                            var Solicitud_ = await bdAsuntosAdministrativos.Solicituds.Where(x => x.CodigoSolicitud == item.CodigoSolicitud).FirstOrDefaultAsync();
                            if (Solicitud_ != null)
                            {
                                string comentarioSolicitud = "";
                                if (objDonación.CodigoEstado == 7)
                                {
                                    comentarioSolicitud = "Donación Aprobada";
                                    Solicitud_.CodigoEstado = 3;
                                }
                                else
                                {
                                    comentarioSolicitud = "Donación Rechazada";
                                    Solicitud_.CodigoEstado = 1;
                                }
                                bdAsuntosAdministrativos.Solicituds.Attach(Solicitud_);
                                await bdAsuntosAdministrativos.SaveChangesAsync();


                                HistorialSolicitud historiaSolicitud = new HistorialSolicitud()
                                {
                                    CodigoSolicitud = Convert.ToInt32(Solicitud_.CodigoSolicitud),
                                    Comentario = comentarioSolicitud,
                                    CodigoEstado = Solicitud_.CodigoEstado,
                                    FechaCreacion = DateTime.Now,
                                    CodigoUsuarioCreacion = objDonación.CodigoUsuarioCreacion
                                };
                                decimal historiaSolicitudId = 0;
                                bdAsuntosAdministrativos.HistorialSolicituds.Add(historiaSolicitud);
                                await bdAsuntosAdministrativos.SaveChangesAsync();
                                historiaSolicitudId = historiaSolicitud.CodigoHistorialSolicitud;
                            }
                        }

                        string comentario = "";
                        if (objDonación.CodigoEstado == 7)
                        {
                            comentario = "Donación Aprobada";
                        }
                        else
                        {
                            comentario = "Donación Rechazada";
                        }
                        HistorialDonacion historiaDonacion = new HistorialDonacion()
                        {
                            CodigoDonacion = Convert.ToInt32(DonacionId),
                            Comentario = comentario,
                            CodigoEstado = objDonación.CodigoEstado,
                            FechaCreacion = DateTime.Now,
                            CodigoUsuarioCreacion = objDonación.CodigoUsuarioCreacion
                        };
                        decimal historiaDonacionId = 0;
                        bdAsuntosAdministrativos.HistorialDonacions.Add(historiaDonacion);
                        await bdAsuntosAdministrativos.SaveChangesAsync();
                        historiaDonacionId = historiaDonacion.CodigoHistorialDonacion;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                scope.Complete();
            }

            ResultDto eobj = new ResultDto();
            eobj.Id = Convert.ToInt32(DonacionId);
            eobj.Descripcion = "Well done";
            eobj.Success = true;
            return eobj;
        }

        public async Task<ResultDto> CreateDonacion(DonacionDto objDonación)
        {
            decimal DonacionId = objDonación.CodigoDonacion;

            using (var bdAsuntosAdministrativos = new AsuntosAdministrativosDbContext())
            {
                var Donacion_ = await bdAsuntosAdministrativos.Donacions.Where(x => x.CodigoDonacion == DonacionId).FirstOrDefaultAsync();
                using TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

                if (Donacion_ == null)
                {
                    Donacion donacion = new Donacion()
                    {
                        CodigoUsuarioEstudiante = objDonación.CodigoUsuarioEstudiante,
                        MontoTotal = objDonación.MontoTotal,
                        CodigoEstado = 6,
                        CodigoUsuarioCreacion = objDonación.CodigoUsuarioCreacion,
                        FechaCreacion = DateTime.Now
                    };

                    try
                    {
                        bdAsuntosAdministrativos.Donacions.Add(donacion);
                        await bdAsuntosAdministrativos.SaveChangesAsync();
                        DonacionId = donacion.CodigoDonacion;
                         
                        foreach (var item in objDonación.SolicitudList)
                        {
                            var totalDonacionesxSolicitud = bdAsuntosAdministrativos.DonacionDetalles
                                .Where(item => item.CodigoSolicitud == item.CodigoSolicitud)
                                .Sum(x => x.Cantidad);

                            var totalCantidadSolicitud = totalDonacionesxSolicitud + item.Cantidad;

                            DonacionDetalle donacionDetalle = new DonacionDetalle()
                            {
                                CodigoDonacion = Convert.ToInt32(DonacionId),
                                CodigoSolicitud = Convert.ToInt32(item.CodigoSolicitud),
                                Cantidad = Convert.ToInt32(item.Cantidad),
                                Habilitado = true,
                            };

                            bdAsuntosAdministrativos.DonacionDetalles.Add(donacionDetalle);
                            await bdAsuntosAdministrativos.SaveChangesAsync();

                            var Solicitud_ = await bdAsuntosAdministrativos.Solicituds.Where(x => x.CodigoSolicitud == item.CodigoSolicitud).FirstOrDefaultAsync();
                            if (Solicitud_ != null)
                            {
                                try
                                {
                                    var Comentario = "";
                                    if (totalCantidadSolicitud == Solicitud_.Cantidad) {
                                        Solicitud_.CodigoEstado = 2;
                                        Comentario = "Se incluyó la solicitud en donación  N° " + DonacionId;
                                    }
                                    else {
                                        Comentario = "Se incluyó parcialmente la solicitud en donación  N° " + DonacionId;
                                        Solicitud_.CodigoEstado = 9;
                                    }
                                     
                                    bdAsuntosAdministrativos.Solicituds.Attach(Solicitud_);
                                    await bdAsuntosAdministrativos.SaveChangesAsync();

                                    HistorialSolicitud historiaSolicitud = new HistorialSolicitud()
                                    {
                                        CodigoSolicitud = Convert.ToInt32(Solicitud_.CodigoSolicitud),
                                        Comentario = Comentario,
                                        CodigoEstado = Solicitud_.CodigoEstado,
                                        FechaCreacion = DateTime.Now,
                                        CodigoUsuarioCreacion = objDonación.CodigoUsuarioCreacion
                                    };

                                    decimal historiaSolicitudId = 0;
                                    bdAsuntosAdministrativos.HistorialSolicituds.Add(historiaSolicitud);
                                    await bdAsuntosAdministrativos.SaveChangesAsync();
                                    historiaSolicitudId = historiaSolicitud.CodigoHistorialSolicitud;
                                }
                                catch (Exception ex)
                                {
                                    throw ex;
                                }
                            }
                        }


                        HistorialDonacion historiaDonacion = new HistorialDonacion()
                        {
                            CodigoDonacion = Convert.ToInt32(DonacionId),
                            Comentario = "Donación Nueva",
                            CodigoEstado = 6,
                            FechaCreacion = DateTime.Now,
                            CodigoUsuarioCreacion = objDonación.CodigoUsuarioCreacion
                        };
                        decimal historialDonacionId = 0;
                        bdAsuntosAdministrativos.HistorialDonacions.Add(historiaDonacion);
                        await bdAsuntosAdministrativos.SaveChangesAsync();
                        historialDonacionId = historiaDonacion.CodigoHistorialDonacion;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

                scope.Complete();
            }

            ResultDto eobj = new ResultDto();
            eobj.Id = Convert.ToInt32(DonacionId);
            eobj.Descripcion = "Well done";
            eobj.Success = true;
            return eobj;
        }

        public async Task<DonacionDto> GetDonacionById(decimal codigoDonacion)
        {
            decimal DonacionId = codigoDonacion;
            DonacionDto donacionDto = new DonacionDto();
            using (var bdAsuntosAdministrativos = new AsuntosAdministrativosDbContext())
            {
                var Donacion_ = from donacion in bdAsuntosAdministrativos.Donacions.Where(x => x.CodigoDonacion == DonacionId)
                                join Estudiante in bdAsuntosAdministrativos.Usuarios on donacion.CodigoUsuarioEstudiante equals Estudiante.CodigoUsuario
                                select new
                                {
                                    MontoTotal = donacion.MontoTotal,
                                    CodigoUsuarioEstudiante = donacion.CodigoUsuarioEstudiante,
                                    estudiante = Estudiante.Nombre + " " + Estudiante.ApellidoPaterno + " " + Estudiante.ApellidoMaterno,
                                    numeroDocumento = Estudiante.NumeroDocumento
                                };

                var result = await Donacion_.FirstOrDefaultAsync();

                if (result != null)
                {
                    donacionDto.MontoTotal = Convert.ToDecimal(result.MontoTotal);
                    donacionDto.CodigoUsuarioEstudiante = Convert.ToInt32(result.CodigoUsuarioEstudiante);
                    donacionDto.Estudiante = result.estudiante;
                    donacionDto.NumeroDocumento = result.numeroDocumento;
                }
            }
            return donacionDto;
        }

        public async Task<ResultDto> CreateDonacionArchivos(DonacionDto objDonación)
        {
            decimal DonacionId = objDonación.CodigoDonacion;

            using (var bdAsuntosAdministrativos = new AsuntosAdministrativosDbContext())
            {
                var Donacion_ = await bdAsuntosAdministrativos.Donacions.Where(x => x.CodigoDonacion == DonacionId).FirstOrDefaultAsync();
                using TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);
                if (Donacion_ != null)
                {
                    try
                    {
                        foreach (var item in objDonación.DocumentList)
                        {
                            DonacionArchivo donacionArchivo = new DonacionArchivo()
                            {
                                CodigoDonacion = Convert.ToInt32(DonacionId),
                                Extension = item.Extension,
                                NombreArchivo = item.NombreArchivo,
                                NumeroVoucher = item.NumeroVoucher,
                                RutaArchivo = item.RutaArchivo,
                                Habilitado = true,
                                TipoArchivo = item.TipoArchivo,
                                CodigoUsuarioCreacion = 1,
                                FechaCreacion = DateTime.Now,
                                Nota = item.Nota
                            };

                            bdAsuntosAdministrativos.DonacionArchivos.Add(donacionArchivo);
                            await bdAsuntosAdministrativos.SaveChangesAsync();
                        }

                    }
                    catch (Exception)
                    {

                        throw;
                    }

                }
                scope.Complete();
            }

            ResultDto eobj = new ResultDto();
            eobj.Id = Convert.ToInt32(DonacionId);
            eobj.Descripcion = "Well done";
            eobj.Success = true;
            return eobj;
        }

        public async Task<List<FileDto>> GetArchivosDonacion(decimal codigoDonacion)
        {
            using (var bdAsuntosAdministrativos = new AsuntosAdministrativosDbContext())
            {
                try
                {
                    var ArchivoList = await bdAsuntosAdministrativos.DonacionArchivos.Where(x => x.CodigoDonacion == codigoDonacion).ToListAsync();

                    return ArchivoList.Select(x => new FileDto()
                    {
                        CodigoFile = x.CodigoDonacionArchivo,
                        Extension = x.Extension,
                        NombreArchivo = x.NombreArchivo,
                        Nota = x.Nota,
                        NumeroVoucher = x.NumeroVoucher,
                        RutaArchivo = x.RutaArchivo,
                        TipoArchivo = x.TipoArchivo
                    }).ToList();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public async Task<List<PDFArticuloDto>> GetAllArticuloByDonacion(decimal codigoDonacion)
        {
            using (var bdAsuntosAdministrativos = new AsuntosAdministrativosDbContext())
            {
                try
                {
                    //var ArticuloList = await bdAsuntosAdministrativos.Articulos.Where(x => x.Habilitado == true).ToListAsync();

                    var ArticulosDonacionList = from donacionDetalle in bdAsuntosAdministrativos.DonacionDetalles.Where(x => x.CodigoDonacion == codigoDonacion)
                                                join solicitud in bdAsuntosAdministrativos.Solicituds on donacionDetalle.CodigoSolicitud equals solicitud.CodigoSolicitud
                                                join articulo in bdAsuntosAdministrativos.Articulos on solicitud.CodigoArticulo equals articulo.CodigoArticulo
                                                select new
                                                {
                                                    cantidad = solicitud.Cantidad,
                                                    nombreArticulo = articulo.Descripcion
                                                };

                    return ArticulosDonacionList.Select(x => new PDFArticuloDto()
                    {
                        cantidad = Convert.ToInt32(x.cantidad),
                        Descripcion = x.nombreArticulo
                    }).ToList();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public async Task<List<PDFBoletaDto>> GetAllBoletasByDonacion(decimal codigoDonacion)
        {
            using (var bdAsuntosAdministrativos = new AsuntosAdministrativosDbContext())
            {
                try
                {
                    var BoletasDonacionList = from donacionArchivos in bdAsuntosAdministrativos.DonacionArchivos.Where(x => x.CodigoDonacion == codigoDonacion)
                                              where donacionArchivos.TipoArchivo == "1" //Solo los tipo que son boletas 1. Boleta 2. Otros
                                              select new
                                              {
                                                  numeroVoucher = donacionArchivos.NumeroVoucher,
                                                  comentario = donacionArchivos.Nota
                                              };

                    return BoletasDonacionList.Select(x => new PDFBoletaDto()
                    {
                        numeroVoucher = x.numeroVoucher,
                        comentario = x.comentario
                    }).ToList();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

    }
}
