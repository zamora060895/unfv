﻿using System;
using Entidad.Repositorio.Models;
using System.Data.SqlClient;
using System.Linq;
using System.Transactions;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Datos.DTO;
using Microsoft.EntityFrameworkCore;

namespace Datos
{
    public class UsuarioDA
    {
        public async Task<DataCollection<UsuarioDto>> GetAllUsuarioPaginate(string nombre, string numeroDocumento, string login, string page, string size)
        {
            List<Usuario> lstUsuarios = new List<Usuario>();
            using (var bdAsuntosAdministrativos = new AsuntosAdministrativosDbContext())
            {
                try
                {
                    var UsuarioList = from usuario in bdAsuntosAdministrativos.Usuarios 
                                      where usuario.CodigoEstudiante == string.Empty
                                      select new
                                      {
                                          codigoUsuario = usuario.CodigoUsuario,
                                          nombre = usuario.Nombre,
                                          apellidoPaterno = usuario.ApellidoPaterno,
                                          apellidoMaterno = usuario.ApellidoMaterno,
                                          numeroDocumento = usuario.NumeroDocumento,
                                          login = usuario.Usuario1,
                                          contrasenia = usuario.Contrasenia,
                                          habilitado = usuario.Habilitado,
                                          codigoestudiante = usuario.CodigoEstudiante
                                      };

                    UsuarioList = UsuarioList.OrderByDescending(x => x.codigoUsuario);

                    if (nombre != "" && nombre != null)
                    {
                        UsuarioList = UsuarioList.Where(x => x.nombre.Contains(nombre)|| x.apellidoPaterno.Contains(nombre)|| x.apellidoMaterno.Contains(nombre));
                    }

                    if (numeroDocumento != "" && numeroDocumento != null)
                    {
                        UsuarioList = UsuarioList.Where(x => x.numeroDocumento.Contains(numeroDocumento));
                    }

                    if (login != "" && login != null)
                    {
                        UsuarioList = UsuarioList.Where(x => x.login.Contains(login));
                    }

                    var resultList = await UsuarioList.GetPagedAsync(Convert.ToInt32(page), Convert.ToInt32(size));

                    DataCollection<UsuarioDto> placementCustomDto = new DataCollection<UsuarioDto>()
                    {
                        Page = resultList.Page,
                        Pages = resultList.Pages,
                        Total = resultList.Total,
                        Items = resultList.Items.Select(x => new UsuarioDto()
                        {
                            CodigoUsuario = x.codigoUsuario,
                            Nombre = x.nombre,
                            ApellidoPaterno = x.apellidoPaterno,
                            ApellidoMaterno = x.apellidoMaterno,
                            NumeroDocumento = x.numeroDocumento,
                            Login = x.login,
                            Contrasenia = x.contrasenia,
                            Estado = x.habilitado == true ? "Activo" : "Inactivo",
                            CodigoEstudiante = x.codigoestudiante

                        })
                    };

                    return placementCustomDto;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public Usuario Validar_Sesion_Por_Usuario(string usuario, string password)
        {
            using (var bdAsuntosAdministrativos = new AsuntosAdministrativosDbContext())
            {
                try
                {
                    var objUsuario = bdAsuntosAdministrativos.Usuarios.Where(a => a.Usuario1 == usuario && a.Contrasenia == password).FirstOrDefault();
                    return objUsuario;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public async Task<ResultDto> InsertUsuario(UsuarioDto objUsuario)
        {
            decimal UsuarioId = objUsuario.CodigoUsuario;

            using (var bdAsuntosAdministrativos = new AsuntosAdministrativosDbContext())
            {
                var Usuario = await bdAsuntosAdministrativos.Usuarios.Where(x => x.CodigoUsuario == UsuarioId).FirstOrDefaultAsync();
                using TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

                if (Usuario != null)
                {
                    try
                    {
                        Usuario.Nombre = objUsuario.Nombre;
                        Usuario.ApellidoPaterno = objUsuario.ApellidoPaterno;
                        Usuario.ApellidoMaterno = objUsuario.ApellidoMaterno;
                        Usuario.NumeroDocumento = objUsuario.NumeroDocumento;
                        Usuario.CorreoElectronico = objUsuario.CorreoElectronico;
                        Usuario.Usuario1 = objUsuario.Login;
                        Usuario.Contrasenia = objUsuario.Contrasenia;
                        Usuario.CodigoEstudiante = objUsuario.CodigoEstudiante;
                        Usuario.Habilitado = objUsuario.Habilitado;
                        bdAsuntosAdministrativos.Usuarios.Attach(Usuario);
                        await bdAsuntosAdministrativos.SaveChangesAsync();

                        //Borrar roles y crearlos de nuevo
                        var registrosAEliminar = bdAsuntosAdministrativos.UsuarioRols.Where(t => t.CodigoUsuario == UsuarioId);
                        bdAsuntosAdministrativos.UsuarioRols.RemoveRange(registrosAEliminar);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                else
                {
                    Usuario usuario = new Usuario()
                    {
                        Nombre = objUsuario.Nombre,
                        ApellidoPaterno = objUsuario.ApellidoPaterno,
                        ApellidoMaterno = objUsuario.ApellidoMaterno,
                        NumeroDocumento = objUsuario.NumeroDocumento,
                        CorreoElectronico = objUsuario.CorreoElectronico,
                        Usuario1 = objUsuario.Login,
                        Contrasenia = objUsuario.Contrasenia,
                        CodigoEstudiante = objUsuario.CodigoEstudiante,
                        Habilitado = true,
                        FechaCreacionRegistro = DateTime.Now
                    };

                    try
                    {
                        bdAsuntosAdministrativos.Usuarios.Add(usuario);
                        await bdAsuntosAdministrativos.SaveChangesAsync();
                        UsuarioId = usuario.CodigoUsuario;

                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }


                if (objUsuario.Administrador)
                {
                    UsuarioRol objUsarioRol = new UsuarioRol()
                    {
                        CodigoUsuario = Convert.ToInt32(UsuarioId),
                        CodigoRol = 1,
                        FechaCreacionRegistro = DateTime.Now,
                        Habilitado = true
                    };
                    bdAsuntosAdministrativos.UsuarioRols.Add(objUsarioRol);
                    await bdAsuntosAdministrativos.SaveChangesAsync();
                }

                if (objUsuario.OperadorSolicitudes)
                {
                    UsuarioRol objUsarioRol = new UsuarioRol()
                    {
                        CodigoUsuario = Convert.ToInt32(UsuarioId),
                        CodigoRol = 3,
                        FechaCreacionRegistro = DateTime.Now,
                        Habilitado = true
                    };
                    bdAsuntosAdministrativos.UsuarioRols.Add(objUsarioRol);
                    await bdAsuntosAdministrativos.SaveChangesAsync();
                }

                if (objUsuario.OperadorDonaciones)
                {
                    UsuarioRol objUsarioRol = new UsuarioRol()
                    {
                        CodigoUsuario = Convert.ToInt32(UsuarioId),
                        CodigoRol = 2,
                        FechaCreacionRegistro = DateTime.Now,
                        Habilitado = true
                    };
                    bdAsuntosAdministrativos.UsuarioRols.Add(objUsarioRol);
                    await bdAsuntosAdministrativos.SaveChangesAsync();
                }

                scope.Complete();

            }


            ResultDto eobj = new ResultDto();

            eobj.Id = Convert.ToInt32(UsuarioId);
            eobj.Descripcion = "Well done";
            eobj.Success = true;

            return eobj;
        }

        public async Task<UsuarioDto> GetUsuarioById(decimal codigoUsuario)
        {
            decimal UsuarioId = codigoUsuario;
            UsuarioDto usuariodto = new UsuarioDto();
            using (var bdAsuntosAdministrativos = new AsuntosAdministrativosDbContext())
            {
                var Usuario = await bdAsuntosAdministrativos.Usuarios.Where(x => x.CodigoUsuario == UsuarioId).FirstOrDefaultAsync();
                if (Usuario != null)
                {

                    var RoleList = from usuarioRol in bdAsuntosAdministrativos.UsuarioRols
                                   where usuarioRol.CodigoUsuario == codigoUsuario
                                   select new
                                   {
                                       codigoUsuario = usuarioRol.CodigoUsuario,
                                       codigoRol = usuarioRol.CodigoRol
                                   };

                    usuariodto.Administrador = false;
                    usuariodto.OperadorDonaciones = false;
                    usuariodto.OperadorSolicitudes = false;

                    foreach (var item in RoleList)
                    {
                        if (item.codigoRol == 1)
                        {
                            usuariodto.Administrador = true;
                        }
                        if (item.codigoRol == 2)
                        {
                            usuariodto.OperadorDonaciones = true;
                        }
                        if (item.codigoRol == 3)
                        {
                            usuariodto.OperadorSolicitudes = true;
                        }
                    }

                    usuariodto.Nombre = Usuario.Nombre;
                    usuariodto.ApellidoPaterno = Usuario.ApellidoPaterno;
                    usuariodto.ApellidoMaterno = Usuario.ApellidoMaterno;
                    usuariodto.NumeroDocumento = Usuario.NumeroDocumento;
                    usuariodto.Login = Usuario.Usuario1;
                    usuariodto.Contrasenia = Usuario.Contrasenia;
                    usuariodto.CorreoElectronico = Usuario.CorreoElectronico;
                    usuariodto.CodigoEstudiante = Usuario.CodigoEstudiante;
                }
            }

            return usuariodto;
        }

        public async Task<ResultDto> ChangeStatusUsuario(Usuario objUsuario)
        {
            decimal UsuarioId = objUsuario.CodigoUsuario;

            using (var bdAsuntosAdministrativos = new AsuntosAdministrativosDbContext())
            {
                var Usuario = await bdAsuntosAdministrativos.Usuarios.Where(x => x.CodigoUsuario == UsuarioId).FirstOrDefaultAsync();
                using TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

                if (Usuario != null)
                {
                    try
                    {
                        Usuario.Habilitado = objUsuario.Habilitado;
                        bdAsuntosAdministrativos.Usuarios.Attach(Usuario);
                        await bdAsuntosAdministrativos.SaveChangesAsync();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

                scope.Complete();

            }


            ResultDto eobj = new ResultDto();

            eobj.Id = Convert.ToInt32(UsuarioId);
            eobj.Descripcion = "Well done";
            eobj.Success = true;

            return eobj;
        }

        public async Task<ResultDto> Validar_Existencia_Usuario(decimal? codigousuario, string numeroDocumento, string login, string codigoEstudiante)
        {
            ResultDto eobj = new ResultDto();
            using (var bdAsuntosAdministrativos = new AsuntosAdministrativosDbContext())
            {
                if (codigousuario == null)
                {
                    codigousuario = 0;
                }
                var Usuario = await bdAsuntosAdministrativos.Usuarios.Where(x => (x.NumeroDocumento == numeroDocumento || x.Usuario1 == login) && x.Habilitado == true && x.CodigoUsuario != codigousuario).FirstOrDefaultAsync();
                using TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

                eobj.Id = 1;
                eobj.Descripcion = "Well done";
                eobj.Success = true;

                if (Usuario != null)
                {
                    eobj.Id = Convert.ToInt32(Usuario.CodigoUsuario);
                   
                    var UsuarioDNI = await bdAsuntosAdministrativos.Usuarios.Where(x => (x.NumeroDocumento == numeroDocumento) && x.Habilitado == true && x.CodigoUsuario != codigousuario).FirstOrDefaultAsync();
                    if (UsuarioDNI != null)
                    {
                        eobj.Descripcion = "Ya existe un usuario con el mismo número de documento";
                    }
                    else
                    {
                        eobj.Descripcion = "Ya existe un usuario con el mismo login";
                    }
                    eobj.Success = false;
                }

                if (codigoEstudiante != "")
                {
                    var UsuarioEstud = await bdAsuntosAdministrativos.Usuarios.Where(x => (x.CodigoEstudiante == codigoEstudiante) && x.Habilitado == true && x.CodigoUsuario != codigousuario).FirstOrDefaultAsync();

                    if (UsuarioEstud != null)
                    {
                        eobj.Id = Convert.ToInt32(Usuario.CodigoUsuario);
                        eobj.Descripcion = "Ya existe un usuario con el mismo código de estudiante";
                        eobj.Success = false;
                    }
                }

                scope.Complete();
            }
            return eobj;
        }

        public async Task<DataCollection<UsuarioDto>> GetAllUserPaginateForSearchValue(string searchValue, string Page, string Size)
        {
            List<Usuario> lstUsuarios = new List<Usuario>();
            using (var bdAsuntosAdministrativos = new AsuntosAdministrativosDbContext())
            {
                try
                {
                    var UsuarioList = from usuario in bdAsuntosAdministrativos.Usuarios where (usuario.CodigoEstudiante != String.Empty && usuario.CodigoEstudiante != null)
                                      select new
                                      {
                                          codigoUsuario = usuario.CodigoUsuario,
                                          nombre = usuario.Nombre,
                                          apellidoPaterno = usuario.ApellidoPaterno,
                                          apellidoMaterno = usuario.ApellidoMaterno,
                                          numeroDocumento = usuario.NumeroDocumento,
                                          login = usuario.Usuario1,
                                          contrasenia = usuario.Contrasenia,
                                          habilitado = usuario.Habilitado,
                                          codigoestudiante = usuario.CodigoEstudiante
                                      };

                    UsuarioList = UsuarioList.OrderByDescending(x => x.codigoUsuario);

                    if (searchValue != "" && searchValue != null)
                    {
                        UsuarioList = UsuarioList.Where(x => x.nombre.Contains(searchValue) || x.apellidoPaterno.Contains(searchValue) || x.apellidoMaterno.Contains(searchValue) || x.numeroDocumento.Contains(searchValue));
                    }


                    var resultList = await UsuarioList.GetPagedAsync(Convert.ToInt32(Page), Convert.ToInt32(Size));

                    DataCollection<UsuarioDto> placementCustomDto = new DataCollection<UsuarioDto>()
                    {
                        Page = resultList.Page,
                        Pages = resultList.Pages,
                        Total = resultList.Total,
                        Items = resultList.Items.Select(x => new UsuarioDto()
                        {
                            CodigoUsuario = x.codigoUsuario,
                            Nombre = x.nombre,
                            ApellidoPaterno = x.apellidoPaterno,
                            ApellidoMaterno = x.apellidoMaterno,
                            NumeroDocumento = x.numeroDocumento,
                            Login = x.login,
                            Contrasenia = x.contrasenia,
                            Estado = x.habilitado == true ? "Activo" : "Inactivo",
                            CodigoEstudiante = x.codigoestudiante

                        })
                    };

                    return placementCustomDto;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
    }
}
