﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidad.Repositorio.Models;
using Microsoft.EntityFrameworkCore;

namespace Datos
{
    public class UsuarioRolDA
    {
        public async Task<List<Rol>> Obtener_Roles_Por_Usuario(long codigo_usuario)
        {
            using (var bdAsuntosAdministrativos = new AsuntosAdministrativosDbContext())
            {
                try
                {
                    var roles = await(from userRole in bdAsuntosAdministrativos.UsuarioRols
                                      join role in bdAsuntosAdministrativos.Rols
                                      on userRole.CodigoRol equals role.CodigoRol
                                      where userRole.CodigoUsuario == Convert.ToDecimal(codigo_usuario)
                                      select new
                                      {
                                          role
                                      }).ToListAsync();

                    return roles.Select(x => new Rol()
                    {
                        CodigoRol = x.role.CodigoRol,
                        Nombre = x.role.Nombre
                    }).ToList();

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

    }
}
