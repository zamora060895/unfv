﻿using System;
using Entidad.Repositorio.Models;
using System.Data.SqlClient;
using System.Linq;
using System.Transactions;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Datos.DTO;
using Microsoft.EntityFrameworkCore;

namespace Datos
{
    public class CategoriaDA
    {
        public async Task<DataCollection<CategoriaDto>> GetAllCategoriaPaginate(string descripcion, string page, string size)
        {
            List<Usuario> lstUsuarios = new List<Usuario>();
            using (var bdAsuntosAdministrativos = new AsuntosAdministrativosDbContext())
            {
                try
                {
                    var CategoriaList = from categoria in bdAsuntosAdministrativos.Categoria
                                        select new
                                        {
                                            codigoCategoria = categoria.CodigoCategoria,
                                            descripcion = categoria.Descripcion,
                                            habilitado = categoria.Habilitado
                                        };

                    CategoriaList = CategoriaList.OrderByDescending(x => x.codigoCategoria);

                    if (descripcion != "" && descripcion != null)
                    {
                        CategoriaList = CategoriaList.Where(x => x.descripcion.Contains(descripcion));
                    }

                    var resultList = await CategoriaList.GetPagedAsync(Convert.ToInt32(page), Convert.ToInt32(size));

                    DataCollection<CategoriaDto> categoriaCustomDto = new DataCollection<CategoriaDto>()
                    {
                        Page = resultList.Page,
                        Pages = resultList.Pages,
                        Total = resultList.Total,
                        Items = resultList.Items.Select(x => new CategoriaDto()
                        {
                            CodigoCategoria = x.codigoCategoria,
                            Descripcion = x.descripcion,
                            Estado = x.habilitado == true ? "Activo" : "Inactivo"

                        })
                    };

                    return categoriaCustomDto;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public async Task<ResultDto> InsertCategoria(Categorium objCategoria)
        {
            decimal CategoriaId = objCategoria.CodigoCategoria;

            using (var bdAsuntosAdministrativos = new AsuntosAdministrativosDbContext())
            {
                var Category = await bdAsuntosAdministrativos.Categoria.Where(x => x.CodigoCategoria == CategoriaId).FirstOrDefaultAsync();
                using TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

                if (Category != null)
                {
                    try
                    {
                        Category.Descripcion = objCategoria.Descripcion;
                        Category.Habilitado = objCategoria.Habilitado;
                        bdAsuntosAdministrativos.Categoria.Attach(Category);
                        await bdAsuntosAdministrativos.SaveChangesAsync();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                else
                {
                    Categorium categoria = new Categorium()
                    {
                        Descripcion = objCategoria.Descripcion,
                        Habilitado = objCategoria.Habilitado
                    };

                    try
                    {
                        bdAsuntosAdministrativos.Categoria.Add(categoria);
                        await bdAsuntosAdministrativos.SaveChangesAsync();
                        CategoriaId = categoria.CodigoCategoria;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

                scope.Complete();

            }


            ResultDto eobj = new ResultDto();

            eobj.Id = Convert.ToInt32(CategoriaId);
            eobj.Descripcion = "Well done";
            eobj.Success = true;

            return eobj;
        }

        public async Task<CategoriaDto> GetCategoryById(decimal codigoCategoria)
        {
            decimal CategoriaId = codigoCategoria;
            CategoriaDto categoriadto = new CategoriaDto();
            using (var bdAsuntosAdministrativos = new AsuntosAdministrativosDbContext())
            {
                var Category = await bdAsuntosAdministrativos.Categoria.Where(x => x.CodigoCategoria == CategoriaId).FirstOrDefaultAsync();                
                if (Category != null)
                {

                    categoriadto.Descripcion = Category.Descripcion;
                }    
            }

            return categoriadto;
        }

        public async Task<ResultDto> ChangeStatusCategory(Categorium objCategoria)
        {
            decimal CategoriaId = objCategoria.CodigoCategoria;

            using (var bdAsuntosAdministrativos = new AsuntosAdministrativosDbContext())
            {
                var Category = await bdAsuntosAdministrativos.Categoria.Where(x => x.CodigoCategoria == CategoriaId).FirstOrDefaultAsync();
                using TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

                if (Category != null)
                {
                    try
                    {
                        Category.Habilitado = objCategoria.Habilitado;
                        bdAsuntosAdministrativos.Categoria.Attach(Category);
                        await bdAsuntosAdministrativos.SaveChangesAsync();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

                scope.Complete();

            }


            ResultDto eobj = new ResultDto();

            eobj.Id = Convert.ToInt32(CategoriaId);
            eobj.Descripcion = "Well done";
            eobj.Success = true;

            return eobj;
        }

        public async Task<List<CategoriaDto>> GetAllCategoria()
        {
            using (var bdAsuntosAdministrativos = new AsuntosAdministrativosDbContext())
            {
                try
                {
                    var CategoriaList = await bdAsuntosAdministrativos.Categoria.Where(x => x.Habilitado == true).ToListAsync();

                    return CategoriaList.Select(x => new CategoriaDto()
                    {
                        CodigoCategoria = x.CodigoCategoria,
                        Descripcion = x.Descripcion,
                        Estado = x.Habilitado == true ? "Activo" : "Inactivo"
                    }).ToList();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
    }
}
