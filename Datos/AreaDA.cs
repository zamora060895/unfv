﻿using System;
using Entidad.Repositorio.Models;
using System.Data.SqlClient;
using System.Linq;
using System.Transactions;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Datos.DTO;
using Microsoft.EntityFrameworkCore;

namespace Datos.DTO
{
    public class AreaDA
    {
        public async Task<DataCollection<AreaDto>> GetAllAreaPaginate(string descripcion, int codigoescuela, string page, string size)
        {
            using (var bdAsuntosAdministrativos = new AsuntosAdministrativosDbContext())
            {
                try
                {
                    var AreaList = from area in bdAsuntosAdministrativos.Areas
                                   join escuela in bdAsuntosAdministrativos.Escuelas on area.CodigoEscuela equals escuela.CodigoEscuela
                                   select new
                                        {
                                            codigoArea = area.CodigoArea,
                                            descripcion = area.Descripcion,
                                            habilitado = area.Habilitado,
                                            codigoescuela = area.CodigoEscuela,
                                            descripcionescuela =escuela.Descripcion
                                   };

                    AreaList = AreaList.OrderByDescending(x => x.codigoArea);

                    if (descripcion != "" && descripcion != null)
                    {
                        AreaList = AreaList.Where(x => x.descripcion.Contains(descripcion));
                    }

                    if (codigoescuela != 0)
                    {
                        AreaList = AreaList.Where(x => x.codigoescuela == codigoescuela);
                    }

                    var resultList = await AreaList.GetPagedAsync(Convert.ToInt32(page), Convert.ToInt32(size));

                    DataCollection<AreaDto> areaDto = new DataCollection<AreaDto>()
                    {
                        Page = resultList.Page,
                        Pages = resultList.Pages,
                        Total = resultList.Total,
                        Items = resultList.Items.Select(x => new AreaDto()
                        {
                            CodigoArea = x.codigoArea,
                            Descripcion = x.descripcion,
                            Estado = x.habilitado == true ? "Activo" : "Inactivo",
                            DescripcionEscuela = x.descripcionescuela
                        })
                    };

                    return areaDto;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public async Task<ResultDto> InsertArea(Area objArea)
        {
            decimal AreaId = objArea.CodigoArea;

            using (var bdAsuntosAdministrativos = new AsuntosAdministrativosDbContext())
            {
                var Area_ = await bdAsuntosAdministrativos.Areas.Where(x => x.CodigoArea == AreaId).FirstOrDefaultAsync();
                using TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

                if (Area_ != null)
                {
                    try
                    {
                        Area_.Descripcion = objArea.Descripcion;
                        Area_.Habilitado = objArea.Habilitado;
                        Area_.CodigoEscuela = objArea.CodigoEscuela;
                        bdAsuntosAdministrativos.Areas.Attach(Area_);
                        await bdAsuntosAdministrativos.SaveChangesAsync();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                else
                {
                    Area area = new Area()
                    {
                        Descripcion = objArea.Descripcion,
                        Habilitado = objArea.Habilitado,
                        CodigoEscuela =objArea.CodigoEscuela                        
                    };

                    try
                    {
                        bdAsuntosAdministrativos.Areas.Add(area);
                        await bdAsuntosAdministrativos.SaveChangesAsync();
                        AreaId = area.CodigoArea;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

                scope.Complete();
            }

            ResultDto eobj = new ResultDto();
            eobj.Id = Convert.ToInt32(AreaId);
            eobj.Descripcion = "Well done";
            eobj.Success = true;
            return eobj;
        }

        public async Task<AreaDto> GetAreaById(decimal codigoArea)
        {
            decimal AreaId = codigoArea;
            AreaDto areaDto = new AreaDto();
            using (var bdAsuntosAdministrativos = new AsuntosAdministrativosDbContext())
            {
                var Area_ = await bdAsuntosAdministrativos.Areas.Where(x => x.CodigoArea == AreaId).FirstOrDefaultAsync();
                if (Area_ != null)
                {
                    areaDto.CodigoEscuela =Convert.ToInt32(Area_.CodigoEscuela);
                    areaDto.Descripcion = Area_.Descripcion;
                }
            }
            return areaDto;
        }

        public async Task<ResultDto> ChangeStatusArea(Area objArea)
        {
            decimal AreaId = objArea.CodigoArea;

            using (var bdAsuntosAdministrativos = new AsuntosAdministrativosDbContext())
            {
                var Area_ = await bdAsuntosAdministrativos.Areas.Where(x => x.CodigoArea == AreaId).FirstOrDefaultAsync();
                using TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

                if (Area_ != null)
                {
                    try
                    {
                        Area_.Habilitado = objArea.Habilitado;
                        bdAsuntosAdministrativos.Areas.Attach(Area_);
                        await bdAsuntosAdministrativos.SaveChangesAsync();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                scope.Complete();
            }

            ResultDto eobj = new ResultDto();
            eobj.Id = Convert.ToInt32(AreaId);
            eobj.Descripcion = "Well done";
            eobj.Success = true;
            return eobj;
        }

        public async Task<List<EscuelaDto>> GetAllEscuela()
        {
            using (var bdAsuntosAdministrativos = new AsuntosAdministrativosDbContext())
            {
                try
                {
                    var EscuelaList = await bdAsuntosAdministrativos.Escuelas.Where(x => x.Habilitado == true).ToListAsync();

                    return EscuelaList.Select(x => new EscuelaDto()
                    {
                        CodigoEscuela = x.CodigoEscuela,
                        Descripcion = x.Descripcion,
                        Estado = x.Habilitado == true ? "Activo" : "Inactivo",
                        CodigoFacultad =Convert.ToInt32(x.CodigoFacultad),
                    }).ToList();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public async Task<List<AreaDto>> GetAllArea()
        {
            using (var bdAsuntosAdministrativos = new AsuntosAdministrativosDbContext())
            {
                try
                {
                    var AreaList = await bdAsuntosAdministrativos.Areas.Where(x => x.Habilitado == true).ToListAsync();

                    return AreaList.Select(x => new AreaDto()
                    {
                        CodigoArea = x.CodigoArea,
                        Descripcion = x.Descripcion,
                        Estado = x.Habilitado == true ? "Activo" : "Inactivo",
                        CodigoEscuela =Convert.ToInt32(x.CodigoEscuela),
                    }).ToList();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public async Task<List<FacultadDto>> GetAllFacultad()
        {
            using (var bdAsuntosAdministrativos = new AsuntosAdministrativosDbContext())
            {
                try
                {
                    var FacultadList = await bdAsuntosAdministrativos.Facultads.Where(x => x.Habilitado == true).ToListAsync();

                    return FacultadList.Select(x => new FacultadDto()
                    {
                        CodigoFacultad = x.CodigoFacultad,
                        Descripcion = x.Descripcion,
                        Estado = x.Habilitado == true ? "Activo" : "Inactivo"
                    }).ToList();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

    }
}
