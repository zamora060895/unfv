﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.SignalR;
using System.Linq;
using System.Threading.Tasks;

namespace Keralty.Presentation.PreAffiliation.Signalr
{
    public class SignalServer : Hub
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public SignalServer(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task AddGroupNotification()
        {
            var group = _httpContextAccessor.HttpContext
                         .User.Claims
                         .FirstOrDefault(claim => claim.Type == "GroupSignal");

            await Groups.AddToGroupAsync(Context.ConnectionId, group.Value);
        }
    }
}
