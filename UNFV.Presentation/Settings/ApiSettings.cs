﻿namespace Keralty.Presentation.PreAffiliation.Settings
{
    public class ApiSettings
    {
        public string Token { get; set; }
        public string BaseAddress { get; set; }
        public string DecanoFacultad { get; set; }
        public string JefeOficinaAdmin { get; set; }
        public string MontoMinimoDonacion { get; set; }
        public string GetByTokenUser { get; set; }
        public string GetMenuByTokenUser { get; set; }
        public string GetRoleByTokenUser { get; set; }

        //Administracion 
        public string GetAllIdentityDocumentType { get; set; }
        public string GetAllDistrict { get; set; }
        public string GetAllState { get; set; }
        public string GetAllDepartment { get; set; }
        public string GetAllUrbanizationType { get; set; }
        public string GetAllStreetType { get; set; }
        public string GetAllSunatCiiu { get; set; }
        public string GetAllProductType { get; set; }
        public string GetAllSubProductType { get; set; }
        public string GetAllRelationship { get; set; }
        public string GetAllRelationshipByValue { get; set; }
        public string GetAllMaritalStatus { get; set; }
        public string GetAllPlan { get; set; }
        //Broker 
        public string GetAllBrokerUniqueSearchValue { get; set; }

        public string GetAllManagerUniqueSearchValue { get; set; }

        public string GetUsersTeamLeader { get; set; }

        //PreAffiliation
        public string CreatePreAffiliation { get; set; }
        public string GetAllPreAffiliationPaginate { get; set; }
        public string GetPreAffiliationById { get; set; }
        public string PlotLoadPreAffiliation { get; set; }
        public string GetAllPreAffiliationType { get; set; }
        public string GetAllPreAffiliationPersonPaginate { get; set; }
        public string GetAllHealthConditionTitle { get; set; }
        public string GetAllPreviousHealthInsurance { get; set; }
        public string GetAllPreAffiliationPersonByPreAffiliationId { get; set; }
        public string CreateDeclarationHealthCond { get; set; }
        public string GetPreAffiliationMotiveById { get; set; }
        public string GetAllPreAffiliationStatus { get; set; }
        public string PreAffiliationCreateTracking { get; set; }
        public string GetPdfPreAffiliationById { get; set; }
        public string GetAllPreAffiliationByPlacementId { get; set; }
        public string GetAllPaymentPeriodicity { get; set; }
        public string CreatePlacementTracking { get; set; }
        public string UpdatePreAffilaitionByDiscount { get; set; }
        public string GetPdfPreAffiliationFamilyById { get; set; }
        public string GetPdfPreAffiliationContractAddById { get; set; }
        public string CreateFilesAddPreaffiliation { get; set; }
        public string GetAllPreAffiliationTrackingPaginate { get; set; }
        public string GetAllPlacementTrackingPaginate { get; set; }
        public string GetDocumentByPreAffiliationId { get; set; }
        public string GetAllCountry { get; set; }
        public string UpdatePreAffilaitionById { get; set; }
        public string GetPdfContractPotestativoPlacementById { get; set; }


        //Client
        public string GetAllClientPaginateForSearchValue { get; set; }
        public string CreateClient { get; set; }

        //Contact
        public string GetAllContactPaginateForSearchValue { get; set; }
        public string CreateContact { get; set; }

        //Placement 
        public string CreatePlacement { get; set; }
        public string UpdatePlacement { get; set; }
        public string CreatePlacementPerson { get; set; }
        public string GetAllPlacementPaginate { get; set; }
        public string GetPlacementById { get; set; }
        public string CreateDocument { get; set; }
        public string GetAllPlacementStatus { get; set; }
        public string CreateCompanyGroup { get; set; }
        public string GetAllCompanyGroupPaginate { get; set; }
        public string GetAllCalendarOperations { get; set; }
        public string CalculateAmountToPay { get; set; }
        public string GetPlanDocumentByPlanGroupId { get; set; }
        public string GetAllRangeAffiliate { get; set; }

    public string GetClientById { get; set; }
        public string UpdateClient { get; set; }

        //Previous Insurer
        public string GetAllPreviousInsurer { get; set; }
        public string GetAllHealtcareSystem { get; set; }
        public string GetAllProposedRate { get; set; }
        public string GetAllTypeLoad { get; set; }
        public string GetAllMandatoryDocument { get; set; }

        //Contractor
        public string CreateContractor { get; set; }

        //Files
        public string FileDownload { get; set; }
        public string FileDownloadPreaffiliationTemplate { get; set; }
        //Product
        public string GetAllPlanStandard { get; set; }
        public string GetAllPlanByIdentityDocument { get; set; }
        public string GetAllPlanGroupPaginate { get; set; }
        public string GetAllPlanGroupByIdentityDocument { get; set; }

    //Firma
    public string CreateCircuit { get; set; }
        public string UpdateCircuit { get; set; }
        public string GetCircuitByPreAffiliationId { get; set; }

    }
}
