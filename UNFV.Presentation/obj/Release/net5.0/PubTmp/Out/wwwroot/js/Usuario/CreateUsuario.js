﻿(function () {
    'use strict';


    Vue.use(VeeValidate, {
        locale: 'es',
        dictionary: {
            es: {

                custom:
                {
                    nombre: {
                        required: 'Ingresar Nombre',
                    },
                    apellidoPaterno: {
                        required: 'Ingresar apellido paterno',
                    },
                    apellidoMaterno: {
                        required: 'Ingresar apellido materno',
                    },
                    numeroDocumento: {
                        required: 'Ingresar número de documento',
                    },
                    apellidoPaterno: {
                        required: 'Ingresar apellido paterno',
                    },
                    correoElectronico: {
                        required: 'Ingresar correo electrónico',
                    },
                    usuario: {
                        required: 'Ingresar usuario',
                    },
                    contrasenia: {
                        required: 'Ingresar contraseña',
                    },
                }
            }
        }
    });

    Vue.use(bootstrapVue);

    Vue.directive("select", {
        twoWay: true,
        bind: function (el, binding, vnode) {
            $(el)
                .select2()
                .on("select2:select", (e) => {
                    el.dispatchEvent(new Event("change", { target: e.target }));
                });
        },
    });

    var app = new Vue({
        el: "#CreateCompleteUsuario",
        data: {
            usuarioId: 0,
            backupUsuario: {},
            update: false,
            prueba: "custom-select",
            //Create Client (Contractor)           
            nombre: '',
            apellidoPaterno: '',
            apellidoMaterno: '',
            numeroDocumento: '',
            correoElectronico: '',
            usuario: '',
            contrasenia: '',
            codigoEstudiante: '',

            administrator: false,
            opsolictudes: false,
            opdonaciones: false,
            NamesRoles: [
                { Id: 0, Name: "Administrador" },
                { Id: 0, Name: "Vendedor EPS" },
                { Id: 0, Name: "Operador Mesa de Control" },
                { Id: 0, Name: "Asistente comercial" },
                { Id: 0, Name: "Supervisor de solicitudes" },
                { Id: 0, Name: "Agente Comercial" },
            ]
        },
        computed: {
            filteredFiles() {
                return this.fileList.filter(x => x.Name.toLowerCase().includes(this.searchFile.toLowerCase()));
            },

        },
        watch: {
        },
        created: function () {
            this.run_waitMe();
            //this.GetListRoles();
            this.initialConfig();
        },
        mounted: function () {
            this.run_waitMe();
            //url params  
            var currentDate = moment().format("DD/MM/YYYY");
            this.saleDate = currentDate;
            let urlParams = new URLSearchParams(window.location.search);
            if (urlParams.get('p')) {
                let params = JSON.parse(atob(urlParams.get('p')));
                if (params.usuarioId) {
                    this.usuarioId = params.usuarioId;
                    this.getUsuarioById();
                }
                if (params.Update) {
                    this.update = params.Update;
                }
            }

            //end url params

            //this.inter();
            Promise.all([
                //this.GetAllMandatoryDocument()

            ]).then(values => {

            })
                .catch(error => {
                    console.error(error);
                }).finally(function () {
                    EasyLoading.hide();
                });


        },
        methods: {
            GetListRoles() {
                let vm = this;

                let roles = JSON.parse(sessionStorage.getItem("RolesKetalty"));
                if (roles) {
                    vm.ListRoles = roles;
                } else {
                    //window.location = "/Login/Index";
                }

                this.NamesRoles = this.NamesRoles.map((role) => {
                    let roleFind = roles.find((item) => {
                        return item.Name === role.Name;
                    });
                    return {
                        ...role,
                        Id: roleFind ? roleFind.Id : 0,
                    };
                });

                this.NamesRoles = this.NamesRoles.filter((role) => role.Id !== 0);

            },

            getUsuarioById() {
                let vm = this;
                axios
                    .get("/Usuario/GetUsuarioById", {
                        params: {
                            usuarioId: vm.usuarioId
                        },
                    })
                    .then(async function (response) {
                        debugger;
                        vm.nombre = response.data.Nombre;
                        vm.apellidoPaterno = response.data.ApellidoPaterno;
                        vm.apellidoMaterno = response.data.ApellidoMaterno;
                        vm.numeroDocumento = response.data.NumeroDocumento;
                        vm.correoElectronico = response.data.CorreoElectronico;
                        vm.usuario = response.data.Login;
                        vm.contrasenia = response.data.Contrasenia;
                        vm.codigoEstudiante = response.data.CodigoEstudiante;
                        vm.administrator = response.data.Administrador;
                        vm.opsolictudes = response.data.OperadorSolicitudes;
                        vm.opdonaciones = response.data.OperadorDonaciones;

                        vm.backupUsuario = {
                            "PlacementId": vm.placementId,
                            "SaleDate": response.data.SaleDate
                        };

                    })
                    .catch(function (response) {
                        console.error('Error al obtener la usuario');
                    })
                    .finally(function () {

                    });

            },
            async save() {
                let vm = this;
                debugger;
                vm.$validator.validateAll('formCreateUsuario').then(async function (result) {
                    if (result) {
                        vm.saveUsuario();
                    } else {
                        iziToast.error({
                            title: 'Error',
                            message: 'información incompleta.',
                            position: 'topRight',
                            timeout: 3000
                        });
                    }
                });
            },
            generalValidations() {
                let validation = false;
                let message = '';
                let affiliateWithIncompleteData = this.affiliatesList.find(affiliate => affiliate.RelationshipId == '' || affiliate.BirthDate == '');


                if (affiliateWithIncompleteData) {
                    validation = true;
                    message = 'Existen afiliados con datos incompletos.';
                } else if (this.affiliatesList.length === 0) {
                    validation = true;
                    message = 'No existen afiliados.';
                }



                if (validation) {
                    iziToast.error({
                        title: 'Error',
                        message: message,
                        position: 'topRight',
                        timeout: 3000
                    });
                }

                return validation;
            },
            saveUsuario: async function () {
                let vm = this;
                vm.run_waitMe();

                //Validar usuario
                var formDataVal = new FormData();
                debugger;
                let objVal = {
                    "CodigoUsuario": vm.usuarioId,
                    "numeroDocumento": vm.numeroDocumento,
                    "login": vm.usuario,
                    "codigoEstudiante": vm.codigoEstudiante,
                };

                formDataVal.append("model", JSON.stringify(objVal));
                var validarUsuario = false;
                return axios({
                    method: 'post',
                    url: '/Usuario/Validar_Existencia_Usuario',
                    data: formDataVal,
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data'
                    }
                }).then(function (response) {
                    debugger;
                    if (response.data.Success) {
                        validarUsuario = true;
                    }
                    else {
                        validarUsuario = false;
                        iziToast.error({
                            title: 'Error',
                            message: response.data.Descripcion,
                            position: 'topRight',
                            timeout: 3000
                        });
                    }
                }).catch(function (error) {
                    debugger;
                    iziToast.error({
                        title: 'Error',
                        message: 'al validar usuario',
                        position: 'topRight',
                        timeout: 3000
                    });
                }).finally(function () {
                    if (validarUsuario == false) {
                        EasyLoading.hide();
                    }
                    else {
                        debugger;
                        if (validarUsuario == true) {
                            vm.run_waitMe();
                            var formData = new FormData();
                            debugger;
                            let obj = {
                                "CodigoUsuario": vm.usuarioId,
                                "nombre": vm.nombre,
                                "apellidoPaterno": vm.apellidoPaterno,
                                "apellidoMaterno": vm.apellidoMaterno,
                                "numeroDocumento": vm.numeroDocumento,
                                "login": vm.usuario,
                                "contrasenia": vm.contrasenia,
                                "codigoEstudiante": vm.codigoEstudiante,
                                "correoElectronico": vm.correoElectronico,
                                "Administrador": vm.administrator,
                                "OperadorDonaciones": vm.opdonaciones,
                                "OperadorSolicitudes": vm.opsolictudes,
                            };

                            formData.append("model", JSON.stringify(obj));

                            return axios({
                                method: 'post',
                                url: vm.usuarioId === 0 ? '/Usuario/CreateUsuario' : '/Usuario/CreateUsuario',
                                data: formData,
                                headers: {
                                    'Accept': 'application/json',
                                    'Content-Type': 'multipart/form-data'
                                }
                            }).then(function (response) {
                                Swal.fire({
                                    title: vm.usuarioId === 0 ? `Se creó el usuario N° ${response.data.Id}` : `Se actualizaron los datos del usuario N° ${response.data.Id}`,
                                    text: 'Buen Trabajo!',
                                    icon: 'success',
                                    confirmButtonColor: '#002F87',
                                    cancelButtonColor: '#d33',
                                    confirmButtonText: 'OK',
                                    allowOutsideClick: false,

                                }).then((result) => {
                                    if (result.isConfirmed) {
                                        window.location = "/Usuario/Index";
                                    }
                                });
                                return response.data;
                            }).catch(function (error) {
                                iziToast.error({
                                    title: 'Error',
                                    message: vm.usuarioId === 0 ? 'al crear la usuario' : 'al actualizar la usuario',
                                    position: 'topRight',
                                    timeout: 3000
                                });
                            }).finally(function () {
                                EasyLoading.hide();
                            });
                        }
                    }
                });                               
                
            },
            initialConfig() {
                var currentDate = moment().format("DD-MM-YYYY");
                this.formatSaleDate = JSON.stringify(
                    {
                        "appendTo": "#saleDateflat",
                        "dateFormat": "d/m/Y",
                        "maxDate": currentDate,
                        "wrap": true,
                        "locale": {
                            "firstDayOfWeek": 0,
                            "weekdays": {
                                "shorthand": ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"]
                            },
                            "months": {
                                "longhand": ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"]
                            }
                        }
                    }
                );

                this.formatStartValidity = JSON.stringify(
                    {
                        "appendTo": "#startValidityFlatpickr",
                        "dateFormat": "d/m/Y",
                        "minDate": currentDate,
                        "wrap": true,
                        "locale": {
                            "firstDayOfWeek": 0,
                            "weekdays": {
                                "shorthand": ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"]
                            },
                            "months": {
                                "longhand": ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"]
                            }
                        }
                    }
                );

                this.formatStartOfContract = JSON.stringify(
                    {
                        "appendTo": "#startOfBillingFlat",
                        "dateFormat": "d/m/Y",
                        "minDate": currentDate,
                        "wrap": true,
                        "locale": {
                            "firstDayOfWeek": 0,
                            "weekdays": {
                                "shorthand": ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"]
                            },
                            "months": {
                                "longhand": ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"]
                            }
                        }
                    }
                );

            },
            pasar_colocacion() {
                window.location = "/Usuario/Index";
            },
            changeContractor(val) {
                this.person = val;
            },
            changeStep(item) {
                this.formAttachedDocumentShow = false;
                this.formAffiliateListShow = false;
                this.formPreAffiliationShow = false;
                if (item === 'contratante') {
                    this.contratanteShow = true;
                    this.placementShow = false;
                    this.placementShowHeader = false;
                    this.attachedDocumentShow = false;
                    this.preAffiliationShow = false;
                    this.affiliateListShow = false;
                    this.confirmationShow = false;
                } else if (item === 'placement') {
                    this.contratanteShow = true;
                    this.placementShow = true;
                    this.placementShowHeader = true;
                    this.attachedDocumentShow = false;
                    this.preAffiliationShow = false;
                    this.affiliateListShow = false;
                    this.confirmationShow = false;
                    this.formPreAffiliationShow = false;
                } else if (item === 'attachedDocument') {

                    this.contratanteShow = true;
                    this.placementShow = false;
                    this.placementShowHeader = true;
                    this.attachedDocumentShow = true;
                    this.preAffiliationShow = false;
                    this.affiliateListShow = false;
                    this.confirmationShow = false;

                    this.formAttachedDocumentShow = true;
                } else if (item === 'preAffiliation') {
                    this.contratanteShow = true;
                    this.placementShow = true;
                    this.placementShowHeader = true;
                    this.attachedDocumentShow = true;
                    this.preAffiliationShow = true;
                    this.affiliateListShow = false;
                    this.confirmationShow = false;

                    this.formPreAffiliationShow = true;
                } else if (item === 'affiliateList') {
                    this.contratanteShow = true;
                    this.placementShow = true;
                    this.placementShowHeader = true;
                    this.attachedDocumentShow = true;
                    this.preAffiliationShow = true;
                    this.affiliateListShow = true;
                    this.confirmationShow = false;

                    this.formAffiliateListShow = true;
                } else if (item === 'confirmation') {
                    this.previewCalculateAmountToPay();
                    this.contratanteShow = true;
                    this.placementShow = true;
                    this.placementShowHeader = true;
                    this.attachedDocumentShow = true;
                    this.preAffiliationShow = true;
                    this.affiliateListShow = true;
                    this.confirmationShow = true;
                }
            },
            run_waitMe: function () {

                EasyLoading.show({
                    type: EasyLoading.TYPE.BALL_SCALE_MULTIPLE,
                    text: "Espere por favor, procesando datos..."
                });

            },

        },
    });
})();