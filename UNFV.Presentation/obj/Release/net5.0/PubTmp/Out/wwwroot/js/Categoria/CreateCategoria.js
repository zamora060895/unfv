﻿(function () {
    'use strict';


    Vue.use(VeeValidate, {
        locale: 'es',
        dictionary: {
            es: {

                custom:
                {
                    descripcion: {
                        required: 'Ingresar dirección',
                    },
                }
            }
        }
    });

    Vue.use(bootstrapVue);

    Vue.directive("select", {
        twoWay: true,
        bind: function (el, binding, vnode) {
            $(el)
                .select2()
                .on("select2:select", (e) => {
                    el.dispatchEvent(new Event("change", { target: e.target }));
                });
        },
    });

    var app = new Vue({
        el: "#CreateCompleteCategory",
        data: {
            categoryId: 0,
            backupCategory: {},
            update: false,
            prueba: "custom-select",
            //Create Client (Contractor)           
            descripcion: '',
            NamesRoles: [
                { Id: 0, Name: "Administrador" },
                { Id: 0, Name: "Vendedor EPS" },
                { Id: 0, Name: "Operador Mesa de Control" },
                { Id: 0, Name: "Asistente comercial" },
                { Id: 0, Name: "Supervisor de solicitudes" },
                { Id: 0, Name: "Agente Comercial" },
            ]
        },
        computed: {
            filteredFiles() {
                return this.fileList.filter(x => x.Name.toLowerCase().includes(this.searchFile.toLowerCase()));
            },

        },
        watch: {
        },
        created: function () {
            this.run_waitMe();
            //this.GetListRoles();
            this.initialConfig();
        },
        mounted: function () {
            this.run_waitMe();
            //url params  
            var currentDate = moment().format("DD/MM/YYYY");
            this.saleDate = currentDate;
            let urlParams = new URLSearchParams(window.location.search);
            if (urlParams.get('p')) {
                let params = JSON.parse(atob(urlParams.get('p')));
                if (params.categoryId) {
                    this.categoryId = params.categoryId;
                    this.getCategoryById();
                }
                if (params.Update) {
                    this.update = params.Update;
                }
            }

            //end url params

            //this.inter();
            Promise.all([
                //this.GetAllMandatoryDocument()

            ]).then(values => {

            })
                .catch(error => {
                    console.error(error);
                }).finally(function () {
                    EasyLoading.hide();
                });


        },
        methods: {
            GetListRoles() {
                let vm = this;

                let roles = JSON.parse(sessionStorage.getItem("RolesKetalty"));
                if (roles) {
                    vm.ListRoles = roles;
                } else {
                    //window.location = "/Login/Index";
                }

                this.NamesRoles = this.NamesRoles.map((role) => {
                    let roleFind = roles.find((item) => {
                        return item.Name === role.Name;
                    });
                    return {
                        ...role,
                        Id: roleFind ? roleFind.Id : 0,
                    };
                });

                this.NamesRoles = this.NamesRoles.filter((role) => role.Id !== 0);

            },

            getCategoryById() {
                let vm = this;
                debugger;
                axios
                    .get("/Categoria/GetCategoryById", {
                        params: {
                            categoryId: vm.categoryId
                        },
                    })
                    .then(async function (response) {
                        vm.descripcion = response.data.Descripcion;                      

                        vm.backupCategory = {
                            "PlacementId": vm.placementId,
                            "SaleDate": response.data.SaleDate                           
                        };

                    })
                    .catch(function (response) {
                        console.error('Error al obtener la categoría');
                    })
                    .finally(function () {

                    });

            },
            async save() {
                let vm = this;
                debugger;
                vm.$validator.validateAll('formCreateCategory').then(async function (result) {
                    if (result) {
                        vm.saveCategory();                      
                    } else {
                        iziToast.error({
                            title: 'Error',
                            message: 'información incompleta.',
                            position: 'topRight',
                            timeout: 3000
                        });
                    }
                });
            },
            generalValidations() {
                let validation = false;
                let message = '';
                let affiliateWithIncompleteData = this.affiliatesList.find(affiliate => affiliate.RelationshipId == '' || affiliate.BirthDate == '');


                if (affiliateWithIncompleteData) {
                    validation = true;
                    message = 'Existen afiliados con datos incompletos.';
                } else if (this.affiliatesList.length === 0) {
                    validation = true;
                    message = 'No existen afiliados.';
                }



                if (validation) {
                    iziToast.error({
                        title: 'Error',
                        message: message,
                        position: 'topRight',
                        timeout: 3000
                    });
                }

                return validation;
            },
            saveCategory: async function () {
                let vm = this;
                vm.run_waitMe();
                var formData = new FormData();
                debugger;
                let obj = {
                    "CodigoCategoria": vm.categoryId,
                    "descripcion": vm.descripcion,
                };

                formData.append("model", JSON.stringify(obj));

                return axios({
                    method: 'post',
                    url: vm.categoryId === 0 ? '/Categoria/CreateCategory' : '/Categoria/CreateCategory',
                    data: formData,
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data'
                    }
                }).then(function (response) {
                    Swal.fire({
                        title: vm.categoryId === 0 ? `Se creó la categoría N° ${response.data.Id}` : `Se actualizaron los datos de la categoría N° ${response.data.Id}`,
                        text: 'Buen Trabajo!',
                        icon: 'success',
                        confirmButtonColor: '#002F87',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'OK',
                        allowOutsideClick: false,

                    }).then((result) => {
                        if (result.isConfirmed) {
                            window.location = "/Categoria/Index";
                        }
                    });


                    return response.data;
                }).catch(function (error) {
                    iziToast.error({
                        title: 'Error',
                        message: vm.categoryId === 0 ? 'al crear la categoría' : 'al actualizar la categoría',
                        position: 'topRight',
                        timeout: 3000
                    });
                }).finally(function () {
                    EasyLoading.hide();
                });
            },
            initialConfig() {
                var currentDate = moment().format("DD-MM-YYYY");
                this.formatSaleDate = JSON.stringify(
                    {
                        "appendTo": "#saleDateflat",
                        "dateFormat": "d/m/Y",
                        "maxDate": currentDate,
                        "wrap": true,
                        "locale": {
                            "firstDayOfWeek": 0,
                            "weekdays": {
                                "shorthand": ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"]
                            },
                            "months": {
                                "longhand": ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"]
                            }
                        }
                    }
                );

                this.formatStartValidity = JSON.stringify(
                    {
                        "appendTo": "#startValidityFlatpickr",
                        "dateFormat": "d/m/Y",
                        "minDate": currentDate,
                        "wrap": true,
                        "locale": {
                            "firstDayOfWeek": 0,
                            "weekdays": {
                                "shorthand": ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"]
                            },
                            "months": {
                                "longhand": ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"]
                            }
                        }
                    }
                );

                this.formatStartOfContract = JSON.stringify(
                    {
                        "appendTo": "#startOfBillingFlat",
                        "dateFormat": "d/m/Y",
                        "minDate": currentDate,
                        "wrap": true,
                        "locale": {
                            "firstDayOfWeek": 0,
                            "weekdays": {
                                "shorthand": ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"]
                            },
                            "months": {
                                "longhand": ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"]
                            }
                        }
                    }
                );

            },
            pasar_colocacion() {
                window.location = "/Categoria/Index";
            },
            changeContractor(val) {
                this.person = val;
            },
            changeStep(item) {
                this.formAttachedDocumentShow = false;
                this.formAffiliateListShow = false;
                this.formPreAffiliationShow = false;
                if (item === 'contratante') {
                    this.contratanteShow = true;
                    this.placementShow = false;
                    this.placementShowHeader = false;
                    this.attachedDocumentShow = false;
                    this.preAffiliationShow = false;
                    this.affiliateListShow = false;
                    this.confirmationShow = false;
                } else if (item === 'placement') {
                    this.contratanteShow = true;
                    this.placementShow = true;
                    this.placementShowHeader = true;
                    this.attachedDocumentShow = false;
                    this.preAffiliationShow = false;
                    this.affiliateListShow = false;
                    this.confirmationShow = false;
                    this.formPreAffiliationShow = false;
                } else if (item === 'attachedDocument') {

                    this.contratanteShow = true;
                    this.placementShow = false;
                    this.placementShowHeader = true;
                    this.attachedDocumentShow = true;
                    this.preAffiliationShow = false;
                    this.affiliateListShow = false;
                    this.confirmationShow = false;

                    this.formAttachedDocumentShow = true;
                } else if (item === 'preAffiliation') {
                    this.contratanteShow = true;
                    this.placementShow = true;
                    this.placementShowHeader = true;
                    this.attachedDocumentShow = true;
                    this.preAffiliationShow = true;
                    this.affiliateListShow = false;
                    this.confirmationShow = false;

                    this.formPreAffiliationShow = true;
                } else if (item === 'affiliateList') {
                    this.contratanteShow = true;
                    this.placementShow = true;
                    this.placementShowHeader = true;
                    this.attachedDocumentShow = true;
                    this.preAffiliationShow = true;
                    this.affiliateListShow = true;
                    this.confirmationShow = false;

                    this.formAffiliateListShow = true;
                } else if (item === 'confirmation') {
                    this.previewCalculateAmountToPay();
                    this.contratanteShow = true;
                    this.placementShow = true;
                    this.placementShowHeader = true;
                    this.attachedDocumentShow = true;
                    this.preAffiliationShow = true;
                    this.affiliateListShow = true;
                    this.confirmationShow = true;
                }
            },
            run_waitMe: function () {

                EasyLoading.show({
                    type: EasyLoading.TYPE.BALL_SCALE_MULTIPLE,
                    text: "Espere por favor, procesando datos..."
                });

            },

        },
    });
})();