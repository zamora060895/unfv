﻿(function () {
    'use strict';

    Vue.use(bootstrapVue);

    Vue.directive("select", {
        twoWay: true,
        bind: function (el, binding, vnode) {
            $(el)
                .select2()
                .on("select2:select", (e) => {
                    el.dispatchEvent(new Event("change", { target: e.target }));
                });
        },
    });

    var app = new Vue({
        el: "#Donacion",
        data: {
            codigoestado: '',
            /*estadoList: [{ Id: 1, Name: 'Pendiente' }, { Id: 2, Name: 'En proceso' }, { Id: 3, Name: 'Aprobado' }, { Id: 4, Name: 'Entregado' }, { Id: 5, Name: 'Rechazado' }],*/
            estadoList: [],
            columnsTable: [
                { key: "CodigoDonacion", label: "N° Donación", sortable: true },
                { key: "FechaDonacion", label: "Fecha donación", sortable: true },
               /* { key: "Usuario", label: "Usuario creación" },*/
                { key: "CodigoEstudiante", label: "Código Estudiante" },
                { key: "Estudiante", label: "Estudiante" },
                { key: "MontoTotal", label: "Monto total" },
                //{ key: "Estado", label: "Estado" },
                { key: "statusDonation", label: "Estado", 'class': 'my-class', },
                { key: 'statesOptions', label: '', thClass: 'text-center', tdClass: 'text-center' },
                {
                    key: "opciones",
                    label: "Opciones",
                    thClass: "text-center",
                    tdClass: "text-center",
                },

            ],

            donacionList: [],

            donacionTrackingList: [],
            columnsTableTracking: [
                { key: "fechacreacion", label: "Fecha registro", sortable: true },
                { key: "usuario", label: "Usuario" },
                { key: "estado", label: "Estado" },
                { key: "comentario", label: "Comentario" },
            ],

            preAffiliationIdFile: 0,
            donacionIdFile: 0,
            documentTypePreaffiliation: '2',
            documentTypePreaffiliationList: [],
            documentTypePreaffiliationListBkp: [],
            fileList: [],
            mandatoryDocumentList: [],

            //fin mis variables
            SaleData: {},
            searching: false,
            withoutResults: false,
            salesList: [],
            totalRows: 0,
            currentPage: 1,
            perPage: 10,


            SelectStatus: [
                { Id: 1, Name: "Pendiente" },
                { Id: 2, Name: "Verificado" },
                { Id: 3, Name: "Verificado por mesa de control" },
            ],

            creationDateFrom: '',
            creationDateTo: '',
            saleId: '',
            description: '',
            clientName: '',
            identityDocument: '',
            seller: '',
            statusId: [],

            ListRoles: [],
            statusList: [],

            //add file
            searchFile: '',
            saleIdForFile: '',
            valueFile: '',
            fileName: '',
            fileNote: '',
            fileNumBoleta: '',
            flagNumBoleta: false,
            //Roles
            NamesRoles: [
                { Id: 0, Name: "Administrador" },
                { Id: 0, Name: "Vendedor EPS" },
                { Id: 0, Name: "Operador Mesa de Control" },
                { Id: 0, Name: "Asistente comercial" },
                { Id: 0, Name: "Supervisor de solicitudes" },
                { Id: 0, Name: "Agente Comercial" },
            ],
            SecondClient: "",
            SecondClientCode: "",
            SecondBroker: "",
            SecondBrokerCode: "",
            SecondManager: "",
            SecondManagerCode: "",
            donacionIdTracking: '',

            currentPageTracking: 1,
            perPageTracking: 10,
            searchingTracking: false,
            withoutResultsTracking: false,
            totalRowsTracking: 0,
            checkboxSigned: false,
            codigodona: '',
            codigoestudiante: '',
        },
        watch: {
            //valueFile: function (file) {
            //    if (document.getElementById("loadFile").files[0])
            //        this.fileName = document.getElementById("loadFile").files[0].name;
            //},
            valueFile: function (file) {
                //if (document.getElementById("loadFile").files[0])
                //    this.fileName = document.getElementById("loadFile").files[0].name;

                if (document.getElementById("loadFile").files[0]) {
                    var fileNameP = document.getElementById("loadFile").value;
                    var idxDot = fileNameP.lastIndexOf(".") + 1;
                    var extFile = fileNameP.substr(idxDot, fileNameP.length).toLowerCase();
                    if (extFile == "jpg" || extFile == "jpeg" || extFile == "png" || extFile == "pdf") {
                        //TO DO
                        this.fileName = document.getElementById("loadFile").files[0].name;
                    } else {
                        this.fileName = '';
                        document.getElementById("loadFile").value = "";
                        this.valueFile = '';

                        iziToast.warning({
                            title: 'Información',
                            message: 'Se acepta solo archivos de tipo pdf, jpg, jpeg y png.',
                            position: 'topRight',
                            timeout: 3000
                        });
                        EasyLoading.hide();
                        return;
                    }
                }
            },
            documentTypePreaffiliation: function (val) {
                if (val == '1') {
                    this.flagNumBoleta = true;
                }
                else {
                    this.flagNumBoleta = false;
                }

            },
        },
        computed: {
            filteredFiles() {
                return this.fileList.filter(x => x.Name.toLowerCase().includes(this.searchFile.toLowerCase()));
            }
        },
        created: function () {
            this.GetAllEstado();
            this.GetAllDonacionPaginate(1);
        },
        mounted: function () {

        },

        methods: {

            saveFilesAdd: async function () {
                let vm = this;
                vm.run_waitMe();
                var formData = new FormData();

                formData.append("donacionId", JSON.stringify(vm.donacionIdFile));

                let newList = await Promise.all(vm.fileList.map(async item => {
                    let fileBase64 = "";
                    if (item.FileBase64) {
                        await this.getBase64(item.FileBase64)
                            .then(function (data) {
                                fileBase64 = data.split(';')[1].split(',')[1]
                            });
                    }

                    return {
                        ...item,
                        file: fileBase64,
                        fileExtension: item.Name.split('.').pop(),
                        name: item.Name,
                        mandatory: "0",
                        mandatoryDocumentTypeId: item.MandatoryDocumentId
                    }
                }));
                              
                formData.append("detailFiles", JSON.stringify(newList))

                return axios({
                    method: 'post',
                    url: '/Donation/CreateFilesAddDonacion',
                    data: formData,
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data'
                    }
                }).then(async function (response) {
                    Swal.fire({
                        title: 'Buen Trabajo!',
                        text: 'Archivos guardados',
                        icon: 'success',
                        confirmButtonColor: '#002F87',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'OK',
                    }).then((result) => {
                        if (result.isConfirmed) {
                            vm.fileList = [];
                            $('#fileModal').modal('hide');
                            window.location = "/Donation/Index";
                            //this.GetAllDonacionPaginate(1);
                        }
                    });
                    return response.data;
                }).catch(function (error) {
                    iziToast.error({
                        title: 'Error',
                        message: 'al guardar archivos de la solicitud',
                        position: 'topRight',
                        timeout: 3000
                    });
                }).finally(function () {
                   
                    EasyLoading.hide();
                   
                });
               
            },
            getBase64: async function (file) {

                return new Promise((resolve, reject) => {
                    const reader = new FileReader();
                    reader.readAsDataURL(file);
                    reader.onload = () => resolve(reader.result);
                    reader.onerror = error => reject(error);
                });
            },
            async addFile() {
                
                let _return = false;
                var totalFiles = document.getElementById("loadFile").files.length;
                
                if (this.documentTypePreaffiliation == '1') {
                    if (this.fileNumBoleta =='') {
                        iziToast.error({
                            title: 'Error',
                            message: 'Ingrese el número de la boleta.',
                            position: 'topRight',
                            timeout: 3000
                        });
                        _return = true;
                        return;
                    }
                }
               
                
                if (_return) {
                    return;
                }

                if (totalFiles) {
                    let element = document.getElementById("loadFile").files[0];
                    let file = {
                        "Id": uuid.v4(),
                        "Name": this.fileName,
                        "OriginId": 0,
                        "SourceId": 0,
                        "Description": this.fileNote,
                        "Mandatory": false,
                        "MandatoryDocumentId": this.documentTypePreaffiliation,
                        "FileBase64": element,
                        "NumBoleta": this.fileNumBoleta,
                    }
                    this.fileList.push(file);
                }
                else {
                    iziToast.error({
                        title: 'Error',
                        message: 'No hay ningún archivo seleccionado.',
                        position: 'topRight',
                        timeout: 3000
                    });
                }
                document.getElementById("loadFile").value = "";
                this.valueFile = "";
                this.fileName = "";
                this.fileNote = "";
                this.fileNumBoleta = "";
                this.flagNumBoleta = false;
                this.documentTypePreaffiliation = '2';
                await this.$nextTick();
                var element = document.getElementById("documentTypePreaffiliation");
                $.HSCore.components.HSSelect2.init($(element));
                //$('#fileModal').modal('hide');

            },
            async closeFileAdd() {
                this.fileList = [];
                this.documentTypePreaffiliationListBkp = [];
                document.getElementById("loadFile").value = "";
                this.valueFile = "";
                this.fileName = "";
                this.fileNote = "";
                this.fileNumBoleta = "";
                this.flagNumBoleta = false;
                this.documentTypePreaffiliation = '2';
                await this.$nextTick();
                var element = document.getElementById("documentTypePreaffiliation");
                $.HSCore.components.HSSelect2.init($(element));
                $('#fileModal').modal('hide');
            },

            GetListRoles() {
                let vm = this;
                
                let roles = JSON.parse(sessionStorage.getItem("RolesKetalty"));
                if (roles) {
                    vm.ListRoles = roles;
                } else {
                    window.location = "/Login/Index";
                }

                this.NamesRoles = this.NamesRoles.map((role) => {
                    let roleFind = roles.find((item) => {
                        return item.Name === role.Name;
                    });
                    return {
                        ...role,
                        Id: roleFind ? roleFind.Id : 0,
                    };
                });

                this.NamesRoles = this.NamesRoles.filter((role) => role.Id !== 0);
            },
            getDonacionTracking(item) {
                let vm = this;
                vm.donacionIdTracking = item.CodigoDonacion;
                $("#trackingModal").modal()
                vm.BtnBuscarTracking();
            },
            BtnBuscarTracking() {
                let vm = this;
                vm.searchingTracking = true;
                vm.donacionTrackingList = [];
                vm.withoutResultsTracking = false;

                setTimeout(function () {
                    return axios
                        .get("/Donation/GetAllDonacionTrackingPaginate", {
                            params: {
                                codigodonacion: vm.donacionIdTracking,
                                Page: vm.currentPageTracking,
                                Size: vm.perPageTracking,
                            },
                        })
                        .then(function (response) {
                            vm.donacionTrackingList = response.data.Items;
                            return response.data;
                        })
                        .catch(function (response) {
                            vm.withoutResultsTracking = true;
                            console.error('Error al buscar el historial');
                        })
                        .finally(function () {
                            vm.searchingTracking = false;
                        });
                }, 0);
            },
            closeTracking() {
                let vm = this;
                vm.donacionIdTracking = '';
                $('#trackingModal').modal('hide');
            },
            GetAllDonacionPaginate(flagStart) {
                let vm = this;

                if ($('#creationDate').val() != "") {
                    vm.creationDateFrom = $.trim($('#creationDate').val().split('-')[0]);
                    vm.creationDateTo = $.trim($('#creationDate').val().split('-')[1]);
                }
                else {
                    var dateNow = new Date();
                    vm.creationDateFrom = new Date(dateNow.getFullYear(), dateNow.getMonth() - 2, 1);
                    vm.creationDateTo = new Date(dateNow.getFullYear(), dateNow.getMonth() + 1, 0);
                }

                if (flagStart ==1) {
                    vm.codigoestado = 6;
                }
                vm.searching = true;
                vm.donacionList = [];
                vm.withoutResults = false;

                setTimeout(function () {
                    var formData = new FormData();

                    let obj = {
                        "creationDateFrom": vm.creationDateFrom,
                        "creationDateTo": vm.creationDateTo,
                        "codigodonacion": vm.codigodona,
                        "codigoestudiante": vm.codigoestudiante,
                        "codigoestado": vm.codigoestado,
                        "page": vm.currentPage,
                        "size": vm.perPage,
                    };
                    formData.append("DonacionPaginateModel", JSON.stringify(obj));

                    return axios({
                        method: 'post',
                        url: '/Donation/GetAllDonacionPaginate',
                        data: formData,
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'multipart/form-data'
                        }
                    }).then(function (response) {
                        
                        vm.donacionList = response.data.Items;
                        vm.totalRows = response.data.Total;
                        if (vm.totalRows === 0) {
                            vm.withoutResults = true;
                        }

                        /*vm.statusId = [];*/
                        return response.data;
                    })
                        .catch(function (response) {
                            vm.withoutResults = true;

                            return alert('Error al buscar donación');
                        })
                        .finally(function () {
                            vm.searching = false;
                        });
                }, 0);
            },
            newDonacion() {
                window.location = "/Donation/DonacionDetail";
            },

            addFileDonacion(item) {
                let vm = this;
                vm.donacionIdFile = item.CodigoDonacion;
                vm.signed = item.Signed;

                $("#fileModal").modal();
                //vm.GetAllMandatoryDocumentPreaffiliation();
                vm.GetDocumentByDonationId();
            },
            donacionDetail({ CodigoDonacion, Status }) {
                let url = "/Donation/DonacionDetail";
                let update = false;
                
                let params = { donacionId: CodigoDonacion, Update: update, };
                window.location = `${url}?p=${btoa(JSON.stringify(params))}`;
            },
            run_waitMe: function () {

                EasyLoading.show({
                    type: EasyLoading.TYPE.BALL_SCALE_MULTIPLE,
                    text: "Espere por favor, procesando datos..."
                });

            },
            ChangeStatusCategory(item, tipo) {
                $.blockUI({ message: '<h1>Procesando...</h1>' });
                let vm = this;

                var data = {};
                if (tipo === 'enviado') {
                    data = {
                        Id: item.Id,
                        PlacementStatusId: 2,
                        Commentary: 'Cambiado a enviado',
                        NotificationText: 'Desea enviar venta al asistente comercial',
                        PlacementSubProductId: item.SubProductTypeId,
                    };
                } else if (tipo === 'verificado') {
                    data = {
                        Id: item.Id,
                        PlacementStatusId: 3,
                        Commentary: 'Enviado a mesa de control',
                        NotificationText: 'Desea enviar venta a mesa de control',
                        PlacementSubProductId: item.SubProductTypeId,
                    };
                } else if (tipo === 'rechazado') {
                    data = {
                        Id: item.Id,
                        PlacementStatusId: 5,
                        Commentary: '',
                        NotificationText: 'Desea rechazar la venta',
                        PlacementSubProductId: item.SubProductTypeId,
                    };
                } else if (tipo === 'devueltoCom') {
                    data = {
                        Id: item.Id,
                        PlacementStatusId: 7,
                        Commentary: '',
                        NotificationText: 'Desea devolver la venta al asesor',
                        PlacementSubProductId: item.SubProductTypeId,
                    };
                } else if (tipo === 'devuelto') {
                    data = {
                        Id: item.Id,
                        PlacementStatusId: 6,
                        Commentary: '',
                        NotificationText: 'Desea devolver la venta al asistente comercial',
                        PlacementSubProductId: item.SubProductTypeId,
                    };
                } else {
                    //mesa de control
                    data = {
                        Id: item.Id,
                        PlacementStatusId: 4,
                        Commentary: 'Cambiado a verificado por Mesa de Control',
                        NotificationText: 'Desea cambiar el estado a verificado por Mesa de Control',
                        PlacementSubProductId: item.SubProductTypeId,
                    };
                }


                vm.SaleData = data;

                $("#SendPlacementControlTablePopUp").modal('show');

                $.unblockUI();
            },
            GetAllEstado() {
                let vm = this;
                return axios
                    .get("/Donation/GetAllEstadoDonacion", {

                    })
                    .then(function (response) {
                        vm.estadoList = response.data;
                        vm.GetAllDonacionPaginate(1);

                        return response.data;
                    })
                    .catch(function (response) {

                        console.error(`Error al buscar estados`);
                    })
                    .finally(function () {

                    });

            },
            getPdfDonacionById({ CodigoDonacion, Estado }) {
                
                var tipo = "0";
                if (Estado=="Pendiente") {
                    tipo = "1";
                }
                if (CodigoDonacion) {
                    window.location = `/Donation/GetPdfDonacionById?codigodonacion=${CodigoDonacion}&tipo=${tipo}`;
                }
            },
            ChangeStatusDonacion(item, tipo) {
                $.blockUI({ message: '<h1>Procesando...</h1>' });
                let vm = this;
                vm.run_waitMe();
                var formData = new FormData();
                var estadodona = 6;
                
                if (tipo == 'AP') {
                    estadodona = 7;
                }
                else if (tipo == 'RZ') {
                    estadodona = 8;
                }

                let obj = {
                    "CodigoDonacion": item.CodigoDonacion,
                    "CodigoEstado": estadodona,
                };

                formData.append("model", JSON.stringify(obj));

                return axios({
                    method: 'post',
                    url: '/Donation/ChangeStatusDonacion',
                    data: formData,
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data'
                    }
                }).then(function (response) {
                    
                    Swal.fire({
                        title: estadodona == 7 ? `Se aprobó la donación N° ${response.data.Id}` : `Se rechazó la donación N° ${response.data.Id}`,
                        text: 'Buen Trabajo!',
                        icon: 'success',
                        confirmButtonColor: '#002F87',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'OK',
                        allowOutsideClick: false,

                    }).then((result) => {
                        if (result.isConfirmed) {
                            window.location = "/Donation/Index";
                        }
                    });


                    return response.data;
                }).catch(function (error) {
                    iziToast.error({
                        title: 'Error',
                        message: 'al cambiar de estado la donación',
                        position: 'topRight',
                        timeout: 3000
                    });
                }).finally(function () {
                    EasyLoading.hide();
                });

                $.unblockUI();
            },
            async GetDocumentByDonationId() {
                let vm = this;
                vm.run_waitMe();
                
                await axios
                    .get("/Donation/GetArchivosDonacion", {
                        params: {
                            donacionId: vm.donacionIdFile
                        },
                    })
                    .then(async function (response) {
                        let count = 0;
                        response.data.forEach(documentElement => {
                            count++;
                            
                            let mandatoryDocument = {
                                documentList: Array.isArray(response.data) ? response.data.map(item => {
                                    return {
                                        "id": item.CodigoFile,
                                        "mandatoryDocumentId": 0,
                                        "name": item.NombreArchivo,
                                        "numBoleta": item.NumeroVoucher,
                                        "relationShipId": 0,
                                        "typeOriginId": 0,
                                        "file": null,
                                        "sourceId": 0,
                                        "mandatory": false,
                                        "MandatoryDocumentId": item.TipoArchivo,
                                        "Description": item.Nota == null ? '' : item.Nota,
                                        "rutaArchivo": item.RutaArchivo,
                                    }
                                }) : []
                            }
                            if (count == 1) {
                                vm.mandatoryDocumentList = mandatoryDocument.documentList;
                            }
                        });
                        
                        return response.data;
                    })
                    .catch(function (response) {
                        console.error('Error al obtener los documentos');
                    })
                    .finally(function () {
                        EasyLoading.hide();
                    });
            },
            DownloadFile(name, rutaArchivo) {
                
                if (rutaArchivo) {
                    /*window.location = `/Donation/DownloadFile?RutaArchivo=${rutaArchivo}`;*/
                    window.location = `/Donation/DownloadFile?RutaArchivo=${rutaArchivo}&FileName=${name}`;
                }
            },
        },

    });
})();