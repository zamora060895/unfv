﻿(function () {
    'use strict';

    Vue.use(VeeValidate, {
        locale: 'es',
        dictionary: {
            es: {
                custom:
                {
                    User: {
                        required: 'Ingresar usuario'
                    },
                    Password: {
                        required: 'Ingresar contraseña',
                        min: 'Mínimo 8 caracteres'
                    }
                }
            }
        }
    });


    var app = new Vue({
        el: '#logInForm',
        data: {
            User: "",
            Password: "",
            LoadingLogin: false,
            showError: false,
        },
        watch: {
            User: function () {
                this.showError = false;
            },
            Password: function () {
                this.showError = false;
            }
        },
        mounted: function () {

        },
        methods: {
             logIn() {
                let vm = this;
                vm.$validator.validateAll('formLogin').then(function (result) {
                    if (result) {
                        vm.LoadingLogin = true;
                        return axios.get('/Login/GetByTokenUser', {
                            params: {
                                User: vm.User,
                                Password: vm.Password
                            }
                        }).then(function (response) {
                            //window.location = "/Placement/Index";
                            if (response.data) {
                                sessionStorage.removeItem("RolesKetalty");
                                sessionStorage.setItem('RolesKetalty', JSON.stringify(response.data.roles));
                                vm.MenuByTokenUserSessionStorage(response.data.user.CodigoUsuario);
                            } else {
                                vm.showError = true;
                            }
                            return null;
                        }).catch(function ({ response }) {                    
                            vm.showError = true;
                            return alert(`logIn Error: ${response.data.errors}`);
                        }).finally(function () {
                            vm.LoadingLogin = false;
                        });
                    }
                    else
                    {
                        vm.showError = true;
                    }
                });
                
            },
            MenuByTokenUserSessionStorage(UserId) { 
                sessionStorage.removeItem("MenuByTokenUser");
                
                return axios.get('/Login/GetMenuByTokenUser', {
                    params: {
                        UserId: UserId,
                    }
                }).then(function (response) { 
                    sessionStorage.setItem('MenuByTokenUser', JSON.stringify(response.data));
                    window.location = "/Solicitud/Index";
                    return null;
                }).catch(function ({ response }) {
                    return alert(`MenuByTokenUserSessionStorage Error: ${response.data.errors}`);
                });
            }
        }
    })

})();