﻿using System.Security.Claims;

namespace UNFV.Presentation.Helpers.ExtensionClaims
{
    public static class ExtensionClaims
    {
        //public static string GetAccessToken(this ClaimsPrincipal principal)
        //{
        //    return principal.FindFirstValue("Access_Token");
        //}
        //public static string GetRefreshToken(this ClaimsPrincipal principal)
        //{
        //    return principal.FindFirstValue("refresh_token");
        //}
        //public static string GetExpiresIn(this ClaimsPrincipal principal)
        //{
        //    return principal.FindFirstValue("Expires_in");
        //}
        public static string GetUserId(this ClaimsPrincipal principal)
        {
            return principal.FindFirstValue("User_Id");
        }
        public static string GetUserName(this ClaimsPrincipal principal)
        {
            return principal.FindFirstValue("User_Name");
        }
        public static string GetUserSurName(this ClaimsPrincipal principal)
        {
            return principal.FindFirstValue("User_SurName");
        }
        //public static string GetUserSenconSurname(this ClaimsPrincipal principal)
        //{
        //    return principal.FindFirstValue("User_SecondSurname");
        //}
        public static string GroupSignal(this ClaimsPrincipal principal)
        {
            return principal.FindFirstValue("GroupSignal");
        }
    }
}
