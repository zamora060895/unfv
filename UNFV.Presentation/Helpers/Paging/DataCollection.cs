﻿using System.Collections.Generic;
using System.Linq;

namespace Keralty.Presentation.PreAffiliation.Helpers.Paging
{
    public class List<T>
    {
        public bool HasItems
        {
            get
            {
                return Items != null && Items.Any();
            }
        }

        public IEnumerable<T> Items { get; set; }
        public int Total { get; set; }
        public int Page { get; set; }
        public int Pages { get; set; }
    }
}
