﻿(function () {
    'use strict';

    var app = new Vue({
        el: '#Layout',
        data: {
            MenuUser: []
        },
        watch: {

        },
        mounted: function () {
            this.MenuByTokenUser();
        },
        methods: {
            
            LogOut() {
                return axios.get('/Login/LogOut', {
                    params: {
                    }
                }).then(function (response) {
                    window.location = "/Login/Index";
                    return null;
                }).catch(function (error) {
                    return alert(`LogOut Error: ${error}`);
                });
            },
            MenuByTokenUser() {
              
                let MenuByToken = JSON.parse(sessionStorage.getItem('MenuByTokenUser'));               
                if (MenuByToken) {
                    this.MenuUser = MenuByToken;
                } 
                else {
                   
                    window.location = "/Login/Index";
                }
            }

        }
    })

})();