﻿
function getNotification() {
    iziToast.success({
        title: 'Informaci&oacuten',
        message: 'Se ingreso solicitud',
        position: 'topRight',
        timeout: 3000
    });
};


var connection = new signalR.HubConnectionBuilder().withUrl("/signalserver").configureLogging(signalR.LogLevel.Information).build();
connection.start().then(function () {
    console.log('Corriendo signalr');
    connection.invoke("AddGroupNotification").catch(function (err) {
        return console.log(err.toString());
    });
}).catch(function (err) {
    return console.error(err.toString());
})


connection.on('displayNotification', () => {
    getNotification();
});

