﻿Vue.component("advanced-search-student", {
    props: {
        Nombre: [String],
        Title: [String],
    },
    data: function () {
        return {
            searchValue: null,
            touchtime: 0,
            touchitem: 0,
            searching: false,
            totalRows: 0,
            withoutResults: false,
            currentPage: 1,
            perPage: 10,
            datastudent: [],

            columnsTable: [
                {
                    key: "StudentName",
                    label: "Estudiante",
                    sortable: true,
                },
                {
                    key: "NumeroDocumento",
                    label: "Documento",
                    sortable: true,
                },
                {
                    key: "CodigoEstudiante",
                    label: "Codigo Estudiante",
                    sortable: true,
                },
            ],
        };
    },
    template: `
    <div v-bind:id="Nombre" 
        class="modal fade" 
        tabindex="-1" 
        role="dialog" 
        v-bind:aria-labelledby="Nombre"
        aria-hidden="true">

        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header p-4">
              <h5 class="modal-title">Buscar {{Title}}</h5>
              <button
                type="button"
                class="btn btn-xs btn-icon btn-ghost-secondary"
                data-dismiss="modal"
                aria-label="Close">
                <i class="tio-clear tio-lg"></i>
              </button>
            </div>
            <div class="modal-body px-4 pt-0 pb-4">
              <div class="row">
                <div class="col-sm-12">
                  <template>
                    <b-row class="row form-group mb-2">
                      <div class="col-sm-12">
                        <div class="input-group">
                          <input
                            v-model="searchValue"
                            type="text"
                            class="form-control"
                            placeholder="Nombre o documento">
                          <div class="input-group-append">
                            <a class="btn btn-icon btn-soft-primary" v-on:click="Buscar">
                              <i class="tio-search"></i>
                            </a>
                          </div>
                        </div>
                      </div>
                    </b-row>
                    <b-table
                                empty-text="No hay registros"
                                show-empty
                                head-variant="light"
                                hover
                                outlined
                                responsive
                                :fields="columnsTable"
                                :items="datastudent"
                                :current-page="1"
                                :per-page="perPage"
                                @row-clicked="SelectClient">
                    </b-table>
                    <b-row v-if="searching" class="vh-50 text-center" align-h="center">
                        <div class="text-center text-secondary my-2">
                            <b-spinner class="align-middle"></b-spinner>
                            <strong>Cargando...</strong>
                        </div>
                    </b-row>
                    <b-row v-if="withoutResults" class="vh-50 text-center" align-h="center">
                        <div class="text-center text-secondary my-2">
                            <strong>No hay resultados</strong>
                        </div>
                    </b-row>

                  
                    <b-row class="row justify-content-center justify-content-sm-between align-items-sm-center">
                      <div class="col-sm mb-2 mb-sm-0">
                        <div class="d-flex justify-content-center justify-content-sm-start align-items-center">
                            <span class="mr-2">Mostrar:</span>
                            <select class="js-select2-custom"
                                      data-hs-select2-options='{
                                      "minimumResultsForSearch": "Infinity",
                                      "customClass": "custom-select custom-select-sm custom-select-borderless",
                                      "dropdownAutoWidth": true,
                                      "width": true
                                      }'
                                        v-model="perPage"
                                        v-select="perPage"
                                        v-on:change="Buscar()">
                                  <option value="10">10</option>
                                  <option value="20">20</option>
                                  <option value="30">30</option>
                              </select>
                            <span class="text-secondary mr-2">registros</span>
                        </div>
                      </div>

                      <div class="col-sm-auto">
                          <div class="d-flex justify-content-center justify-content-sm-end " >
                              <b-pagination :total-rows="totalRows" :per-page="perPage" v-model="currentPage" class="m-0" v-on:change="Buscar">
                              </b-pagination>
                          </div>
                      </div>
                    </b-row>
                  </template>
                </div>
              </div>
            </div>
          </div>
        </div>
        </div>
    `,
    mounted: function () {
    },
    methods: {
        Buscar() {
            let vm = this;
            setTimeout(function () {
                
                return axios.get('/Usuario/GetAllUserPaginateForSearchValue', {
                    params: {
                        searchValue: vm.searchValue,
                        Page: vm.currentPage,
                        Size: vm.perPage
                    }
                }).then(function (response) {
                    vm.datastudent = response.data.Items.map((item) => {

                        return {
                            ...item,
                            StudentName: `${item.Nombre} ${item.ApellidoPaterno} ${item.ApellidoMaterno}`
                        }
                    });


                    vm.totalRows = response.data.Total;
                    return response.data;
                }).catch(function (error) {
                    return alert('Buscar Error: ${error}');
                });
            }, 0);
        },
        SelectClient(item) {
            this.$emit("click", item);
            $("#" + this.Nombre).modal("hide");
        },
    },
});