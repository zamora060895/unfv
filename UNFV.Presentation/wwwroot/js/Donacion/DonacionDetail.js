﻿(function () {
    'use strict';


    Vue.use(VeeValidate, {
        locale: 'es',
        dictionary: {
            es: {

                custom:
                {
                    //address: {
                    //    required: 'Ingresar dirección',
                    //},
                    nombreEstudiante: {
                        required: 'Seleccionar estudiante',
                    },
                }
            }
        }
    });

    Vue.use(bootstrapVue);

    Vue.directive("select", {
        twoWay: true,
        bind: function (el, binding, vnode) {
            $(el)
                .select2()
                .on("select2:select", (e) => {
                    el.dispatchEvent(new Event("change", { target: e.target }));
                });
        },
    });

    var app = new Vue({
        el: "#CreateCompleteCategory",
        data: {
            backupPlacement: {},
            update: false,
            prueba: "custom-select",
            //Create Client (Contractor)           
            address: '',
            NamesRoles: [
                { Id: 0, Name: "Administrador" },
                { Id: 0, Name: "Vendedor EPS" },
                { Id: 0, Name: "Operador Mesa de Control" },
                { Id: 0, Name: "Asistente comercial" },
                { Id: 0, Name: "Supervisor de solicitudes" },
                { Id: 0, Name: "Agente Comercial" },
            ],
  
            codigoEstudiante: 0,
            nombreEstudiante: '',
            currentPage: 1,
            perPage: 10,
            searching: null,
            withoutResults: null,
            totalRows: 100,
            total: 0,
            columnsTable: [
                { key: "CodigoSolicitud", label: "N° Solicitud", sortable: true },
                { key: "FechaSolicitud", label: "Fecha Solicitud" },
                { key: "DescripcionArticulo", label: "Artículo", sortable: true },
                { key: "DescripcionArea", label: "Área" },
                { key: "Cantidad", label: "Cantidad" },
                { key: "TotalCantidadDonado", label: "Total Cantidad Donado" },
                { key: "ValorAproximado", label: "Valor aproximado" },
                { key: "Estado", label: "Estado" },
                { key: 'statesOptions', label: '', thClass: 'text-center', tdClass: 'text-center' },
                {
                    key: "opciones",
                    label: "Opciones",
                    thClass: "text-center",
                    tdClass: "text-center",
                },

            ],

            columnsTableDonacion: [
                { key: "CodigoSolicitud", label: "N° Solicitud", sortable: true },
                { key: "FechaSolicitud", label: "Fecha Solicitud" },
                { key: "DescripcionArticulo", label: "Artículo", sortable: true },
                { key: "DescripcionArea", label: "Área" },
                { key: "Cantidad", label: "Cantidad" },
                { key: "ValorAproximado", label: "Valor aproximado" },
                { key: "Estado", label: "Estado" },
                { key: 'statesOptions', label: '', thClass: 'text-center', tdClass: 'text-center' },
                {
                    key: "opciones",
                    label: "Opciones",
                    thClass: "text-center",
                    tdClass: "text-center",
                },

            ],
            solicitudList: [],
              
            solicitudListAgregadass: [],
            nrosolicitud: '',
            donacionId: 0,
        },
        computed: {
            filteredFiles() {
                return this.fileList.filter(x => x.Name.toLowerCase().includes(this.searchFile.toLowerCase()));
            },

        },
        watch: {
        },
        created: function () {
            this.run_waitMe();
            //this.GetListRoles();
            this.initialConfig();
            this.GetAllSolicitudPendientePaginate(1);
        },
        mounted: function () {
            this.run_waitMe();
            //url params  
            var currentDate = moment().format("DD/MM/YYYY");
            this.saleDate = currentDate;
            let urlParams = new URLSearchParams(window.location.search);
            if (urlParams.get('p')) {
                let params = JSON.parse(atob(urlParams.get('p')));
                if (params.donacionId) {
                    this.donacionId = params.donacionId;
                    this.getDonacionById();
                }
                if (params.Update) {
                    this.update = params.Update;
                }
            }

            //end url params

            //this.inter();
            Promise.all([
                //this.GetAllMandatoryDocument()

            ]).then(values => {

            })
                .catch(error => {
                    console.error(error);
                }).finally(function () {
                    EasyLoading.hide();
                });


        },
        methods: {
            GetListRoles() {
                let vm = this;

                let roles = JSON.parse(sessionStorage.getItem("RolesKetalty"));
                if (roles) {
                    vm.ListRoles = roles;
                } else {
                    //window.location = "/Login/Index";
                }

                this.NamesRoles = this.NamesRoles.map((role) => {
                    let roleFind = roles.find((item) => {
                        return item.Name === role.Name;
                    });
                    return {
                        ...role,
                        Id: roleFind ? roleFind.Id : 0,
                    };
                });

                this.NamesRoles = this.NamesRoles.filter((role) => role.Id !== 0);

            },
            getDonacionById() {
                let vm = this;
                vm.GetAllSolicitudxDonacionPaginate();
                
                axios
                    .get("/Donation/GetDonacionById", {
                        params: {
                            donacionId: vm.donacionId
                        },
                    })
                    .then(async function (response) {
                        
                        vm.total = response.data.MontoTotal;  
                        vm.nombreEstudiante = response.data.Estudiante;
                        vm.codigoEstudiante = response.data.CodigoUsuarioEstudiante;
                    })
                    .catch(function (response) {
                        console.error('Error al obtener la donación');
                    })
                    .finally(function () {

                    });

            },
            async save() {
                let vm = this;
                
                vm.$validator.validateAll('formCreateSale').then(async function (result) {
                    if (result) {

                        
                        if (vm.nombreEstudiante === '') {
                            iziToast.error({
                                title: 'Error',
                                message: 'No seleccionó el estudiante.',
                                position: 'topRight',
                                timeout: 3000
                            });
                            return;
                        }

                        if (vm.solicitudListAgregadass.length === 0) {
                            iziToast.error({
                                title: 'Error',
                                message: 'No agregó ninguna solicitud para la donación.',
                                position: 'topRight',
                                timeout: 3000
                            });
                            return;
                        }
                        debugger;
                        //var miValor = @Model.MontoMinimoDonacion;
                        var miValor = document.getElementById("miDiv").getAttribute("data");

                        if (vm.total < miValor) {
                            iziToast.error({
                                title: 'Error',
                                message: `El monto mínimo de una donación es:  ${miValor} soles.`,
                                position: 'topRight',
                                timeout: 3000
                            });
                            return;
                        }

                        vm.saveDonacion();

                    } else {
                        
                        iziToast.error({
                            title: 'Error',
                            message: 'información incompleta.',
                            position: 'topRight',
                            timeout: 3000
                        });
                    }
                });
            },
            generalValidations() {
                let validation = false;
                let message = '';
                let affiliateWithIncompleteData = this.affiliatesList.find(affiliate => affiliate.RelationshipId == '' || affiliate.BirthDate == '');


                if (affiliateWithIncompleteData) {
                    validation = true;
                    message = 'Existen afiliados con datos incompletos.';
                } else if (this.affiliatesList.length === 0) {
                    validation = true;
                    message = 'No existen afiliados.';
                }



                if (validation) {
                    iziToast.error({
                        title: 'Error',
                        message: message,
                        position: 'topRight',
                        timeout: 3000
                    });
                }

                return validation;
            },
            getBase64: async function (file) {

                return new Promise((resolve, reject) => {
                    const reader = new FileReader();
                    reader.readAsDataURL(file);
                    reader.onload = () => resolve(reader.result);
                    reader.onerror = error => reject(error);
                });
            },
            saveDonacion() {
                let vm = this;

                for (const item of vm.solicitudListAgregadass) {
                    const solicitud = vm.solicitudList.find(x => x.CodigoSolicitud == item.CodigoSolicitud);
                    if (solicitud) {
                        const totalPendiente = solicitud.Cantidad - solicitud.TotalCantidadDonado;
                        const cantidadDonacion = item.Cantidad;
                        if (cantidadDonacion > totalPendiente) {
                            iziToast.error({
                                title: 'Error',
                                message: `la cantidad maxima para donar de la solicitud ${item.CodigoSolicitud} es ${totalPendiente}.`,
                                position: 'topRight',
                                timeout: 3000
                            });
                           
                            break;
                        }
                    }
                } 

                vm.run_waitMe();
                return axios({
                    method: 'post',
                    url: '/Donation/CreateDonacion',
                    data: {
                        "CodigoDonacion": vm.donacionId,
                        "Codigoestudiante": vm.codigoEstudiante,
                        "MontoTotal": vm.total,
                        "SolicitudList": vm.solicitudListAgregadass.map(item => {
                            return {
                                ...item
                            }
                        })
                    }
                }).then(function (response) {
                    Swal.fire({
                        title: vm.donacionId === 0 ? `Se creó la donación N° ${response.data.Id}` : `Se actualizaron los datos de la donación N° ${response.data.Id}`,
                        text: 'Buen Trabajo!',
                        icon: 'success',
                        confirmButtonColor: '#002F87',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'OK',
                        allowOutsideClick: false,
                    }).then((result) => {
                        if (result.isConfirmed) {
                            window.location = "/Donation/Index";
                        }
                    });


                    return response.data;
                }).catch(function (error) {
                    iziToast.error({
                        title: 'Error',
                        message: 'al crear la donación',
                        position: 'topRight',
                        timeout: 3000
                    });
                }).finally(function () {
                    EasyLoading.hide();
                });
            },
            initialConfig() {
                var currentDate = moment().format("DD-MM-YYYY");
                this.formatSaleDate = JSON.stringify(
                    {
                        "appendTo": "#saleDateflat",
                        "dateFormat": "d/m/Y",
                        "maxDate": currentDate,
                        "wrap": true,
                        "locale": {
                            "firstDayOfWeek": 0,
                            "weekdays": {
                                "shorthand": ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"]
                            },
                            "months": {
                                "longhand": ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"]
                            }
                        }
                    }
                );

                this.formatStartValidity = JSON.stringify(
                    {
                        "appendTo": "#startValidityFlatpickr",
                        "dateFormat": "d/m/Y",
                        "minDate": currentDate,
                        "wrap": true,
                        "locale": {
                            "firstDayOfWeek": 0,
                            "weekdays": {
                                "shorthand": ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"]
                            },
                            "months": {
                                "longhand": ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"]
                            }
                        }
                    }
                );

                this.formatStartOfContract = JSON.stringify(
                    {
                        "appendTo": "#startOfBillingFlat",
                        "dateFormat": "d/m/Y",
                        "minDate": currentDate,
                        "wrap": true,
                        "locale": {
                            "firstDayOfWeek": 0,
                            "weekdays": {
                                "shorthand": ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"]
                            },
                            "months": {
                                "longhand": ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"]
                            }
                        }
                    }
                );

            },
            pasar_colocacion() {
                window.location = "/Categoria/Index";
            },
            changeContractor(val) {
                this.person = val;
            },
            changeStep(item) {
                this.formAttachedDocumentShow = false;
                this.formAffiliateListShow = false;
                this.formPreAffiliationShow = false;
                if (item === 'contratante') {
                    this.contratanteShow = true;
                    this.placementShow = false;
                    this.placementShowHeader = false;
                    this.attachedDocumentShow = false;
                    this.preAffiliationShow = false;
                    this.affiliateListShow = false;
                    this.confirmationShow = false;
                } else if (item === 'placement') {
                    this.contratanteShow = true;
                    this.placementShow = true;
                    this.placementShowHeader = true;
                    this.attachedDocumentShow = false;
                    this.preAffiliationShow = false;
                    this.affiliateListShow = false;
                    this.confirmationShow = false;
                    this.formPreAffiliationShow = false;
                } else if (item === 'attachedDocument') {

                    this.contratanteShow = true;
                    this.placementShow = false;
                    this.placementShowHeader = true;
                    this.attachedDocumentShow = true;
                    this.preAffiliationShow = false;
                    this.affiliateListShow = false;
                    this.confirmationShow = false;

                    this.formAttachedDocumentShow = true;
                } else if (item === 'preAffiliation') {
                    this.contratanteShow = true;
                    this.placementShow = true;
                    this.placementShowHeader = true;
                    this.attachedDocumentShow = true;
                    this.preAffiliationShow = true;
                    this.affiliateListShow = false;
                    this.confirmationShow = false;

                    this.formPreAffiliationShow = true;
                } else if (item === 'affiliateList') {
                    this.contratanteShow = true;
                    this.placementShow = true;
                    this.placementShowHeader = true;
                    this.attachedDocumentShow = true;
                    this.preAffiliationShow = true;
                    this.affiliateListShow = true;
                    this.confirmationShow = false;

                    this.formAffiliateListShow = true;
                } else if (item === 'confirmation') {
                    this.previewCalculateAmountToPay();
                    this.contratanteShow = true;
                    this.placementShow = true;
                    this.placementShowHeader = true;
                    this.attachedDocumentShow = true;
                    this.preAffiliationShow = true;
                    this.affiliateListShow = true;
                    this.confirmationShow = true;
                }
            },
            run_waitMe: function () {

                EasyLoading.show({
                    type: EasyLoading.TYPE.BALL_SCALE_MULTIPLE,
                    text: "Espere por favor, procesando datos..."
                });

            },

            GetAllSolicitudPendientePaginate(flagStart) {
                let vm = this;
              
                vm.searching = true;
                vm.solicitudList = [];
                vm.withoutResults = false;

                setTimeout(function () {
                    var formData = new FormData();

                    let obj = {            
                        "nrosolicitud": vm.nrosolicitud,
                        "page": vm.currentPage,
                        "size": vm.perPage,
                    };
                    formData.append("SolicitudPaginateModel", JSON.stringify(obj));

                    return axios({
                        method: 'post',
                        url: '/Solicitud/GetAllSolicitudPendientePaginate',
                        data: formData,
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'multipart/form-data'
                        }
                    }).then(function (response) {
                         
                        vm.solicitudList = response.data.Items;
                        vm.totalRows = response.data.Total;
                        if (vm.totalRows === 0) {
                            vm.withoutResults = true;
                        }

                        /*vm.statusId = [];*/
                        return response.data;
                    })
                        .catch(function (response) {
                            vm.withoutResults = true;

                            return alert('Error al buscar solicitudes pendientes');
                        })
                        .finally(function () {
                            vm.searching = false;
                        });
                }, 0);
            },
            AddSolicitud(item, tipo) {
                $.blockUI({ message: '<h1>Procesando...</h1>' });
                
                let vm = this;  

                var nuevaSolicitud = {
                    CodigoSolicitud: item.CodigoSolicitud,
                    FechaSolicitud: item.FechaSolicitud ,
                    DescripcionArticulo: item.DescripcionArticulo,
                    DescripcionArea: item.DescripcionArea,
                    Cantidad: item.Cantidad - item.TotalCantidadDonado,
                    ValorAproximado: item.ValorAproximado,
                    Estado: item.Estado
                };

                var existeSolicitud = vm.solicitudListAgregadass.some(function (solicitud) {
                    return solicitud.CodigoSolicitud === nuevaSolicitud.CodigoSolicitud;
                });

                if (!existeSolicitud) {
                    vm.solicitudListAgregadass.push(nuevaSolicitud);
                    vm.total = vm.total + item.ValorAproximado;
                }
                else {
                    iziToast.error({
                        title: 'Error',
                        message: `Ya se agregó la solicitud N° ${item.CodigoSolicitud}`,
                        position: 'topRight',
                        timeout: 3000
                    });
                }               

                $.unblockUI();
            },
            QuitarSolicitud(item, tipo) {
                $.blockUI({ message: '<h1>Procesando...</h1>' });
                
                let vm = this;
                var codigoSolicitudEliminar = item.CodigoSolicitud;

                vm.solicitudListAgregadass = this.solicitudListAgregadass.filter(function (solicitud) {                   
                    return solicitud.CodigoSolicitud !== codigoSolicitudEliminar;                  
                });
                vm.total = vm.total - item.ValorAproximado;
                $.unblockUI();
            },
            selectStudent(item) {
                
                this.nombreEstudiante = item.Nombre + " " + item.ApellidoPaterno + " " + item.ApellidoMaterno;
                this.codigoEstudiante = item.CodigoUsuario;
                this.$validator.reset('formCreateSale');
            },
            GetAllSolicitudxDonacionPaginate() {
                let vm = this;

                vm.searching = true;
                vm.solicitudList = [];
                vm.withoutResults = false;

                setTimeout(function () {
                    var formData = new FormData();

                    let obj = {
                        "nrodonacion": vm.donacionId,
                        "page": vm.currentPage,
                        "size": vm.perPage,
                    };
                    formData.append("SolicitudPaginateModel", JSON.stringify(obj));

                    return axios({
                        method: 'post',
                        url: '/Solicitud/GetAllSolicitudxDonacionPaginate',
                        data: formData,
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'multipart/form-data'
                        }
                    }).then(function (response) {
                        
                        vm.solicitudListAgregadass = response.data.Items;
                        vm.totalRows = response.data.Total;
                        if (vm.totalRows === 0) {
                            vm.withoutResults = true;
                        }

                        /*vm.statusId = [];*/
                        return response.data;
                    })
                        .catch(function (response) {
                            vm.withoutResults = true;

                            return alert('Error al buscar solicitudes de la donación');
                        })
                        .finally(function () {
                            vm.searching = false;
                        });
                }, 0);
            },
        },
    });
})();