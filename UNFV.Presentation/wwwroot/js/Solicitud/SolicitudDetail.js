﻿(function () {
    'use strict';


    Vue.use(VeeValidate, {
        locale: 'es',
        dictionary: {
            es: {

                custom:
                {
                    address: {
                        required: 'Ingresar dirección',
                    },
                    codigofacultad: {
                        required: 'Seleccione facultad.',
                    },
                    codigoescuela: {
                        required: 'Seleccione escuela.',
                    },
                    codigoarea: {
                        required: 'Seleccione area.',
                    },
                    codigoarticulo: {
                        required: 'Seleccione articulo.',
                    },
                    cantidad: {
                        required: 'Ingrese cantidad.',
                    },
                    valor: {
                        required: 'Ingrese valor.',
                    },
                }
            }
        }
    });

    Vue.use(bootstrapVue);

    Vue.directive("select", {
        twoWay: true,
        bind: function (el, binding, vnode) {
            $(el)
                .select2()
                .on("select2:select", (e) => {
                    el.dispatchEvent(new Event("change", { target: e.target }));
                });
        },
    });

    var app = new Vue({
        el: "#CreateCompleteSolicitud",
        data: {
            solicitudId: 0,
            backupPlacement: {},
            update: false,
            prueba: "custom-select",
            //Create Client (Contractor)           
            address: '',
            NamesRoles: [
                { Id: 0, Name: "Administrador" },
                { Id: 0, Name: "Vendedor EPS" },
                { Id: 0, Name: "Operador Mesa de Control" },
                { Id: 0, Name: "Asistente comercial" },
                { Id: 0, Name: "Supervisor de solicitudes" },
                { Id: 0, Name: "Agente Comercial" },
            ],

            //mis variables
            codigofacultad: "",
            facultad: "",
            facultadList: [],
            codigoescuela: "",
            escuela: "",
            escuelaList: [],
            newEscuelaList: [],
            codigoarea: "",
            area: "",
            areaList: [],
            newAreaList: [],
            codigoarticulo: "",
            articulo: "",
            articuloList: "",
            solicitanteList: [{ Id: 1, Name: 'solicitante 1' }, { Id: 2, Name: 'solicitante  2' }, { Id: 3, Name: 'solicitante  3' }],
            estadoId: "",
            estadoList: [{ Id: 1, Name: 'Pendiente' }, { Id: 2, Name: 'En proceso' }, { Id: 3, Name: 'Aprobado' }, { Id: 4, Name: 'Entregado' }, { Id: 5, Name: 'Rechazado' }],

            cantidad: "",
            valor: "",
            cleanEscuelaCodigo: true,
            cleanAreaCodigo: true,
        },
        computed: {
            filteredFiles() {
                return this.fileList.filter(x => x.Name.toLowerCase().includes(this.searchFile.toLowerCase()));
            },

        },
        watch: {
            codigofacultad: function (val, oldVal) {
                
                this.cleanEscuela();
                
                this.newEscuelaList = this.escuelaList.filter(item => {
                    if (item.CodigoFacultad === val) {
                        return item;
                    }
                });
            },
            newEscuelaList: async function () {
                await this.$nextTick();
                $(this.$refs.codigoescuela).select2();
                $(".js-select2-custom").each(function () {
                    $.HSCore.components.HSSelect2.init($(this));
                });
            },
            codigoescuela: function (val, oldVal) {
                this.cleanArea();
                this.newAreaList = this.areaList.filter(item => {
                    if (item.CodigoEscuela === val) {
                        return item;
                    }
                });
            },
            newAreaList: async function () {
                await this.$nextTick();
                $(this.$refs.codigoarea).select2();
                $(".js-select2-custom").each(function () {
                    $.HSCore.components.HSSelect2.init($(this));
                });
            },
        },
        created: function () {
            this.run_waitMe();
            //this.GetListRoles();
            this.GetAllFacultad();
            this.GetAllEscuela();
            this.GetAllArea();
            this.GetAllArticulo();
            this.initialConfig();
        },
        mounted: function () {
            this.run_waitMe();
            //url params  
            var currentDate = moment().format("DD/MM/YYYY");
            this.saleDate = currentDate;
            let urlParams = new URLSearchParams(window.location.search);
            if (urlParams.get('p')) {
                let params = JSON.parse(atob(urlParams.get('p')));
                if (params.solicitudId) {
                    this.solicitudId = params.solicitudId;
                    this.getSolicitudById();
                }
                if (params.Update) {
                    this.update = params.Update;
                }
            }

            //end url params

            //this.inter();
            Promise.all([
                //this.GetAllMandatoryDocument()

            ]).then(values => {

            })
                .catch(error => {
                    console.error(error);
                }).finally(function () {
                    EasyLoading.hide();
                });


        },
        methods: {
            GetListRoles() {
                let vm = this;

                let roles = JSON.parse(sessionStorage.getItem("RolesKetalty"));
                if (roles) {
                    vm.ListRoles = roles;
                } else {
                    //window.location = "/Login/Index";
                }

                this.NamesRoles = this.NamesRoles.map((role) => {
                    let roleFind = roles.find((item) => {
                        return item.Name === role.Name;
                    });
                    return {
                        ...role,
                        Id: roleFind ? roleFind.Id : 0,
                    };
                });

                this.NamesRoles = this.NamesRoles.filter((role) => role.Id !== 0);

            },

            getSolicitudById() {
                let vm = this;
                
                axios
                    .get("/Solicitud/GetAreaById", {
                        params: {
                            solicitudId: vm.solicitudId
                        },
                    })
                    .then(async function (response) {
                        vm.valor = response.data.ValorAproximado;
                        vm.cantidad = response.data.Cantidad;
                        
                        vm.codigofacultad = response.data.CodigoFacultad;
                        vm.facultad = response.data.DescripcionFacultad;
                        vm.escuela = response.data.DescripcionEscuela;
                        vm.area = response.data.DescripcionArea;
                        vm.articulo = response.data.DescripcionArticulo;
                        vm.codigoescuela = response.data.CodigoEscuela;
                        
                        vm.codigoarticulo = response.data.CodigoArticulo;
                        vm.cleanEscuelaCodigo = false;
                        vm.cleanAreaCodigo = false;

                        await vm.$nextTick();
                        var element = document.getElementById("codigoarticulo");
                        $.HSCore.components.HSSelect2.init($(element));                        
                        var element = document.getElementById("codigofacultad");
                        $.HSCore.components.HSSelect2.init($(element));                        
                        var element = document.getElementById("codigoescuela");
                        $.HSCore.components.HSSelect2.init($(element));     
                        vm.codigoarea = response.data.CodigoArea;
                        var element = document.getElementById("codigoarea");
                        $.HSCore.components.HSSelect2.init($(element));

                    })
                    .catch(function (response) {
                        console.error('Error al obtener la solicitud');
                    })
                    .finally(function () {

                    });

            },
            async save() {
                let vm = this;
                
                vm.$validator.validateAll('formCreateSolicitud').then(async function (result) {
                    if (result) {
                        vm.saveSolicitud();
                    } else {
                        iziToast.error({
                            title: 'Error',
                            message: 'información incompleta.',
                            position: 'topRight',
                            timeout: 3000
                        });
                    }
                });
            },
            saveSolicitud: async function () {
                let vm = this;
                vm.run_waitMe();
                var formData = new FormData();
                
                let obj = {
                    "CodigoSolicitud": vm.solicitudId,
                    "CodigoArea": vm.codigoarea,
                    "CodigoArticulo": vm.codigoarticulo,
                    "ValorAproximado": vm.valor,
                    "Cantidad": vm.cantidad
                };

                formData.append("model", JSON.stringify(obj));

                return axios({
                    method: 'post',
                    url: vm.solicitudId === 0 ? '/Solicitud/CreateSolicitud' : '/Solicitud/CreateSolicitud',
                    data: formData,
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data'
                    }
                }).then(function (response) {
                    Swal.fire({
                        title: vm.solicitudId === 0 ? `Se creó la solicitud N° ${response.data.Id}` : `Se actualizaron los datos de la solicitud N° ${response.data.Id}`,
                        text: 'Buen Trabajo!',
                        icon: 'success',
                        confirmButtonColor: '#002F87',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'OK',
                        allowOutsideClick: false,

                    }).then((result) => {
                        if (result.isConfirmed) {
                            window.location = "/Solicitud/Index";
                        }
                    });


                    return response.data;
                }).catch(function (error) {
                    iziToast.error({
                        title: 'Error',
                        message: vm.solicitudId === 0 ? 'al crear la solicitud' : 'al actualizar la solicitud',
                        position: 'topRight',
                        timeout: 3000
                    });
                }).finally(function () {
                    EasyLoading.hide();
                });
            },
            initialConfig() {
                var currentDate = moment().format("DD-MM-YYYY");
                this.formatSaleDate = JSON.stringify(
                    {
                        "appendTo": "#saleDateflat",
                        "dateFormat": "d/m/Y",
                        "maxDate": currentDate,
                        "wrap": true,
                        "locale": {
                            "firstDayOfWeek": 0,
                            "weekdays": {
                                "shorthand": ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"]
                            },
                            "months": {
                                "longhand": ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"]
                            }
                        }
                    }
                );

                this.formatStartValidity = JSON.stringify(
                    {
                        "appendTo": "#startValidityFlatpickr",
                        "dateFormat": "d/m/Y",
                        "minDate": currentDate,
                        "wrap": true,
                        "locale": {
                            "firstDayOfWeek": 0,
                            "weekdays": {
                                "shorthand": ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"]
                            },
                            "months": {
                                "longhand": ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"]
                            }
                        }
                    }
                );

                this.formatStartOfContract = JSON.stringify(
                    {
                        "appendTo": "#startOfBillingFlat",
                        "dateFormat": "d/m/Y",
                        "minDate": currentDate,
                        "wrap": true,
                        "locale": {
                            "firstDayOfWeek": 0,
                            "weekdays": {
                                "shorthand": ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"]
                            },
                            "months": {
                                "longhand": ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"]
                            }
                        }
                    }
                );

            },
            pasar_colocacion() {
                window.location = "/Area/Index";
            },
            changeContractor(val) {
                this.person = val;
            },
            changeStep(item) {
                this.formAttachedDocumentShow = false;
                this.formAffiliateListShow = false;
                this.formPreAffiliationShow = false;
                if (item === 'contratante') {
                    this.contratanteShow = true;
                    this.placementShow = false;
                    this.placementShowHeader = false;
                    this.attachedDocumentShow = false;
                    this.preAffiliationShow = false;
                    this.affiliateListShow = false;
                    this.confirmationShow = false;
                } else if (item === 'placement') {
                    this.contratanteShow = true;
                    this.placementShow = true;
                    this.placementShowHeader = true;
                    this.attachedDocumentShow = false;
                    this.preAffiliationShow = false;
                    this.affiliateListShow = false;
                    this.confirmationShow = false;
                    this.formPreAffiliationShow = false;
                } else if (item === 'attachedDocument') {

                    this.contratanteShow = true;
                    this.placementShow = false;
                    this.placementShowHeader = true;
                    this.attachedDocumentShow = true;
                    this.preAffiliationShow = false;
                    this.affiliateListShow = false;
                    this.confirmationShow = false;

                    this.formAttachedDocumentShow = true;
                } else if (item === 'preAffiliation') {
                    this.contratanteShow = true;
                    this.placementShow = true;
                    this.placementShowHeader = true;
                    this.attachedDocumentShow = true;
                    this.preAffiliationShow = true;
                    this.affiliateListShow = false;
                    this.confirmationShow = false;

                    this.formPreAffiliationShow = true;
                } else if (item === 'affiliateList') {
                    this.contratanteShow = true;
                    this.placementShow = true;
                    this.placementShowHeader = true;
                    this.attachedDocumentShow = true;
                    this.preAffiliationShow = true;
                    this.affiliateListShow = true;
                    this.confirmationShow = false;

                    this.formAffiliateListShow = true;
                } else if (item === 'confirmation') {
                    this.previewCalculateAmountToPay();
                    this.contratanteShow = true;
                    this.placementShow = true;
                    this.placementShowHeader = true;
                    this.attachedDocumentShow = true;
                    this.preAffiliationShow = true;
                    this.affiliateListShow = true;
                    this.confirmationShow = true;
                }
            },
            run_waitMe: function () {

                EasyLoading.show({
                    type: EasyLoading.TYPE.BALL_SCALE_MULTIPLE,
                    text: "Espere por favor, procesando datos..."
                });

            },
            GetAllEscuela() {
                let vm = this;
                return axios
                    .get("/Area/GetAllEscuela", {

                    })
                    .then(function (response) {
                        vm.escuelaList = response.data;
                        return response.data;
                    })
                    .catch(function (response) {

                        console.error(`Error al buscar escuelas`);
                    })
                    .finally(function () {

                    });

            },
            GetAllArea() {
                let vm = this;
                return axios
                    .get("/Area/GetAllArea", {

                    })
                    .then(function (response) {
                        vm.areaList = response.data;
                        return response.data;
                    })
                    .catch(function (response) {

                        console.error(`Error al buscar áreas`);
                    })
                    .finally(function () {

                    });

            },
            GetAllFacultad() {
                let vm = this;
                return axios
                    .get("/Area/GetAllFacultad", {

                    })
                    .then(function (response) {
                        vm.facultadList = response.data;
                        return response.data;
                    })
                    .catch(function (response) {

                        console.error(`Error al buscar facultad`);
                    })
                    .finally(function () {

                    });

            },
            GetAllArticulo() {
                let vm = this;
                return axios
                    .get("/Articulo/GetAllArticulo", {

                    })
                    .then(function (response) {
                        vm.articuloList = response.data;
                        return response.data;
                    })
                    .catch(function (response) {

                        console.error(`Error al buscar articulo`);
                    })
                    .finally(function () {

                    });

            },
            //Create Client (Contractor)
            
            cleanEscuela() {
                if (this.cleanEscuelaCodigo) {
                    this.codigoescuela = '';
                }
                else {
                    this.cleanEscuelaCodigo = true;
                }
            },
            cleanArea() {
                if (this.cleanAreaCodigo) {
                    this.codigoarea = '';
                }
                else {
                    this.cleanAreaCodigo = true;
                }
            },
        },
    });
})();