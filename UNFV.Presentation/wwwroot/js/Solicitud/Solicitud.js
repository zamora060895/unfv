﻿(function () {
    'use strict';

    Vue.use(bootstrapVue);

    Vue.directive("select", {
        twoWay: true,
        bind: function (el, binding, vnode) {
            $(el)
                .select2()
                .on("select2:select", (e) => {
                    el.dispatchEvent(new Event("change", { target: e.target }));
                });
        },
    });

    var app = new Vue({
        el: "#Solicitud",
        data: {
            codigofacultad: '',
            codigoescuela: '',
            codigoarea: '',
            codigoestado: '',
            facultadList: [],
            escuelaList: [],
            areaList: [],
            estadoList: [],
            solicitanteId: "",
            solicitanteList: [{ Id: 1, Name: 'solicitante 1' }, { Id: 2, Name: 'solicitante  2' }, { Id: 3, Name: 'solicitante  3' }],

            columnsTable: [
                { key: "CodigoSolicitud", label: "N° Solicitud", sortable: true },
                { key: "FechaSolicitud", label: "Fecha Solicitud" },
                { key: "CodigoDonacion", label: "N° Donación", sortable: true },
                { key: "DescripcionArticulo", label: "Artículo", sortable: true },
                { key: "DescripcionArea", label: "Área" },
                { key: "Cantidad", label: "Cantidad" },
                { key: "ValorAproximado", label: "Valor aproximado" },
                { key: "Estado", label: "Estado" },
                { key: 'statesOptions', label: '', thClass: 'text-center', tdClass: 'text-center' },
                {
                    key: "opciones",
                    label: "Opciones",
                    thClass: "text-center",
                    tdClass: "text-center",
                },

            ],

            solicitudList: [
            ],

            solicitudTrackingList: [
            ],
            columnsTableTracking: [
                { key: "fechacreacion", label: "Fecha registro", sortable: true },
                { key: "usuario", label: "Usuario" },
                { key: "estado", label: "Estado" },
                { key: "comentario", label: "Comentario" },
            ],

            //fin mis variables
            SaleData: {},
            searching: false,
            withoutResults: false,
            salesList: [],
            totalRows: 0,
            currentPage: 1,
            perPage: 10,


            SelectStatus: [
                { Id: 1, Name: "Pendiente" },
                { Id: 2, Name: "Verificado" },
                { Id: 3, Name: "Verificado por mesa de control" },
            ],

            creationDateFrom: '',
            creationDateTo: '',
            saleId: '',
            description: '',
            clientName: '',
            identityDocument: '',
            seller: '',
            statusId: [],

            ListRoles: [],
            statusList: [],

            //add file
            saleIdForFile: '',
            valueFile: '',
            fileName: '',
            fileNote: '',

            //Roles
            NamesRoles: [
                { Id: 1, Name: "Administrador" },
                { Id: 2, Name: "Operador Donaciones" },
                { Id: 3, Name: "Operador Solicitudes" },
            ],
            SecondClient: "",
            SecondClientCode: "",
            SecondBroker: "",
            SecondBrokerCode: "",
            SecondManager: "",
            SecondManagerCode: "",
            solicitudIdTracking: '',

            currentPageTracking: 1,
            perPageTracking: 10,
            searchingTracking: false,
            withoutResultsTracking: false,
            totalRowsTracking: 0,
            checkboxSigned: false,
            cleanEscuelaCodigo: true,
            cleanAreaCodigo: true,
            newEscuelaList: [],
            newAreaList: [],
            nrosolicitud: '',
        },
        watch: {
            codigofacultad: function (val, oldVal) {
                
                this.cleanEscuela();
                
                this.newEscuelaList = this.escuelaList.filter(item => {
                    if (item.CodigoFacultad === val) {
                        return item;
                    }
                });
            },
            newEscuelaList: async function () {
                await this.$nextTick();
                $(this.$refs.codigoescuela).select2();
                $(".js-select2-custom").each(function () {
                    $.HSCore.components.HSSelect2.init($(this));
                });
            },
            codigoescuela: function (val, oldVal) {
                this.cleanArea();
                this.newAreaList = this.areaList.filter(item => {
                    if (item.CodigoEscuela === val) {
                        return item;
                    }
                });
            },
            newAreaList: async function () {
                await this.$nextTick();
                $(this.$refs.codigoarea).select2();
                $(".js-select2-custom").each(function () {
                    $.HSCore.components.HSSelect2.init($(this));
                });
            },
        },
        created: function () {
            this.GetListRoles();
            //this.GetAllPlacementStatus();
            this.GetAllFacultad();
            this.GetAllEscuela();
            this.GetAllArea();
            this.GetAllEstado();
            this.GetAllSolicitudPaginate(1);
        },
        mounted: function () {

        },
        methods: {
            getSolicitudTracking(item) {
                let vm = this;
                vm.solicitudIdTracking = item.CodigoSolicitud;
                $("#trackingModal").modal()
                vm.BtnBuscarTracking();
            },
            BtnBuscarTracking() {
                let vm = this;
                vm.searchingTracking = true;
                vm.solicitudTrackingList = [];
                vm.withoutResultsTracking = false;

                setTimeout(function () {
                    return axios
                        .get("/Solicitud/GetAllSolicitudTrackingPaginate", {
                            params: {
                                codigosolicitud: vm.solicitudIdTracking,
                                Page: vm.currentPageTracking,
                                Size: vm.perPageTracking,
                            },
                        })
                        .then(function (response) {
                            vm.solicitudTrackingList = response.data.Items;
                            return response.data;
                        })
                        .catch(function (response) {
                            vm.withoutResultsTracking = true;
                            console.error('Error al buscar el historial');
                        })
                        .finally(function () {
                            vm.searchingTracking = false;
                        });
                }, 0);
            },
            closeTracking() {
                let vm = this;
                vm.solicitudIdTracking = '';
                $('#trackingModal').modal('hide');
            },
            GetAllSolicitudPaginate(flagStart) {
                let vm = this;

                if ($('#creationDate').val() != "") {
                    vm.creationDateFrom = $.trim($('#creationDate').val().split('-')[0]);
                    vm.creationDateTo = $.trim($('#creationDate').val().split('-')[1]);
                }
                else {
                    var dateNow = new Date();
                    vm.creationDateFrom = new Date(dateNow.getFullYear(), dateNow.getMonth() - 2, 1);
                    vm.creationDateTo = new Date(dateNow.getFullYear(), dateNow.getMonth() + 1, 0);
                }

                vm.searching = true;
                vm.solicitudList = [];
                vm.withoutResults = false;

                setTimeout(function () {
                    var formData = new FormData();

                    let obj = {
                        "creationDateFrom": vm.creationDateFrom,
                        "creationDateTo": vm.creationDateTo,
                        "codigofacultad": vm.codigofacultad,
                        "codigoescuela": vm.codigoescuela,
                        "codigoarea": vm.codigoarea,
                        "codigoestado": vm.codigoestado,
                        "nrosolicitud": vm.nrosolicitud,
                        "page": vm.currentPage,
                        "size": vm.perPage,
                    };
                    formData.append("SolicitudPaginateModel", JSON.stringify(obj));

                    return axios({
                        method: 'post',
                        url: '/Solicitud/GetAllSolicitudPaginate',
                        data: formData,
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'multipart/form-data'
                        }
                    }).then(function (response) {
                        
                        vm.solicitudList = response.data.Items;
                        vm.totalRows = response.data.Total;
                        if (vm.totalRows === 0) {
                            vm.withoutResults = true;
                        }

                        /*vm.statusId = [];*/
                        return response.data;
                    })
                        .catch(function (response) {
                            vm.withoutResults = true;

                            return alert('Error al buscar solicitud');
                        })
                        .finally(function () {
                            vm.searching = false;
                        });
                }, 0);
            },
            GetListRoles() {
                let vm = this;
                
                let roles = JSON.parse(sessionStorage.getItem("RolesKetalty"));
                if (Array.isArray(roles.Result)) {
                    vm.ListRoles = roles;
                } else {
                    window.location = "/Login/Index";
                }

                this.NamesRoles = this.NamesRoles.map((role) => {
                    let roleFind = roles.Result.find((item) => {
                        return item.Nombre === role.Name;
                    });
                    return {
                        ...role,
                        Id: roleFind ? roleFind.CodigoRol : 0,
                    };
                });

                this.NamesRoles = this.NamesRoles.filter((role) => role.Id !== 0);
            },
            newSolicitud() {
                window.location = "/Solicitud/SolicitudDetail";
            },
            solicitudDetail({ CodigoSolicitud, Status }) {
                let url = "/Solicitud/SolicitudDetail";
                let update = false;
                update = true;
                
                let params = { solicitudId: CodigoSolicitud, Update: update, };
                window.location = `${url}?p=${btoa(JSON.stringify(params))}`;
            },
            run_waitMe: function () {

                EasyLoading.show({
                    type: EasyLoading.TYPE.BALL_SCALE_MULTIPLE,
                    text: "Espere por favor, procesando datos..."
                });

            },
            ChangeStatusCategory(item, tipo) {
                $.blockUI({ message: '<h1>Procesando...</h1>' });
                let vm = this;

                var data = {};
                if (tipo === 'enviado') {
                    data = {
                        Id: item.Id,
                        PlacementStatusId: 2,
                        Commentary: 'Cambiado a enviado',
                        NotificationText: 'Desea enviar venta al asistente comercial',
                        PlacementSubProductId: item.SubProductTypeId,
                    };
                } else if (tipo === 'verificado') {
                    data = {
                        Id: item.Id,
                        PlacementStatusId: 3,
                        Commentary: 'Enviado a mesa de control',
                        NotificationText: 'Desea enviar venta a mesa de control',
                        PlacementSubProductId: item.SubProductTypeId,
                    };
                } else if (tipo === 'rechazado') {
                    data = {
                        Id: item.Id,
                        PlacementStatusId: 5,
                        Commentary: '',
                        NotificationText: 'Desea rechazar la venta',
                        PlacementSubProductId: item.SubProductTypeId,
                    };
                } else if (tipo === 'devueltoCom') {
                    data = {
                        Id: item.Id,
                        PlacementStatusId: 7,
                        Commentary: '',
                        NotificationText: 'Desea devolver la venta al asesor',
                        PlacementSubProductId: item.SubProductTypeId,
                    };
                } else if (tipo === 'devuelto') {
                    data = {
                        Id: item.Id,
                        PlacementStatusId: 6,
                        Commentary: '',
                        NotificationText: 'Desea devolver la venta al asistente comercial',
                        PlacementSubProductId: item.SubProductTypeId,
                    };
                } else {
                    //mesa de control
                    data = {
                        Id: item.Id,
                        PlacementStatusId: 4,
                        Commentary: 'Cambiado a verificado por Mesa de Control',
                        NotificationText: 'Desea cambiar el estado a verificado por Mesa de Control',
                        PlacementSubProductId: item.SubProductTypeId,
                    };
                }


                vm.SaleData = data;

                $("#SendPlacementControlTablePopUp").modal('show');

                $.unblockUI();
            },
            GetAllFacultad() {
                let vm = this;
                return axios
                    .get("/Area/GetAllFacultad", {

                    })
                    .then(function (response) {
                        vm.facultadList = response.data;
                        vm.GetAllSolicitudPaginate(1);

                        return response.data;
                    })
                    .catch(function (response) {

                        console.error(`Error al buscar facultad`);
                    })
                    .finally(function () {

                    });

            },
            GetAllEscuela() {
                let vm = this;
                return axios
                    .get("/Area/GetAllEscuela", {

                    })
                    .then(function (response) {
                        vm.escuelaList = response.data;
                        vm.GetAllSolicitudPaginate(1);

                        return response.data;
                    })
                    .catch(function (response) {

                        console.error(`Error al buscar escuelas`);
                    })
                    .finally(function () {

                    });

            },
            GetAllArea() {
                let vm = this;
                return axios
                    .get("/Area/GetAllArea", {

                    })
                    .then(function (response) {
                        vm.areaList = response.data;
                        vm.GetAllSolicitudPaginate(1);

                        return response.data;
                    })
                    .catch(function (response) {

                        console.error(`Error al buscar área`);
                    })
                    .finally(function () {

                    });

            },
            GetAllEstado() {
                let vm = this;
                return axios
                    .get("/Solicitud/GetAllEstadoSolicitud", {

                    })
                    .then(function (response) {
                        vm.estadoList = response.data;
                        vm.GetAllSolicitudPaginate(1);

                        return response.data;
                    })
                    .catch(function (response) {

                        console.error(`Error al buscar estados`);
                    })
                    .finally(function () {

                    });

            },
            cleanEscuela() {
                if (this.cleanEscuelaCodigo) {
                    this.codigoescuela = '';
                }
                else {
                    this.cleanEscuelaCodigo = true;
                }
            },
            cleanArea() {
                if (this.cleanAreaCodigo) {
                    this.codigoarea = '';
                }
                else {
                    this.cleanAreaCodigo = true;
                }
            },
            ChangeStatusSolicitud(item, tipo) {
                $.blockUI({ message: '<h1>Procesando...</h1>' });
                let vm = this;
                vm.run_waitMe();
                var formData = new FormData();
                var estadosoli = 1;
                
                if (tipo == 'RB') {
                    estadosoli = 4;
                }
                else if (tipo == 'RZ') {
                    estadosoli = 5;
                }

                let obj = {
                    "CodigoSolicitud": item.CodigoSolicitud,
                    "CodigoEstado": estadosoli,
                };

                formData.append("model", JSON.stringify(obj));

                return axios({
                    method: 'post',
                    url: '/Solicitud/ChangeStatusSolicitud',
                    data: formData,
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data'
                    }
                }).then(function (response) {
                    
                    Swal.fire({
                        title: estadosoli == 4 ? `Se recibió la solicitud N° ${response.data.Id}` : `Se rechazó la solicitud N° ${response.data.Id}`,
                        text: 'Buen Trabajo!',
                        icon: 'success',
                        confirmButtonColor: '#002F87',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'OK',
                        allowOutsideClick: false,

                    }).then((result) => {
                        if (result.isConfirmed) {
                            window.location = "/Solicitud/Index";
                        }
                    });


                    return response.data;
                }).catch(function (error) {
                    iziToast.error({
                        title: 'Error',
                        message: 'al cambiar de estado la solicitud',
                        position: 'topRight',
                        timeout: 3000
                    });
                }).finally(function () {
                    EasyLoading.hide();
                });

                $.unblockUI();
            },
        },

    });
})();