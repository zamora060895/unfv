﻿(function () {
    'use strict';

    Vue.use(bootstrapVue);

    Vue.directive("select", {
        twoWay: true,
        bind: function (el, binding, vnode) {
            $(el)
                .select2()
                .on("select2:select", (e) => {
                    el.dispatchEvent(new Event("change", { target: e.target }));
                });
        },
    });

    var app = new Vue({
        el: "#Area",
        data: {
            SaleData: {},
            searching: false,
            withoutResults: false,
            salesList: [],
            totalRows: 0,
            currentPage: 1,
            perPage: 10,
            columnsTable: [
                { key: "CodigoArea", label: "Código", sortable: true },
                { key: "Descripcion", label: "Descripcion", sortable: true },
                { key: "DescripcionEscuela", label: "Escuela", sortable: true },
                { key: "Estado", label: "Estado" },
                { key: 'statesOptions', label: '', thClass: 'text-center', tdClass: 'text-center' },
                {
                    key: "opciones",
                    label: "Opciones",
                    thClass: "text-center",
                    tdClass: "text-center",
                },

            ],

            SelectStatus: [
                { Id: 1, Name: "Pendiente" },
                { Id: 2, Name: "Verificado" },
                { Id: 3, Name: "Verificado por mesa de control" },
            ],

            creationDateFrom: '',
            creationDateTo: '',
            saleId: '',
            description: '',
            codigoescuela:'',
            clientName: '',
            identityDocument: '',
            seller: '',
            statusId: [],

            ListRoles: [],
            escuelaList: [],
            codigoescuela: '',
            //add file
            saleIdForFile: '',
            valueFile: '',
            fileName: '',
            fileNote: '',

            //Roles
            NamesRoles: [
                { Id: 0, Name: "Administrador" },
                { Id: 0, Name: "Vendedor EPS" },
                { Id: 0, Name: "Operador Mesa de Control" },
                { Id: 0, Name: "Asistente comercial" },
                { Id: 0, Name: "Supervisor de solicitudes" },
                { Id: 0, Name: "Agente Comercial" },
            ],
            SecondClient: "",
            SecondClientCode: "",
            SecondBroker: "",
            SecondBrokerCode: "",
            SecondManager: "",
            SecondManagerCode: "",
            placementIdTracking: '',
            placementTrackingList: [],
            columnsTableTracking: [
                { key: "CreationDate", label: "Fecha registro", sortable: true },
                { key: "UserName", label: "Usuario" },
                { key: "PlacementStatusName", label: "Estado" },
                /* { key: "PlacementMotiveName", label: "Motivo" },*/
                { key: "Commentary", label: "Comentario" },
            ],
            currentPageTracking: 1,
            perPageTracking: 10,
            searchingTracking: false,
            withoutResultsTracking: false,
            totalRowsTracking: 0,
            checkboxSigned: false

        },
        watch: {

            valueFile: function (file) {
                if (document.getElementById("loadFile").files[0])
                    this.fileName = document.getElementById("loadFile").files[0].name;
            },
        },
        created: function () {
            //this.GetListRoles();
            this.GetAllEscuela();
            this.GetAllAreaPaginate(1);
        },
        mounted: function () {

        },
        methods: {
            GetListRoles() {
                let vm = this;
                debugger;
                let roles = JSON.parse(sessionStorage.getItem("RolesKetalty"));
                if (roles) {
                    vm.ListRoles = roles;
                } else {
                    window.location = "/Login/Index";
                }

                this.NamesRoles = this.NamesRoles.map((role) => {
                    let roleFind = roles.find((item) => {
                        return item.Name === role.Name;
                    });
                    return {
                        ...role,
                        Id: roleFind ? roleFind.Id : 0,
                    };
                });

                this.NamesRoles = this.NamesRoles.filter((role) => role.Id !== 0);
            },

            GetAllAreaPaginate(flagStart) {
                let vm = this;
                vm.searching = true;
                vm.salesList = [];
                vm.withoutResults = false;

                setTimeout(function () {
                    var formData = new FormData();

                    let obj = {
                        //"creationDateFrom": vm.creationDateFrom,
                        //"creationDateTo": vm.creationDateTo,
                        "codigoescuela": vm.codigoescuela,
                        "descripcion": vm.description,
                        "page": vm.currentPage,
                        "size": vm.perPage,
                    };
                    formData.append("AreaPaginateModel", JSON.stringify(obj));

                    return axios({
                        method: 'post',
                        url: '/Area/GetAllAreaPaginate',
                        data: formData,
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'multipart/form-data'
                        }
                    }).then(function (response) {
                        vm.salesList = response.data.Items;
                        vm.totalRows = response.data.Total;
                        if (vm.totalRows === 0) {
                            vm.withoutResults = true;
                        }

                        /*vm.statusId = [];*/
                        return response.data;
                    })
                        .catch(function (response) {
                            vm.withoutResults = true;

                            return alert('Error al buscar area');
                        })
                        .finally(function () {
                            vm.searching = false;
                        });
                }, 0);
            },

            newArea() {
                window.location = "/Area/AreaDetail";
            },
            areaDetail({ CodigoArea, Status }) {
                let url = "/Area/AreaDetail";

                let update = false;
                update = true;
                debugger;
                let params = { areaId: CodigoArea, Update: update, };
                window.location = `${url}?p=${btoa(JSON.stringify(params))}`;
            },
            run_waitMe: function () {

                EasyLoading.show({
                    type: EasyLoading.TYPE.BALL_SCALE_MULTIPLE,
                    text: "Espere por favor, procesando datos..."
                });

            },
            ChangeStatusArea(item, tipo) {
                $.blockUI({ message: '<h1>Procesando...</h1>' });
                let vm = this;
                vm.run_waitMe();
                var formData = new FormData();
                debugger;
                let obj = {
                    "CodigoArea": item.CodigoArea,
                    "estado": item.Estado,
                };

                formData.append("model", JSON.stringify(obj));

                return axios({
                    method: 'post',
                    url: '/Area/ChangeStatusArea',
                    data: formData,
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data'
                    }
                }).then(function (response) {
                    debugger;
                    Swal.fire({
                        title: item.Estado == "Activo" ? `Se deshabilitó el área N° ${response.data.Id}` : `Se habilitó el área N° ${response.data.Id}`,
                        text: 'Buen Trabajo!',
                        icon: 'success',
                        confirmButtonColor: '#002F87',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'OK',
                        allowOutsideClick: false,

                    }).then((result) => {
                        if (result.isConfirmed) {
                            window.location = "/Area/Index";
                        }
                    });


                    return response.data;
                }).catch(function (error) {
                    iziToast.error({
                        title: 'Error',
                        message: 'al cambiar de estado el área',
                        position: 'topRight',
                        timeout: 3000
                    });
                }).finally(function () {
                    EasyLoading.hide();
                });

                $.unblockUI();
            },
            GetAllEscuela() {
                let vm = this;
                return axios
                    .get("/Area/GetAllEscuela", {

                    })
                    .then(function (response) {
                        vm.escuelaList = response.data;
                        vm.GetAllAreaPaginate(1);

                        return response.data;
                    })
                    .catch(function (response) {

                        console.error(`Error al buscar escuelas`);
                    })
                    .finally(function () {

                    });

            },
        },

    });
})();