﻿(function () {
    'use strict';


    Vue.use(VeeValidate, {
        locale: 'es',
        dictionary: {
            es: {

                custom:
                {
                    descripcion: {
                        required: 'Ingresar dirección',
                    },
                    codigoescuela: {
                        required: 'Seleccione la escuela.',
                    },
                }
            }
        }
    });

    Vue.use(bootstrapVue);

    Vue.directive("select", {
        twoWay: true,
        bind: function (el, binding, vnode) {
            $(el)
                .select2()
                .on("select2:select", (e) => {
                    el.dispatchEvent(new Event("change", { target: e.target }));
                });
        },
    });

    var app = new Vue({
        el: "#CreateCompleteArea",
        data: {
            areaId: 0,
            update: false,
            prueba: "custom-select",
            //Create Client (Contractor)           
            descripcion: '',
            NamesRoles: [
                { Id: 0, Name: "Administrador" },
                { Id: 0, Name: "Vendedor EPS" },
                { Id: 0, Name: "Operador Mesa de Control" },
                { Id: 0, Name: "Asistente comercial" },
                { Id: 0, Name: "Supervisor de solicitudes" },
                { Id: 0, Name: "Agente Comercial" },
            ],
            escuelaList: [],
            codigoescuela: '',
        },
        computed: {
            filteredFiles() {
                return this.fileList.filter(x => x.Name.toLowerCase().includes(this.searchFile.toLowerCase()));
            },

        },
        watch: {
        },
        created: function () {
            this.run_waitMe();
            //this.GetListRoles();
            this.GetAllEscuela();
            this.initialConfig();
        },
        mounted: function () {
            this.run_waitMe();
            //url params  
            var currentDate = moment().format("DD/MM/YYYY");
            this.saleDate = currentDate;
            let urlParams = new URLSearchParams(window.location.search);
            if (urlParams.get('p')) {
                let params = JSON.parse(atob(urlParams.get('p')));
                if (params.areaId) {
                    this.areaId = params.areaId;
                    this.getAreaById();
                }
                if (params.Update) {
                    this.update = params.Update;
                }
            }

            //end url params

            //this.inter();
            Promise.all([
                //this.GetAllMandatoryDocument()

            ]).then(values => {

            })
                .catch(error => {
                    console.error(error);
                }).finally(function () {
                    EasyLoading.hide();
                });


        },
        methods: {
            GetListRoles() {
                let vm = this;

                let roles = JSON.parse(sessionStorage.getItem("RolesKetalty"));
                if (roles) {
                    vm.ListRoles = roles;
                } else {
                    //window.location = "/Login/Index";
                }

                this.NamesRoles = this.NamesRoles.map((role) => {
                    let roleFind = roles.find((item) => {
                        return item.Name === role.Name;
                    });
                    return {
                        ...role,
                        Id: roleFind ? roleFind.Id : 0,
                    };
                });

                this.NamesRoles = this.NamesRoles.filter((role) => role.Id !== 0);

            },

            getAreaById() {
                let vm = this;
                debugger;
                axios
                    .get("/Area/GetAreaById", {
                        params: {
                            areaId: vm.areaId
                        },
                    })
                    .then(async function (response) {
                        vm.descripcion = response.data.Descripcion;
                        debugger;
                        vm.codigoescuela = response.data.CodigoEscuela;

                        await vm.$nextTick();
                        var element = document.getElementById("codigoescuela");
                        $.HSCore.components.HSSelect2.init($(element));

                    })
                    .catch(function (response) {
                        console.error('Error al obtener la categoría');
                    })
                    .finally(function () {

                    });

            },
            async save() {
                let vm = this;
                debugger;
                vm.$validator.validateAll('formCreateArea').then(async function (result) {
                    if (result) {
                        vm.saveArea();
                    } else {
                        iziToast.error({
                            title: 'Error',
                            message: 'información incompleta.',
                            position: 'topRight',
                            timeout: 3000
                        });
                    }
                });
            },
            saveArea: async function () {
                let vm = this;
                vm.run_waitMe();
                var formData = new FormData();
                debugger;
                let obj = {
                    "CodigoArea": vm.areaId,
                    "descripcion": vm.descripcion,
                    "CodigoEscuela": vm.codigoescuela
                };

                formData.append("model", JSON.stringify(obj));

                return axios({
                    method: 'post',
                    url: vm.areaId === 0 ? '/Area/CreateArea' : '/Area/CreateArea',
                    data: formData,
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data'
                    }
                }).then(function (response) {
                    Swal.fire({
                        title: vm.areaId === 0 ? `Se creó la área N° ${response.data.Id}` : `Se actualizaron los datos del área N° ${response.data.Id}`,
                        text: 'Buen Trabajo!',
                        icon: 'success',
                        confirmButtonColor: '#002F87',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'OK',
                        allowOutsideClick: false,

                    }).then((result) => {
                        if (result.isConfirmed) {
                            window.location = "/Area/Index";
                        }
                    });


                    return response.data;
                }).catch(function (error) {
                    iziToast.error({
                        title: 'Error',
                        message: vm.areaId === 0 ? 'al crear el área' : 'al actualizar el área',
                        position: 'topRight',
                        timeout: 3000
                    });
                }).finally(function () {
                    EasyLoading.hide();
                });
            },
            initialConfig() {
                var currentDate = moment().format("DD-MM-YYYY");
                this.formatSaleDate = JSON.stringify(
                    {
                        "appendTo": "#saleDateflat",
                        "dateFormat": "d/m/Y",
                        "maxDate": currentDate,
                        "wrap": true,
                        "locale": {
                            "firstDayOfWeek": 0,
                            "weekdays": {
                                "shorthand": ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"]
                            },
                            "months": {
                                "longhand": ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"]
                            }
                        }
                    }
                );

                this.formatStartValidity = JSON.stringify(
                    {
                        "appendTo": "#startValidityFlatpickr",
                        "dateFormat": "d/m/Y",
                        "minDate": currentDate,
                        "wrap": true,
                        "locale": {
                            "firstDayOfWeek": 0,
                            "weekdays": {
                                "shorthand": ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"]
                            },
                            "months": {
                                "longhand": ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"]
                            }
                        }
                    }
                );

                this.formatStartOfContract = JSON.stringify(
                    {
                        "appendTo": "#startOfBillingFlat",
                        "dateFormat": "d/m/Y",
                        "minDate": currentDate,
                        "wrap": true,
                        "locale": {
                            "firstDayOfWeek": 0,
                            "weekdays": {
                                "shorthand": ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"]
                            },
                            "months": {
                                "longhand": ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"]
                            }
                        }
                    }
                );

            },
            pasar_colocacion() {
                window.location = "/Area/Index";
            },
            changeContractor(val) {
                this.person = val;
            },
            changeStep(item) {
                this.formAttachedDocumentShow = false;
                this.formAffiliateListShow = false;
                this.formPreAffiliationShow = false;
                if (item === 'contratante') {
                    this.contratanteShow = true;
                    this.placementShow = false;
                    this.placementShowHeader = false;
                    this.attachedDocumentShow = false;
                    this.preAffiliationShow = false;
                    this.affiliateListShow = false;
                    this.confirmationShow = false;
                } else if (item === 'placement') {
                    this.contratanteShow = true;
                    this.placementShow = true;
                    this.placementShowHeader = true;
                    this.attachedDocumentShow = false;
                    this.preAffiliationShow = false;
                    this.affiliateListShow = false;
                    this.confirmationShow = false;
                    this.formPreAffiliationShow = false;
                } else if (item === 'attachedDocument') {

                    this.contratanteShow = true;
                    this.placementShow = false;
                    this.placementShowHeader = true;
                    this.attachedDocumentShow = true;
                    this.preAffiliationShow = false;
                    this.affiliateListShow = false;
                    this.confirmationShow = false;

                    this.formAttachedDocumentShow = true;
                } else if (item === 'preAffiliation') {
                    this.contratanteShow = true;
                    this.placementShow = true;
                    this.placementShowHeader = true;
                    this.attachedDocumentShow = true;
                    this.preAffiliationShow = true;
                    this.affiliateListShow = false;
                    this.confirmationShow = false;

                    this.formPreAffiliationShow = true;
                } else if (item === 'affiliateList') {
                    this.contratanteShow = true;
                    this.placementShow = true;
                    this.placementShowHeader = true;
                    this.attachedDocumentShow = true;
                    this.preAffiliationShow = true;
                    this.affiliateListShow = true;
                    this.confirmationShow = false;

                    this.formAffiliateListShow = true;
                } else if (item === 'confirmation') {
                    this.previewCalculateAmountToPay();
                    this.contratanteShow = true;
                    this.placementShow = true;
                    this.placementShowHeader = true;
                    this.attachedDocumentShow = true;
                    this.preAffiliationShow = true;
                    this.affiliateListShow = true;
                    this.confirmationShow = true;
                }
            },
            run_waitMe: function () {

                EasyLoading.show({
                    type: EasyLoading.TYPE.BALL_SCALE_MULTIPLE,
                    text: "Espere por favor, procesando datos..."
                });

            },
            GetAllEscuela() {
                let vm = this;
                return axios
                    .get("/Area/GetAllEscuela", {

                    })
                    .then(function (response) {
                        vm.escuelaList = response.data;
                        return response.data;
                    })
                    .catch(function (response) {

                        console.error(`Error al buscar escuelas`);
                    })
                    .finally(function () {

                    });

            },
        },
    });
})();