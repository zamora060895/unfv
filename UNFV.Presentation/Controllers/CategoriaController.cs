﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using UNFV.Presentation.Models.Categoria;
using Negocio;
using Entidad.Repositorio.Models;
namespace UNFV.Presentation.Controllers
{
    public class CategoriaController : Controller
    {
        public CategoriaController()
        {

        }
        [AllowAnonymous]
        public IActionResult Index()
        {
            return View();
        }
        [AllowAnonymous]
        public IActionResult CategoryDetail()
        {
            return View();
        }
        [AllowAnonymous]
        public async Task<IActionResult> GetAllCategoryPaginate(string CategoriaPaginateModel)
        {
            try
            {
                CategoriaPaginateModel ObjCategoryPaginate =
                    Newtonsoft.Json.JsonConvert.DeserializeObject<CategoriaPaginateModel>(CategoriaPaginateModel);

                CategoriaNG NCategoria = new CategoriaNG();

                var result = await NCategoria.GetAllCategoriaPaginate(ObjCategoryPaginate.descripcion, ObjCategoryPaginate.page, ObjCategoryPaginate.size);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
                
        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> CreateCategory(string model)
        {
            CategoriaNG NCategoria = new CategoriaNG();
            CreateCategoryModel oCreateCategoryModel = Newtonsoft.Json.JsonConvert.DeserializeObject<CreateCategoryModel>(model);
            Categorium objCategoria = new Categorium();
            objCategoria.CodigoCategoria =Convert.ToInt32(oCreateCategoryModel.CodigoCategoria);
            objCategoria.Descripcion = oCreateCategoryModel.descripcion;
            objCategoria.Habilitado = true;
            var category = await NCategoria.InsertCategoria(objCategoria);
            return Ok(category);
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> GetCategoryById(string categoryId)
        {
            CategoriaNG NCategoria = new CategoriaNG();
            var result = await NCategoria.GetCategoryById(Convert.ToDecimal(categoryId));
            return Ok(result);
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> ChangeStatusCategory(string model)
        {
            CategoriaNG NCategoria = new CategoriaNG();
            CreateCategoryModel oCreateCategoryModel = Newtonsoft.Json.JsonConvert.DeserializeObject<CreateCategoryModel>(model);
            Categorium objCategoria = new Categorium();
            objCategoria.CodigoCategoria =Convert.ToInt32(oCreateCategoryModel.CodigoCategoria);
            if (oCreateCategoryModel.estado == "Activo")
            {
                objCategoria.Habilitado = false;
            }
            else {
                objCategoria.Habilitado = true;
            }
            var category = await NCategoria.ChangeStatusCategory(objCategoria);
            return Ok(category);
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> GetAllCategoria()
        {
            CategoriaNG NCategoria = new CategoriaNG();
            var result = await NCategoria.GetAllCategoria();
            return Ok(result);
        }
    }
}
