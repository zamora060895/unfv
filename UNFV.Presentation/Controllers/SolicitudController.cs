﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using UNFV.Presentation.Models.Solicitud;
using Negocio;
using Entidad.Repositorio.Models;
using Microsoft.AspNetCore.Http;
using System.Linq;
using System.Security.Claims;

namespace UNFV.Presentation.Controllers
{
    public class SolicitudController : Controller
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        public SolicitudController(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }
        [AllowAnonymous]
        public IActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        public IActionResult SolicitudDetail()
        {
            return View();
        }

        [AllowAnonymous]
        public async Task<IActionResult> GetAllSolicitudPaginate(string SolicitudPaginateModel)
        {
            try
            {
                SolicitudPaginateModel ObjSolicitudPaginate =
                    Newtonsoft.Json.JsonConvert.DeserializeObject<SolicitudPaginateModel>(SolicitudPaginateModel);

                SolicitudNG NSolicitud = new SolicitudNG();
                int codigo_facultad = 0;
                if (ObjSolicitudPaginate.codigofacultad != "")
                {
                    codigo_facultad = Convert.ToInt32(ObjSolicitudPaginate.codigofacultad);
                }

                int codigo_escuela = 0;
                if (ObjSolicitudPaginate.codigoescuela != "")
                {
                    codigo_escuela = Convert.ToInt32(ObjSolicitudPaginate.codigoescuela);
                }

                int codigo_area = 0;
                if (ObjSolicitudPaginate.codigoarea != "")
                {
                    codigo_area = Convert.ToInt32(ObjSolicitudPaginate.codigoarea);
                }

                int codigo_estado = 0;
                if (ObjSolicitudPaginate.codigoestado != "")
                {
                    codigo_estado = Convert.ToInt32(ObjSolicitudPaginate.codigoestado);
                }

                int nrosolicitud = 0;
                if (ObjSolicitudPaginate.nrosolicitud != "")
                {
                    nrosolicitud = Convert.ToInt32(ObjSolicitudPaginate.nrosolicitud);
                }

                var result = await NSolicitud.GetAllSolicitudPaginate(ObjSolicitudPaginate.creationDateFrom, ObjSolicitudPaginate.creationDateTo, nrosolicitud, codigo_facultad, codigo_escuela, codigo_area, codigo_estado, ObjSolicitudPaginate.page, ObjSolicitudPaginate.size);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> GetAllEstadoSolicitud()
        {
            SolicitudNG NSolicitud = new SolicitudNG();
            var result = await NSolicitud.GetAllEstadoSolicitud();
            return Ok(result);
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> GetAllSolicitudTrackingPaginate(string codigosolicitud, string Page, string Size)
        {
            SolicitudNG NSolicitud = new SolicitudNG();
            var result = await NSolicitud.GetAllSolicitudTrackingPaginate(codigosolicitud, Page, Size);
            return Ok(result);
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> CreateSolicitud(string model)
        {
            var data = ((ClaimsIdentity)User.Identity);
            var Usuario = data.FindFirst("User_Id").Value;
            

            SolicitudNG NSolicitud = new SolicitudNG();
            CreateSolicitudModel oCreateSolicitudModel = Newtonsoft.Json.JsonConvert.DeserializeObject<CreateSolicitudModel>(model);
            Solicitud objSolicitud = new Solicitud();
            objSolicitud.CodigoSolicitud = Convert.ToInt32(oCreateSolicitudModel.CodigoSolicitud);
            objSolicitud.CodigoArea = Convert.ToInt32(oCreateSolicitudModel.CodigoArea);
            objSolicitud.CodigoArticulo = Convert.ToInt32(oCreateSolicitudModel.CodigoArticulo);
            objSolicitud.ValorAproximado = oCreateSolicitudModel.ValorAproximado;
            objSolicitud.Cantidad = oCreateSolicitudModel.Cantidad;
            objSolicitud.CodigoUsuarioCreacion = Convert.ToInt32(Usuario);

            var solicitud = await NSolicitud.InsertSolicitud(objSolicitud);
            return Ok(solicitud);
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> GetAreaById(string solicitudId)
        {
            SolicitudNG NSolicitud = new SolicitudNG();
            var result = await NSolicitud.GetSolicitudById(Convert.ToDecimal(solicitudId));
            return Ok(result);
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> ChangeStatusSolicitud(string model)
        {
            var data = ((ClaimsIdentity)User.Identity);
            var Usuario = data.FindFirst("User_Id").Value;
            SolicitudNG NSolicitud = new SolicitudNG();
            CreateSolicitudModel oCreateSolicitudModel = Newtonsoft.Json.JsonConvert.DeserializeObject<CreateSolicitudModel>(model);
            Solicitud objSolicitud = new Solicitud();
            objSolicitud.CodigoSolicitud = Convert.ToInt32(oCreateSolicitudModel.CodigoSolicitud);

            objSolicitud.CodigoEstado = Convert.ToInt32(oCreateSolicitudModel.CodigoEstado);
            objSolicitud.CodigoUsuarioCreacion = Convert.ToInt32(Usuario);
            var solicitud = await NSolicitud.ChangeStatusSolicitud(objSolicitud);
            return Ok(solicitud);
        }

        [AllowAnonymous]
        public async Task<IActionResult> GetAllSolicitudPendientePaginate(string SolicitudPaginateModel)
        {
            try
            {
                SolicitudPaginateModel ObjSolicitudPaginate =
                    Newtonsoft.Json.JsonConvert.DeserializeObject<SolicitudPaginateModel>(SolicitudPaginateModel);

                SolicitudNG NSolicitud = new SolicitudNG();

                int nrosolicitud = 0;
                if (ObjSolicitudPaginate.nrosolicitud != "")
                {
                    nrosolicitud = Convert.ToInt32(ObjSolicitudPaginate.nrosolicitud);
                }

                var result = await NSolicitud.GetAllSolicitudPendientePaginate(nrosolicitud, ObjSolicitudPaginate.page, ObjSolicitudPaginate.size);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [AllowAnonymous]
        public async Task<IActionResult> GetAllSolicitudxDonacionPaginate(string SolicitudPaginateModel)
        {
            try
            {
                SolicitudPaginateModel ObjSolicitudPaginate =
                    Newtonsoft.Json.JsonConvert.DeserializeObject<SolicitudPaginateModel>(SolicitudPaginateModel);

                SolicitudNG NSolicitud = new SolicitudNG();

                int nrodonacion = 0;
                if (ObjSolicitudPaginate.nrodonacion != "")
                {
                    nrodonacion = Convert.ToInt32(ObjSolicitudPaginate.nrodonacion);
                }

                var result = await NSolicitud.GetAllSolicitudxDonacionPaginate(nrodonacion, ObjSolicitudPaginate.page, ObjSolicitudPaginate.size);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}
