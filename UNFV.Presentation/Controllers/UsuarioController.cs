﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using UNFV.Presentation.Models.Usuario;
using Negocio;
using Entidad.Repositorio.Models;
using Datos.DTO;
namespace UNFV.Presentation.Controllers
{
    public class UsuarioController : Controller
    {
        public UsuarioController()
        {

        }

        [AllowAnonymous]
        public IActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        public IActionResult UsuarioDetail()
        {
            return View();
        }
        [AllowAnonymous]
        public async Task<IActionResult> GetAllUsuarioPaginate(string UsuarioPaginateModel)
        {
            try
            {
                UsuarioPaginateModel ObjUsuarioPaginate =
                    Newtonsoft.Json.JsonConvert.DeserializeObject<UsuarioPaginateModel>(UsuarioPaginateModel);

                UsuarioNG NUsuario = new UsuarioNG();

                var result = await NUsuario.GetAllUsuarioPaginate(ObjUsuarioPaginate.nombre, ObjUsuarioPaginate.numeroDocumento, ObjUsuarioPaginate.login, ObjUsuarioPaginate.page, ObjUsuarioPaginate.size);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> CreateUsuario(string model)
        {
            UsuarioNG NUsuario = new UsuarioNG();
            CreateUsuarioModel oCreateUsuarioModel = Newtonsoft.Json.JsonConvert.DeserializeObject<CreateUsuarioModel>(model);
            UsuarioDto objUsuario = new UsuarioDto();
            objUsuario.CodigoUsuario = Convert.ToInt32(oCreateUsuarioModel.CodigoUsuario);
            objUsuario.Nombre = oCreateUsuarioModel.nombre;
            objUsuario.ApellidoPaterno = oCreateUsuarioModel.apellidoPaterno;
            objUsuario.ApellidoMaterno = oCreateUsuarioModel.apellidoMaterno;
            objUsuario.NumeroDocumento = oCreateUsuarioModel.numeroDocumento;
            objUsuario.CorreoElectronico = oCreateUsuarioModel.correoElectronico;
            objUsuario.Login = oCreateUsuarioModel.login;
            objUsuario.Contrasenia = oCreateUsuarioModel.contrasenia;
            objUsuario.CodigoEstudiante = oCreateUsuarioModel.codigoEstudiante;
            objUsuario.Administrador = oCreateUsuarioModel.Administrador;
            objUsuario.OperadorDonaciones = oCreateUsuarioModel.OperadorDonaciones;
            objUsuario.OperadorSolicitudes = oCreateUsuarioModel.OperadorSolicitudes;
            objUsuario.Habilitado = true;
            var category = await NUsuario.InsertUsuario(objUsuario);
            return Ok(category);
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> GetUsuarioById(string usuarioId)
        {
            UsuarioNG NUsuario = new UsuarioNG();
            var result = await NUsuario.GetUsuarioById(Convert.ToDecimal(usuarioId));
            return Ok(result);
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> ChangeStatusUsuario(string model)
        {
            UsuarioNG NUsuario = new UsuarioNG();
            CreateUsuarioModel oCreateUsuarioModel = Newtonsoft.Json.JsonConvert.DeserializeObject<CreateUsuarioModel>(model);
            Usuario objUsuario = new Usuario();
            objUsuario.CodigoUsuario = Convert.ToInt32(oCreateUsuarioModel.CodigoUsuario);
            if (oCreateUsuarioModel.estado == "Activo")
            {
                objUsuario.Habilitado = false;
            }
            else
            {
                objUsuario.Habilitado = true;
            }
            var category = await NUsuario.ChangeStatusUsuario(objUsuario);
            return Ok(category);
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Validar_Existencia_Usuario(string model)
        {
            UsuarioNG NUsuario = new UsuarioNG();
            CreateUsuarioModel oCreateUsuarioModel = Newtonsoft.Json.JsonConvert.DeserializeObject<CreateUsuarioModel>(model);
           
            var usuario = await NUsuario.Validar_Existencia_Usuario(oCreateUsuarioModel.CodigoUsuario, oCreateUsuarioModel.numeroDocumento, oCreateUsuarioModel.login, oCreateUsuarioModel.codigoEstudiante);
            return Ok(usuario);
        }

        [AllowAnonymous]
        public async Task<IActionResult> GetAllUserPaginateForSearchValue(string searchValue, string Page, string Size)
        {
            UsuarioNG NUsuario = new UsuarioNG();
            var result = await NUsuario.GetAllUserPaginateForSearchValue(searchValue, Page, Size);
            return Ok(result);
        }
    }
}
