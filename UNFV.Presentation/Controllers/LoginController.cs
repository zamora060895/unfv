﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;
using Entidad.Repositorio.Models;
using Negocio;
using System.Security.Claims;

namespace Keralty.Presentation.PreAffiliation.Controllers
{
    public class LoginController : Controller
    {

        private readonly ILogger<LoginController> _logger;

        public LoginController(ILogger<LoginController> logger)
        {
            _logger = logger;
        }

        [AllowAnonymous]
        public IActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        public async Task<IActionResult> GetByTokenUser(string User, string Password)
        {
            UsuarioNG NUsuario = new UsuarioNG();
            UsuarioRolNG NUsuarioRolNG = new UsuarioRolNG();

            try
            {
                var objUsuario = NUsuario.Validar_Sesion_Por_Usuario(User, Password);

                var lstRoles = NUsuarioRolNG.Obtener_Roles_Por_Usuario(0);

                if (objUsuario != null)
                {
                    lstRoles = NUsuarioRolNG.Obtener_Roles_Por_Usuario(objUsuario.CodigoUsuario);
                }

                var jsonData = new { user = objUsuario, roles = lstRoles };

                DateTimeOffset dateTimeOffset = DateTimeOffset.FromUnixTimeSeconds(30000);

                string codigo_usuario = "";
                string nombre = "";
                string apellidoPaterno = "";

                if (objUsuario != null)
                {
                    codigo_usuario = objUsuario.CodigoUsuario.ToString();
                    nombre = objUsuario.Nombre;
                    apellidoPaterno = objUsuario.ApellidoPaterno;
                }

                List<Claim> claims = new List<Claim>
            {
                new Claim("User_Id", codigo_usuario),
                new Claim("User_Name", nombre),
                new Claim("User_SurName", apellidoPaterno),
                new Claim("GroupSignal", "control")
            };

                var authProperties = new AuthenticationProperties
                {
                    ExpiresUtc = DateTimeOffset.UtcNow.AddMinutes(30)
                };


                var claimIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                await HttpContext.SignInAsync(
                    CookieAuthenticationDefaults.AuthenticationScheme,
                    new ClaimsPrincipal(claimIdentity),
                    authProperties);               

                return Ok(jsonData);
                //return Ok();
            }
            catch (Exception)
            {

                throw;
            }


        }

        [AllowAnonymous]
        public async Task<IActionResult> GetMenuByTokenUser(string UserId)
        {
            MenuNG NMenuNG = new MenuNG();
            var menuByTokenUser = await NMenuNG.Obtener_Menu_Por_Usuario(Convert.ToInt32(UserId));
            return Ok(menuByTokenUser);
        }

        public async Task<IActionResult> LogOut()
        {
            await HttpContext.SignOutAsync();
            return Ok();
        }


    }
}
