﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using UNFV.Presentation.Models.Articulo;
using Negocio;
using Entidad.Repositorio.Models;

namespace UNFV.Presentation.Controllers
{
    public class ArticuloController : Controller
    {
        public ArticuloController()
        {

        }
        [AllowAnonymous]
        public IActionResult Index()
        {
            return View();
        }
        [AllowAnonymous]
        public IActionResult ArticuloDetail()
        {
            return View();
        }
        [AllowAnonymous]
        public async Task<IActionResult> GetAllArticuloPaginate(string ArticuloPaginateModel)
        {
            try
            {
                ArticuloPaginateModel ObjArticuloPaginate =
                    Newtonsoft.Json.JsonConvert.DeserializeObject<ArticuloPaginateModel>(ArticuloPaginateModel);

                ArticuloNG NArticulo = new ArticuloNG();
                int codigo_categoria = 0;
                if (ObjArticuloPaginate.codigocategoria != "")
                {
                    codigo_categoria = Convert.ToInt32(ObjArticuloPaginate.codigocategoria);
                }

                var result = await NArticulo.GetAllArticuloPaginate(ObjArticuloPaginate.descripcion, codigo_categoria, ObjArticuloPaginate.page, ObjArticuloPaginate.size);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> CreateArticulo(string model)
        {
            ArticuloNG NArticulo = new ArticuloNG();
            CreateArticuloModel oCreateArticuloModel = Newtonsoft.Json.JsonConvert.DeserializeObject<CreateArticuloModel>(model);
            Articulo objArticulo = new Articulo();
            objArticulo.CodigoArticulo = Convert.ToInt32(oCreateArticuloModel.CodigoArticulo);
            objArticulo.CodigoCategoria = Convert.ToInt32(oCreateArticuloModel.CodigoCategoria);
            objArticulo.Descripcion = oCreateArticuloModel.descripcion;
            objArticulo.Habilitado = true;
            var area = await NArticulo.InsertArticulo(objArticulo);
            return Ok(area);
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> GetArticuloById(string articuloId)
        {
            ArticuloNG NArticulo = new ArticuloNG();
            var result = await NArticulo.GetArticuloById(Convert.ToDecimal(articuloId));
            return Ok(result);
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> ChangeStatusArticulo(string model)
        {
            ArticuloNG NArticulo = new ArticuloNG();
            CreateArticuloModel oCreateArticuloModel = Newtonsoft.Json.JsonConvert.DeserializeObject<CreateArticuloModel>(model);
            Articulo objArticulo = new Articulo();
            objArticulo.CodigoArticulo = Convert.ToInt32(oCreateArticuloModel.CodigoArticulo);
            if (oCreateArticuloModel.estado == "Activo")
            {
                objArticulo.Habilitado = false;
            }
            else
            {
                objArticulo.Habilitado = true;
            }
            var articulo = await NArticulo.ChangeStatusArticulo(objArticulo);
            return Ok(articulo);
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> GetAllArticulo()
        {
            ArticuloNG NArticulo = new ArticuloNG();
            var result = await NArticulo.GetAllArticulo();
            return Ok(result);
        }
    }
}
