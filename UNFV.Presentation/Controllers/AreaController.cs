﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using UNFV.Presentation.Models.Area;
using Negocio;
using Entidad.Repositorio.Models;

namespace UNFV.Presentation.Controllers
{
    public class AreaController : Controller
    {
        public AreaController()
        {

        }
        [AllowAnonymous]
        public IActionResult Index()
        {
            return View();
        }
        [AllowAnonymous]
        public IActionResult AreaDetail()
        {
            return View();
        }
        [AllowAnonymous]
        public async Task<IActionResult> GetAllAreaPaginate(string AreaPaginateModel)
        {
            try
            {
                AreaPaginateModel ObjAreaPaginate =
                    Newtonsoft.Json.JsonConvert.DeserializeObject<AreaPaginateModel>(AreaPaginateModel);

                AreaNG NArea = new AreaNG();
                int codigo_escuela = 0;
                if (ObjAreaPaginate.codigoescuela != "")
                {
                    codigo_escuela = Convert.ToInt32(ObjAreaPaginate.codigoescuela);
                }

                var result = await NArea.GetAllAreaPaginate(ObjAreaPaginate.descripcion, codigo_escuela, ObjAreaPaginate.page, ObjAreaPaginate.size);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> CreateArea(string model)
        {
            AreaNG NArea = new AreaNG();
            CreateAreaModel oCreateAreaModel = Newtonsoft.Json.JsonConvert.DeserializeObject<CreateAreaModel>(model);
            Area objArea = new Area();
            objArea.CodigoArea = Convert.ToInt32(oCreateAreaModel.CodigoArea);
            objArea.CodigoEscuela = Convert.ToInt32(oCreateAreaModel.CodigoEscuela);
            objArea.Descripcion = oCreateAreaModel.descripcion;
            objArea.Habilitado = true;
            var area = await NArea.InsertArea(objArea);
            return Ok(area);
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> GetAreaById(string areaId)
        {
            AreaNG NArea = new AreaNG();
            var result = await NArea.GetAreaById(Convert.ToDecimal(areaId));
            return Ok(result);
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> ChangeStatusArea(string model)
        {
            AreaNG NArea = new AreaNG();
            CreateAreaModel oCreateAreaModel = Newtonsoft.Json.JsonConvert.DeserializeObject<CreateAreaModel>(model);
            Area objArea = new Area();
            objArea.CodigoArea = Convert.ToInt32(oCreateAreaModel.CodigoArea);
            if (oCreateAreaModel.estado == "Activo")
            {
                objArea.Habilitado = false;
            }
            else
            {
                objArea.Habilitado = true;
            }
            var area = await NArea.ChangeStatusArea(objArea);
            return Ok(area);
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> GetAllEscuela()
        {
            AreaNG NArea = new AreaNG();
            var result = await NArea.GetAllEscuela();
            return Ok(result);
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> GetAllArea()
        {
            AreaNG NArea = new AreaNG();
            var result = await NArea.GetAllArea();
            return Ok(result);
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> GetAllFacultad()
        {
            AreaNG NArea = new AreaNG();
            var result = await NArea.GetAllFacultad();
            return Ok(result);
        }
    }
}
