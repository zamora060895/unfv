﻿using System.ComponentModel.DataAnnotations;

namespace Keralty.Presentation.PreAffiliation.Enums
{

    public enum ReglasValidacion
    {
        [Display(Name = "VALIDACIÓN DE IPRESS")]
        VIPRESS,
        [Display(Name = "VALIDACIÓN DE SERVICIOS")]
        VSERVI,
        [Display(Name = "VALIDACIÓN DE COBERTURA")]
        VCOBER,
        [Display(Name = "VALIDACIÓN DE CARENCIA")]
        VCAREN,
        [Display(Name = "VALIDACIÓN DE DIAGNÓSTICO")]
        VDIAGN,
        [Display(Name = "VALIDACIÓN DE TIEMPO DE ESPERA")]
        VTIEMP,
        [Display(Name = "DIAGNÓSTICO AUTOMATICOS")]
        ADIAGN,
        [Display(Name = "SERVICIOS AUTOMATICOS")]
        ASERVI


    }
    public enum RolesEnum
    {
        [Display(Name = "Administrador")]
        Admin,
        [Display(Name = "Operador Donaciones")]
        OperadorDonaciones,
        [Display(Name = "Operador Solicitudes")]
        OperadorSolicitudes
        //[Display(Name = "Gestor de ventas")]
        //GestorVenta,
        //[Display(Name = "Ejecutivo de ventas")]
        //EjecutivoVenta,
        //[Display(Name = "Vendedor EPS")]
        //VendedorEPS,
        //[Display(Name = "Supervisor de solicitudes")]
        //SupervisorSolicitudes,
        //[Display(Name = "Agente Comercial")]
        //AgenteComercial
    }
}
