﻿using System.ComponentModel.DataAnnotations;

namespace Keralty.Presentation.PreAffiliation.Enums
{
    public enum ClaimStatus
    {
        [Display(Name = "Creado")]//Estado transitorio, no se registra en DB ni se mostrará en los desplegables de estados
        Creado = 0,

        [Display(Name = "Registrado")]
        Registrado = 1,

        [Display(Name = "Asignado")]
        Asignado = 2,

        [Display(Name = "Respuesta Preliminar")]
        Preliminar = 3,

        [Display(Name = "Devuelto")]
        Devuelto = 4,

        [Display(Name = "Cerrado")]
        Cerrado = 5,

        [Display(Name = "Archivado")]
        Archivado = 6,

        [Display(Name = "Anulado")]
        Anulado = 7

    }

    }
