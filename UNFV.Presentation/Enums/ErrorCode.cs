﻿using System.Runtime.Serialization;

namespace Keralty.Presentation.PreAffiliation.Enums
{
    [DataContract]
    public enum eErrorCode
    {
        /// <summary>
        /// Sin errores.
        /// </summary>
        [EnumMember]
        None,
        /// <summary>
        /// Error inesperado. Consultar los logs para más detalles.
        /// </summary>
        [EnumMember]
        UnexpectedError,
        /// <summary>
        /// Nombre de usuario o contraseña desconocidos.
        /// </summary>
        [EnumMember]
        UnknownUsernameOrPassword,
        /// <summary>
        /// El usuario esta deshabilitado.
        /// </summary>
        [EnumMember]
        UserIsDisabled,
        /// <summary>
        /// El usuario no tiene roles asignados.
        /// </summary>
        [EnumMember]
        NoAssignedRoles,
        /// <summary>
        /// No existen modulos asignados para los roles y la instancia requerida por el usuario.
        /// </summary>
        [EnumMember]
        NoAssignedModules,
        /// <summary>
        /// El usuario no esta autorizado a ejecutar la operación.
        /// </summary>
        [EnumMember]
        Unauthorized,
        /// <summary>
        /// La sesión expiró por inactividad.
        /// </summary>
        [EnumMember]
        SessionInactivityExpiration,
        /// <summary>
        /// El usuario debe cambiar su contraseña antes de poder operar con el sistema.
        /// </summary>
        [EnumMember]
        PasswordChangeRequired,
        /// <summary>
        /// No se encontró la entidad buscada en la operación.
        /// </summary>
        [EnumMember]
        NotFound,
        /// <summary>
        /// La operación no es válida en el contexto de los datos proporcionados.
        /// </summary>
        [EnumMember]
        InvalidOperation,

        [EnumMember]
        ChannelRequired,
        /// <summary>
        /// El usuario no tiene definido el tipo de acceso

        [EnumMember]
        CompanyRequired,
        /// <summary>
        /// El usuario no tiene definido la compañia a la que pertenece
        
        [EnumMember]
        BrokerRequired,
        /// <summary>
        /// El usuario no tiene asignado un Broker

        [EnumMember]
        InstanceRequired,
        /// <summary>
        /// El usuario no tiene instancia correcta
        /// 
    }
}
