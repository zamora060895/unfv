using System;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;

namespace Keralty.Presentation.PreAffiliation
{
    public class Program
    {
        private static string _env = "";
        public static void Main(string[] args)
        {
            SetEnvironment();
            CreateHostBuilder(args).Build().Run();
        }
        private static void SetEnvironment()
        {
            try
            {
                var config = new ConfigurationBuilder()
                    .AddJsonFile("appsettings.json", false)
                    .Build();

                _env = config.GetSection("Environment").Value;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static IWebHostBuilder CreateHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args).ConfigureAppConfiguration((hostingContext, config) =>
            {
                config.AddJsonFile("appsettings.json");
                config.AddJsonFile($"appsettings.{_env}.json", optional: true);
            }).UseStartup<Startup>();
    }
}
