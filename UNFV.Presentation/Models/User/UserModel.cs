﻿using System;

namespace Keralty.Presentation.PreAffiliation.Models
{
    public class UserModel
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public int? BranchOfficeId { get; set; }
        public int? PrintBranchOfficeId { get; set; }
        public DateTime? CreationDate { get; set; }
        public int? CompanyId { get; set; }
        public string Email { get; set; }
        public int? IdentityDocumentTypeId { get; set; }
        public string IdentityDocument { get; set; }
        public string SecondSurname { get; set; }
        public int? AccessTypeId { get; set; }
        public int? BrokerId { get; set; }
        public int? PositionId { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public string UpdatedBy { get; set; }
        public int? CreatedBy { get; set; }
        public int? IpressId { get; set; }
        public string FinalName { get; set; }

        public decimal? BrokerCode { get; set; }
    }
}
