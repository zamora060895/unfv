﻿namespace Keralty.Presentation.PreAffiliation.Models.MandatoryDocument
{
    public class MandatoryDocumentModel
    {
        public decimal Id { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
        public decimal TypeOriginId { get; set; }
        public string IsPlan { get; set; }
        public string SendToSign { get; set; }
    }
}
