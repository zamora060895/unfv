﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UNFV.Presentation.Models.Usuario
{
    public class CreateUsuarioModel
    {
        public decimal? CodigoUsuario { get; set; }
        public string nombre { get; set; }
        public string apellidoPaterno { get; set; }
        public string apellidoMaterno { get; set; }
        public string numeroDocumento { get; set; }
        public string correoElectronico { get; set; }
        public string login { get; set; }
        public string contrasenia { get; set; }
        public string codigoEstudiante { get; set; }
        public string estado { get; set; }
        public bool Administrador { get; set; }
        public bool OperadorSolicitudes { get; set; }
        public bool OperadorDonaciones { get; set; }
    }
}
