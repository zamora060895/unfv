﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UNFV.Presentation.Models.Usuario
{
    public class UsuarioPaginateModel
    {
        //public string creationDateFrom { get; set; }
        //public string creationDateTo { get; set; }
        public string nombre { get; set; }
        public string numeroDocumento { get; set; }
        public string login { get; set; }
        public string page { get; set; }
        public string size { get; set; }

    }
}
