﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UNFV.Presentation.Models.Solicitud
{
    public class CreateSolicitudModel
    {
        public decimal? CodigoSolicitud { get; set; }
        public decimal CodigoArticulo { get; set; }
        public decimal CodigoArea { get; set; }
        public decimal ValorAproximado { get; set; }
        public int Cantidad { get; set; }
        public decimal CodigoEstado { get; set; }
    }
}
