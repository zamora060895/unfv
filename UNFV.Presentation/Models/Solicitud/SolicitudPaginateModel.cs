﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UNFV.Presentation.Models.Solicitud
{
    public class SolicitudPaginateModel
    {
        public string creationDateFrom { get; set; }
        public string creationDateTo { get; set; }
        public string codigofacultad { get; set; }
        public string codigoescuela { get; set; }
        public string codigoarea { get; set; }
        public string codigoestado { get; set; }
        public string nrosolicitud { get; set; }
        public string nrodonacion { get; set; }
        public string page { get; set; }
        public string size { get; set; }
    }
}
