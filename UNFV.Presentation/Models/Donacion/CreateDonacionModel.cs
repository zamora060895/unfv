﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UNFV.Presentation.Models.Donacion
{
    public class CreateDonacionModel
    {
        public decimal? CodigoDonacion { get; set; }
        public decimal Codigoestudiante { get; set; }       
        public decimal MontoTotal { get; set; }
        public decimal CodigoEstado { get; set; }
        public List<SolicitudModel> SolicitudList { get; set; }
    }
}
