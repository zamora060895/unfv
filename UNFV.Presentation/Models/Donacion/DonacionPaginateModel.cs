﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UNFV.Presentation.Models.Donacion
{
    public class DonacionPaginateModel
    {
        public string creationDateFrom { get; set; }
        public string creationDateTo { get; set; }
        public string codigodonacion { get; set; }
        public string codigoestudiante { get; set; }
        public string codigoestado { get; set; }
        public string page { get; set; }
        public string size { get; set; }
    }
}
