﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UNFV.Presentation.Models.Donacion
{
    public class SolicitudModel
    {
        public decimal? CodigoSolicitud { get; set; }
        public decimal? Cantidad { get; set; }
    }
}
