﻿using System.Collections.Generic;
using UNFV.Presentation.Models.Files;

namespace UNFV.Presentation.Models.Donacion
{
    public class DonacionFileAddModel
    {
        public int donacionId { get; set; }
        public List<FileModel> documentList { get; set; }
    }
}
