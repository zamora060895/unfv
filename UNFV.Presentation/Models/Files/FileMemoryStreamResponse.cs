﻿using System.IO;
namespace Keralty.Presentation.PreAffiliation.Models.Files
{
    public class FileMemoryStreamResponse : BaseOperationResponse
    {
        public string Message { get; set; }

        public string FileMemoryStream { get; set; }
    }
}
