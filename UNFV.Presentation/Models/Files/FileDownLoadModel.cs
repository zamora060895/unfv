﻿namespace Keralty.Presentation.PreAffiliation.Models.Files
{
    public class FileDownLoadModel
    {
        public string FileBase64 { get; set; }
        public string FileName { get; set; }
    }
}
