﻿using System.Collections.Generic;

namespace Keralty.Presentation.PreAffiliation.Models.Files
{
    public class CustomFileModelDeclarationModel
    {
        public decimal PersonId { get; set; }
        public decimal? PreAffiliationDeclarationId { get; set; }
        public List<FileModelDeclarationModel> FileModelDeclaration { get; set; }
    }
}
