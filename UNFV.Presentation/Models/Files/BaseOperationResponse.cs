﻿using System.Runtime.Serialization;
using Keralty.Presentation.PreAffiliation.Enums;

namespace Keralty.Presentation.PreAffiliation.Models.Files
{
    public abstract class BaseOperationResponse
    {
        /// <summary>
        /// Indica si la operación se completo exitosamente.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Describe el error en caso de que haya ocurrido uno, o devuelve <value>eErrorCode.None</value>.
        /// </summary>
        [DataMember]
        public eErrorCode ErrorCode { get; set; }

        /// <summary>
        /// Provee información detallada acerca del error en caso de que haya ocurrido uno.
        /// </summary>
        [DataMember]
        public string ErrorDescription { get; set; }
    }
}
