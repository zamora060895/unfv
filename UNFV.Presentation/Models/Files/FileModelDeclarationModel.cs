﻿
namespace Keralty.Presentation.PreAffiliation.Models.Files
{
    public class FileModelDeclarationModel
    {
        public decimal? id { get; set; }
        public decimal preAffiliationId { get; set; }
        public string file { get; set; }
        public string name { get; set; }
        public string fileExtension { get; set; }
        public decimal? preaffiliationDeclarationId { get; set; }
        public decimal? sourceId { get; set; }
        public string mandatory { get; set; }
        public string mandatoryDocumentTypeId { get; set; }
        public string description { get; set; }
    }
}
