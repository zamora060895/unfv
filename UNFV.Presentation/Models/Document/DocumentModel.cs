﻿using System;

namespace Keralty.Presentation.PreAffiliation.Models.Document
{
    public class DocumentModel
    {
        public decimal Id { get; set; }
        public string Name { get; set; }
        public decimal OriginId { get; set; }
        public decimal SourceId { get; set; }
        public string Description { get; set; }
        public string Mandatory { get; set; }
        public string FileBase64 { get; set; }
        public string SendToSign { get; set; }

    }
}
