﻿using System;
using System.Collections.Generic;

namespace Keralty.Presentation.PreAffiliation.Models.Service
{
    public  class VwAllMedicineModel
    {
      
        public string Code { get; set; }

        public string Description { get; set; }

        public string Concentration { get; set; }

        public string Type { get; set; }

        public decimal? Quantity { get; set; } = 1;


    }
}
