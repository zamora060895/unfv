﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Keralty.Presentation.PreAffiliation.Models.Service
{
    public  class ServiceModel
    {
        public decimal Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string GenderId { get; set; }
        public decimal ServiceTypeId { get; set; }
        public decimal FrequencyMonths { get; set; }
        public decimal FrequencyQuantity { get; set; }
        public decimal ParentId { get; set; }
        public string Status { get; set; }
        public string ParentCode { get; set; }
        public decimal ReferenceLevel { get; set; }

       
        public string CodigoSanitas { get; set; }
        public string NombreServicioSanitas { get; set; }
        public string CodigoIpress { get; set; }
        public string NombreServicioIpress { get; set; }
        public bool IsLaterality { get; set; }
        public string EsCovid { get; set; }

        public decimal CopaymentFixed { get; set; }
        public string CopaymentVariable { get; set; }
        public string ExceedsLimit { get; set; }

        public string Automatic { get; set; }
        public bool select { get; set; }
    }
}
