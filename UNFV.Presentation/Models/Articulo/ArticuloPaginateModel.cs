﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UNFV.Presentation.Models.Articulo
{
    public class ArticuloPaginateModel
    {
        public string descripcion { get; set; }
        public string codigocategoria { get; set; }
        public string page { get; set; }
        public string size { get; set; }
    }
}
