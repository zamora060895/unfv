﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UNFV.Presentation.Models.Articulo
{
    public class CreateArticuloModel
    {
        public decimal? CodigoArticulo { get; set; }
        public decimal CodigoCategoria { get; set; }
        public string descripcion { get; set; }
        public string estado { get; set; }
    }
}
