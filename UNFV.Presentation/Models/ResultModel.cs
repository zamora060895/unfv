﻿namespace Keralty.Presentation.PreAffiliation.Models
{
    public class ResultModel
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
        public bool Success { get; set; }
    }
}
