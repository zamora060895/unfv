﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UNFV.Presentation.Models.Area
{
    public class AreaPaginateModel
    {
        public string descripcion { get; set; }
        public string codigoescuela { get; set; }
        public string page { get; set; }
        public string size { get; set; }
    }
}
