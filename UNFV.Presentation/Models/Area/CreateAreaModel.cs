﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UNFV.Presentation.Models.Area
{
    public class CreateAreaModel
    {
        public decimal? CodigoArea{ get; set; }
        public decimal CodigoEscuela{ get; set; }
        public string descripcion { get; set; }
        public string estado { get; set; }
    }
}
