﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UNFV.Presentation.Models.Categoria
{
    public class CategoriaPaginateModel
    {
        //public string creationDateFrom { get; set; }
        //public string creationDateTo { get; set; }
        public string descripcion { get; set; }
        public string page { get; set; }
        public string size { get; set; }

    }
}
