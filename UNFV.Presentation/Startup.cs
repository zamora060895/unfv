using Keralty.Presentation.PreAffiliation.Enums;
using Keralty.Presentation.PreAffiliation.Middleware;
using Keralty.Presentation.PreAffiliation.Models;
using Keralty.Presentation.PreAffiliation.Settings;
using Keralty.Presentation.PreAffiliation.Signalr;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using UNFV.Presentation.Enums;
using DinkToPdf;
using DinkToPdf.Contracts;

namespace Keralty.Presentation.PreAffiliation
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
                    options.AddPolicy("PreAffiliationCors",
                        builder =>
                        {
                            builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader();
                        }
                    )
                );

            services.AddHttpContextAccessor();
            services.AddHttpClient();
             
            services.Configure<ApiSettings>(Configuration.GetSection(nameof(ApiSettings)));
            services.Configure<ParameterSetting>(Configuration.GetSection(nameof(ParameterSetting)));

            services.AddAuthorization(options =>
            {
                options.AddPolicy("PreAffiliationPolice", policy =>
                    policy.RequireClaim(ClaimTypes.Role, new String[] {
                        RolesEnum.Admin.GetEnumDisplayName(),
                        RolesEnum.OperadorDonaciones.GetEnumDisplayName(),
                        RolesEnum.OperadorSolicitudes.GetEnumDisplayName()//,
                        //RolesEnum.AsistentAdvisor.GetEnumDisplayName(),
                        //RolesEnum.GestorVenta.GetEnumDisplayName(),
                        //RolesEnum.SupervisorSolicitudes.GetEnumDisplayName(),
                        //RolesEnum.EjecutivoVenta.GetEnumDisplayName(),
                        //RolesEnum.AgenteComercial.GetEnumDisplayName(),
                        //RolesEnum.VendedorEPS.GetEnumDisplayName()
                    }));
            });

            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(options =>
                {
                    options.AccessDeniedPath = "/Login/Index";
                    options.LoginPath = "/Login/Index";
                    options.Cookie.Name = "AspNetPreAffiliation";
                    options.Events = new CookieAuthenticationEvents()
                    {
                        OnRedirectToLogin = ctx =>
                        {
                            ctx.Response.Redirect(ctx.RedirectUri);
                            return Task.CompletedTask;
                        },
                        OnValidatePrincipal = context =>
                        {
                            var claimsPrincipal = context.Principal;
                            var Expire_At = claimsPrincipal.FindFirstValue("Expire_At");
                            var refreshToke = claimsPrincipal.FindFirstValue("Refresh_token");

                            if (Convert.ToDateTime(Expire_At) < DateTime.Now)
                            {
                                var httpClient = new HttpClient();
                                var values = new Dictionary<string, string>
                                    {
                                         { "grant_type", "refresh_token" },
                                         { "client_id", "ApiAffiliationId" },
                                         { "client_secret", "SanitasPeruSecret" },
                                         { "refresh_token", refreshToke },
                                    };

                                //var response = httpClient.PostAsync(Configuration["ApiSettings:Token"] + "/connect/token", new FormUrlEncodedContent(values)).GetAwaiter().GetResult();
                                //var contents = JsonConvert.DeserializeObject<TokenModel>(response.Content.ReadAsStringAsync().Result);

                                //if (contents.access_token != null)
                                //{
                                //    var identity = (ClaimsIdentity)claimsPrincipal.Identity;

                                //    var handler = new JwtSecurityTokenHandler();
                                //    var token = handler.ReadJwtToken(contents.access_token);
                                //    var expireAt = token.Claims.Where(x => x.Type == "exp").FirstOrDefault().Value;
                                //    DateTimeOffset dateTimeOffset = DateTimeOffset.FromUnixTimeSeconds(Convert.ToInt32(expireAt));

                                //    var accessTokenClaim = identity.FindFirst("Access_token");
                                //    var refreshTokenClaim = identity.FindFirst("Refresh_token");
                                //    var expireTime = identity.FindFirst("Expire_At");

                                //    identity.RemoveClaim(accessTokenClaim);
                                //    identity.RemoveClaim(refreshTokenClaim);
                                //    identity.RemoveClaim(expireTime);

                                //    identity.AddClaim(new Claim("Refresh_token", contents.refresh_token));
                                //    identity.AddClaim(new Claim("Access_Token", contents.access_token));
                                //    identity.AddClaim(new Claim("Expire_At", dateTimeOffset.LocalDateTime.ToString()));

                                //    context.ShouldRenew = true;
                                //}
                            }


                            return Task.CompletedTask;
                        }
                    };

                });

            services.AddSignalR();

            services.AddControllersWithViews().AddMvcOptions(x =>
            {
                var policy = new AuthorizationPolicyBuilder()
                                    .RequireAuthenticatedUser()
                                    .Build();
                x.Filters.Add(new AuthorizeFilter(policy));

                x.CacheProfiles.Add("DefaultNoCacheProfile", new CacheProfile
                {
                    NoStore = true,
                    Location = ResponseCacheLocation.None
                });
                x.Filters.Add(new ResponseCacheAttribute
                {
                    CacheProfileName = "DefaultNoCacheProfile"
                });
            }).AddJsonOptions(o =>
            {
                o.JsonSerializerOptions.PropertyNamingPolicy = null;
            });

            services.AddSingleton(typeof(IConverter), new SynchronizedConverter(new PdfTools()));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseCors("PreAffiliationCors");

            app.UseStaticFiles();

            //loggerFactory.AddSyslog(
            //        Configuration.GetValue<string>("Papertrail:host"),
            //        Configuration.GetValue<int>("Papertrail:port"));

            loggerFactory.AddFile(@"LogOptimusUI\Log-{Date}.txt");

            app.UseRouting();

            app.UseMiddleware<MagamentExceptionMiddleware>();

            app.UseCookiePolicy();

            app.UseAuthentication();

            //app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Login}/{action=Index}/{id?}");

                endpoints.MapHub<SignalServer>("/signalserver");
            });
        }

    }
}
