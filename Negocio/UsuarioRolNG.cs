﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidad.Repositorio.Models;
using Datos;

namespace Negocio
{
    public class UsuarioRolNG
    {
        public async Task<List<Rol>> Obtener_Roles_Por_Usuario(long codigo_usuario)
        {
            return await new UsuarioRolDA().Obtener_Roles_Por_Usuario(codigo_usuario);
        }
    }
}
