﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;
using Entidad.Repositorio.Models;
using Datos.DTO;
namespace Negocio
{
    public class CategoriaNG
    {
        public async Task<DataCollection<CategoriaDto>> GetAllCategoriaPaginate(string descripcion, string page, string size)
        {
            return await new CategoriaDA().GetAllCategoriaPaginate(descripcion, page,size);
        }

        public async Task<ResultDto> InsertCategoria(Categorium objCategoria)
        {
            return await new CategoriaDA().InsertCategoria(objCategoria);
        }

        public async Task<CategoriaDto> GetCategoryById(decimal codigoCategoria)
        {
            return await new CategoriaDA().GetCategoryById(codigoCategoria);
        }

        public async Task<ResultDto> ChangeStatusCategory(Categorium objCategoria)
        {
            return await new CategoriaDA().ChangeStatusCategory(objCategoria);
        }

        public async Task<List<CategoriaDto>> GetAllCategoria()
        {
            return await new CategoriaDA().GetAllCategoria();
        }
    }
}
