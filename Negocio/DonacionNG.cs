﻿using System.Text;
using System.Threading.Tasks;
using Datos;
using Entidad.Repositorio.Models;
using Datos.DTO;
using System.Collections.Generic;
using DinkToPdf;
using DinkToPdf.Contracts;

namespace Negocio
{
    public class DonacionNG
    {
        public async Task<DataCollection<DonacionDto>> GetAllDonacionPaginate(string creationDateFrom, string creationDateTo, int codigodonacion, string codigoestudiante, int codigoestado, string page, string size)
        {
            return await new DonacionDA().GetAllDonacionPaginate(creationDateFrom, creationDateTo, codigodonacion, codigoestudiante, codigoestado, page, size);
        }
        public async Task<List<EstadoDto>> GetAllEstadoDonacion()
        {
            return await new DonacionDA().GetAllEstadoDonacion();
        }

        public async Task<DataCollection<HistorialDonacionDto>> GetAllDonacionTrackingPaginate(string codigodonacion, string page, string size)
        {
            return await new DonacionDA().GetAllDonacionTrackingPaginate(codigodonacion, page, size);
        }

        public async Task<FileDownLoadModel> GetPdfDonacionById(string codigodonacion, string decano, string jefeOficAdm, int flagPreActa)
        {
            return await new DonacionDA().GetPdfDonacionById(codigodonacion, decano, jefeOficAdm, flagPreActa);
        }
        public async Task<ResultDto> ChangeStatusDonacion(Donacion objDonacion)
        {
            return await new DonacionDA().ChangeStatusDonacion(objDonacion);
        }

        public async Task<ResultDto> CreateDonacion(DonacionDto objDonacion)
        {
            return await new DonacionDA().CreateDonacion(objDonacion);
        }

        public async Task<DonacionDto> GetDonacionById(decimal codigoDonacion)
        {
            return await new DonacionDA().GetDonacionById(codigoDonacion);
        }

        public async Task<ResultDto> CreateDonacionArchivos(DonacionDto objDonacion)
        {
            return await new DonacionDA().CreateDonacionArchivos(objDonacion);
        }

        public async Task<List<FileDto>> GetArchivosDonacion(decimal codigoDonacion)
        {
            return await new DonacionDA().GetArchivosDonacion(codigoDonacion);
        }

        public async Task<List<PDFArticuloDto>> GetAllArticuloByDonacion(decimal codigoDonacion)
        {
            return await new DonacionDA().GetAllArticuloByDonacion(codigoDonacion);
        }

        public async Task<List<PDFBoletaDto>> GetAllBoletasByDonacion(decimal codigoDonacion)
        {
            return await new DonacionDA().GetAllBoletasByDonacion(codigoDonacion);
        }
    }
}
