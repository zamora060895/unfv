﻿using System.Text;
using System.Threading.Tasks;
using Datos;
using Entidad.Repositorio.Models;
using Datos.DTO;
using System.Collections.Generic;

namespace Negocio
{
    public class AreaNG
    {
        public async Task<DataCollection<AreaDto>> GetAllAreaPaginate(string descripcion, int codigoescuela, string page, string size)
        {
            return await new AreaDA().GetAllAreaPaginate(descripcion, codigoescuela, page, size);
        }

        public async Task<ResultDto> InsertArea(Area objArea)
        {
            return await new AreaDA().InsertArea(objArea);
        }

        public async Task<AreaDto> GetAreaById(decimal codigoArea)
        {
            return await new AreaDA().GetAreaById(codigoArea);
        }

        public async Task<ResultDto> ChangeStatusArea(Area objArea)
        {
            return await new AreaDA().ChangeStatusArea(objArea);
        }

        public async Task<List<EscuelaDto>> GetAllEscuela()
        {
            return await new AreaDA().GetAllEscuela();
        }
        public async Task<List<AreaDto>> GetAllArea()
        {
            return await new AreaDA().GetAllArea();
        }

        public async Task<List<FacultadDto>> GetAllFacultad()
        {
            return await new AreaDA().GetAllFacultad();
        }
    }
}
