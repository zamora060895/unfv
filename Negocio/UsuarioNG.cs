﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;
using Entidad.Repositorio.Models;
using Datos.DTO;
namespace Negocio
{
    public class UsuarioNG
    {
        public Usuario Validar_Sesion_Por_Usuario(string usuario, string password)
        {
            return new UsuarioDA().Validar_Sesion_Por_Usuario(usuario, password);
        }
        public async Task<DataCollection<UsuarioDto>> GetAllUsuarioPaginate(string nombre, string numeroDocumento, string login, string page, string size)
        {
            return await new UsuarioDA().GetAllUsuarioPaginate(nombre, numeroDocumento, login, page, size);
        }

        public async Task<ResultDto> InsertUsuario(UsuarioDto objUsuario)
        {
            return await new UsuarioDA().InsertUsuario(objUsuario);
        }

        public async Task<UsuarioDto> GetUsuarioById(decimal codigoUsuario)
        {
            return await new UsuarioDA().GetUsuarioById(codigoUsuario);
        }

        public async Task<ResultDto> ChangeStatusUsuario(Usuario objUsuario)
        {
            return await new UsuarioDA().ChangeStatusUsuario(objUsuario);
        }

        public async Task<ResultDto> Validar_Existencia_Usuario(decimal? codigousuario,string numeroDocumento, string login, string codigoEstudiante)
        {
            return await new UsuarioDA().Validar_Existencia_Usuario(codigousuario,numeroDocumento, login, codigoEstudiante);
        }

        public async Task<DataCollection<UsuarioDto>> GetAllUserPaginateForSearchValue(string searchValue, string Page, string Size)
        {
            return await new UsuarioDA().GetAllUserPaginateForSearchValue(searchValue, Page, Size);
        }
    }
}
