﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidad.Repositorio.Models;
using Datos;
using Datos.DTO;
namespace Negocio
{
    public class MenuNG
    {
        public async Task<List<MenuDto>> Obtener_Menu_Por_Usuario(long codigo_usuario)
        {
            return await new MenuDA().Obtener_Menu_Por_Usuario(codigo_usuario);
        }
    }
}
