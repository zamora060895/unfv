﻿using System.Text;
using System.Threading.Tasks;
using Datos;
using Entidad.Repositorio.Models;
using Datos.DTO;
using System.Collections.Generic;
namespace Negocio
{
    public class SolicitudNG
    {
        public async Task<DataCollection<SolicitudDto>> GetAllSolicitudPaginate(string creationDateFrom, string creationDateTo,int nrosolicitud, int codigofacultad, int codigoescuela, int codigoarea, int codigoestado, string page, string size)
        {
            return await new SolicitudDA().GetAllSolicitudPaginate(creationDateFrom, creationDateTo, nrosolicitud,codigofacultad, codigoescuela, codigoarea, codigoestado, page, size);
        }

        public async Task<ResultDto> InsertSolicitud(Solicitud objSolicitud)
        {
            return await new SolicitudDA().InsertSolicitud(objSolicitud);
        }

        public async Task<SolicitudDto> GetSolicitudById(decimal codigoSolicitud)
        {
            return await new SolicitudDA().GetSolicitudById(codigoSolicitud);
        }

        public async Task<ResultDto> ChangeStatusSolicitud(Solicitud objSolicitud)
        {
            return await new SolicitudDA().ChangeStatusSolicitud(objSolicitud);
        }
        public async Task<List<EstadoDto>> GetAllEstadoSolicitud()
        {
            return await new SolicitudDA().GetAllEstadoSolicitud();
        }

        public async Task<DataCollection<HistorialSolicitudDto>> GetAllSolicitudTrackingPaginate(string codigosolicitud, string page, string size)
        {
            return await new SolicitudDA().GetAllSolicitudTrackingPaginate(codigosolicitud, page,size);
        }

        public async Task<DataCollection<SolicitudDto>> GetAllSolicitudPendientePaginate(int nrosolicitud, string page, string size)
        {
            return await new SolicitudDA().GetAllSolicitudPendientePaginate(nrosolicitud, page, size);
        }

        public async Task<DataCollection<SolicitudDto>> GetAllSolicitudxDonacionPaginate(int nrosolicitud, string page, string size)
        {
            return await new SolicitudDA().GetAllSolicitudxDonacionPaginate(nrosolicitud, page, size);
        }
    }
}
