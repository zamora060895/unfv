﻿using System.Text;
using System.Threading.Tasks;
using Datos;
using Entidad.Repositorio.Models;
using Datos.DTO;
using System.Collections.Generic;

namespace Negocio
{
    public class ArticuloNG
    {
        public async Task<DataCollection<ArticuloDto>> GetAllArticuloPaginate(string descripcion, int codigocategoria, string page, string size)
        {
            return await new ArticuloDA().GetAllArticuloPaginate(descripcion, codigocategoria, page, size);
        }

        public async Task<ResultDto> InsertArticulo(Articulo objArticulo)
        {
            return await new ArticuloDA().InsertArticulo(objArticulo);
        }

        public async Task<ArticuloDto> GetArticuloById(decimal codigoArticulo)
        {
            return await new ArticuloDA().GetArticuloById(codigoArticulo);
        }

        public async Task<ResultDto> ChangeStatusArticulo(Articulo objArticulo)
        {
            return await new ArticuloDA().ChangeStatusArticulo(objArticulo);
        }

        public async Task<List<ArticuloDto>> GetAllArticulo()
        {
            return await new ArticuloDA().GetAllArticulo();
        }

    }
}
